import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Container, Row, Col } from "reactstrap";
import video from "../video/naruto.mp4"

function CaixaVideo(props) {
  return (
    <Container className={props.tipoCaixa + " borda10 fundoVideo"}>
      <Row>
        <Col md="12" lg="12" className="marginLeftIcone">
          <a href={props.link}>
            <img
              src={props.icone}
              alt=""
              className="imgCaixaVideo"
            />
          </a>
        </Col>
      </Row>
    </Container>
  );
}

class ModalVideo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  render() {
    const videoSrc = "../video/naruto.mp4";
    return (
      <div>
        <Button onClick={this.toggle}>{this.props.buttonLabel}</Button>
        <Modal
          isOpen={this.state.modal}
          toggle={this.toggle}
          className={this.props.className}
        >
          <Container>
            <Row>
              <Col md="12" lg="12">
                <iframe
                  width="560"
                  height="315"
                  src={require("../video/naruto.mp4")}
                  frameborder="0"
                  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                  allowfullscreen
                ></iframe>
              </Col>
            </Row>
          </Container>
          
        </Modal>
      </div>
    );
  }
}

export default ModalVideo;
