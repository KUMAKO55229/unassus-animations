/* eslint-disable import/first */
import React from "react";
import Testeira from "./Testeira.js";
import CapaUnidade from "./capa/CapaPP.js";
import Sumario from "./SumarioLink.js";
import Unidade from "./Unidade1.js";
import menuHamburguer from "../img/menuObesidade2.png";
import ReturnStickyButton from "./ReturnStickyButton.js";

import Botoes from "./botoes/Botoes.js";


import un1 from "../img/un1/un1.png";
import un2 from "../img/un1/un2.png";
import un3 from "../img/un1/un3.png";

const testeira = {
  nomeCursoPt1: "Promoção do Ganho de Peso",
  nomeCursoPt2: "Adequado na Gestação",
  nomeUnidadePt1: "GESTAÇÃO: IMPORTÂNCIA DO ESTADO NUTRICIONAL,",
  nomeUnidadePt2: "ALIMENTAÇÃO E NUTRIÇÃO",
  tipoBarra: "barraHamburguerLogo"
};

const sumario = {
  qntColunas: 2,
  tipoSumario: "sumarioUnidade"
};

const sumarioPt1 = [
  { id: "/un1#sec11", title: "Papel do estado nutricional pré-gestacional" },
  { id: "/un1#sec12", title: "Importância do ganho de peso adequado na gestãção" }
];

const sumarioPt2 = [
  {
    id: "/un1#sec13",
    title: "Impacto do estado nutricional da gestante e o pós-parto"
  },
  {
    id: "/un1#sec15",
    title: "Alimentação da gestante na prevenção do sobrepeso e obesidade"
  }
];

const linkCurso = "https://unasus-cp.moodle.ufsc.br/course/view.php?id=472";

const unidade = {
  tipoUnidade: "barraUnidade",
  corBarra: "yellow"
};

function DescritorUnidade1() {
  return (
    <div className="DescritorUnidade1">
      {/*<Testeira nomeCursoPt1={testeira.nomeCursoPt1} nomeCursoPt2 ={testeira.nomeCursoPt2} 
        nomeUnidadePt1={testeira.nomeUnidadePt1} nomeUnidadePt2={testeira.nomeUnidadePt2} 
        tipoBarra={testeira.tipoBarra} menuHamburguer={menuHamburguer}/> */}
      <div id="top">
        <CapaUnidade
          tituloPt1={testeira.nomeUnidadePt1}
          tituloPt2={testeira.nomeUnidadePt2}
        />
        <Testeira
          nomeCursoPt1={testeira.nomeCursoPt1}
          nomeCursoPt2={testeira.nomeCursoPt2}
          nomeUnidadePt1={testeira.nomeUnidadePt1}
          nomeUnidadePt2={testeira.nomeUnidadePt2}
          tipoBarra={testeira.tipoBarra}
          menuHamburguer={menuHamburguer}
        />
        

        <Sumario
          tipoSumario={sumario.tipoSumario}
          titulosPt1={sumarioPt1}
          titulosPt2={sumarioPt2}
        />
      </div>
      <ReturnStickyButton linkCurso={linkCurso} />
      <Unidade tipoUnidade={unidade.tipoUnidade} corBarra={unidade.corBarra} />      
      <Botoes
        un1Path={"/un1#top"}
        un2Path={"/un2#top"}
        un3Path={"/un3#top"}
        un1={un1}
        un2={un2}
        un3={un3}
      />
    </div>
  );
}

export default DescritorUnidade1;
