import React from "react";
import "./css/Unidade.css";
import "./css/Utilitarios.css";
import "./css/Unidade2.css";
import "font-awesome/css/font-awesome.min.css";
import ScrollAnimation from "react-animate-on-scroll";

import {
	Container,
	Row,
	Col,
	Button,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Carousel,
	CarouselItem,
	CarouselControl,
	CarouselIndicators,
	CarouselCaption
} from "reactstrap";
import { caixa } from "./Utilitarios.js";
import { criarLink } from "./Utilitarios.js";
import {
	TituloSecaoBarra2,
	TituloSubSecaoBarra,
	CaixaTemplate,
	CaixaGeralRetorno,
	TituloSecaoBarraNova,
	TituloSubSecaoBarraNova
} from "./Utilitarios.js";

import ModalPopUp from "./ModalPopUp.js";

import ReturnStickyButton from "./ReturnStickyButton.js";

import IconeHide from "./IconeNota.js";
import arrow from "../img/bullet.png";
import DivHide from "./DivClickHide.js";
// import changeImgPlace from "./changeImgPlace.js";

import img01 from "../img/un1/img01.jpeg";
import n1 from "../img/n1.png";
import n2 from "../img/n2.png";
import img02 from "../img/un1/img02.jpeg";
import img03 from "../img/un1/img03.png";

import infob from "../img/un1/infob.png";
import info02 from "../img/un1/info02.png";
import info02a from "../img/un1/info02a.png";

import quadro01a from "../img/un1/quadro01a.png";
import quadro01 from "../img/un1/quadro01.png";
import quadro5 from "../img/un1/quadro5.png";
import quadro6 from "../img/un1/quadro6.png";
import quadro02 from "../img/un1/quadro02.png";
import quadro3 from "../img/un1/quadro3.png";
import img04 from "../img/un2/img04.png";
import info04 from "../img/un1/info04.png";
import quadro4 from "../img/un1/quadro4.png";

import img05 from "../img/un2/img05.png";
import img06 from "../img/un2/img06.png";
import img07 from "../img/un2/img07.png";
import img07b from "../img/un2/img07b.png";

import imgCobrinhas from "../img/un1/Mooc3v1-59.png";

import img01a from "../img/un1/img01a.png";
import img08 from "../img/un2/img08.png";
import img09 from "../img/un2/img09.png";
import img10 from "../img/un2/img10.jpg";
import img11 from "../img/un2/img11.png";
import tabG from "../img/un1/Mooc3v1-51.png";
import tabG2 from "../img/un1/Mooc3v2-62.png";

import iconeSaibaMais from "../img/saibaMais.png";
import iconeReflexao from "../img/reflexao.png";
import iconeDestaque from "../img/destaque.png";
import iconeNota from "../img/nota.png";
import iconeLink from "../img/link.png";

import imgIMC1 from "../img/uni1/infoIMC1.png";
import imgIMC2 from "../img/uni1/infoIMC4.png";

import click from "../img/click.png";
import Slider from "./SliderUn1.js";
import cliqueAqui from "../img/clique.png";

import mulheres from "../img/un1/Mooc3v1-60.png";

import Accordion from "./Accord.js";

class Unidade extends React.Component {
	constructor() {
		super();
		this.state = {
			isHiddenCaixa: true,
			isHiddenCaixa2: true,
			modal: false
		};
		this.toggle = this.toggle.bind(this);
	}
	toggle() {
		this.setState(prevState => ({
			modal: !prevState.modal
		}));
	}
	toggleCaixa() {
		this.setState({
			isHiddenCaixa: !this.state.isHiddenCaixa
		});
	}

	toggleCaixa2() {
		this.setState({
			isHiddenCaixa2: !this.state.isHiddenCaixa2
		});
	}
	componentDidMount() {
		setTimeout(() => {
			//var height01 = document.getElementById("textoImg01").clientHeight;
			var height02 = document.getElementById("textoImgQuadro")
				.clientHeight;

			var heightN =
				document.getElementById("textoImg02").clientHeight +
				document.getElementById("textoImg03").clientHeight +
				16;

			this.setState({
				//height01,
				height02,
				heightN
			});
		}, 1000);
	}

	render() {
		return (
			<div>
				<TituloSecaoBarraNova
					titulo={"Introdução"}
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo25"
					}
				/>
				<Container className="">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Nesta unidade, abordaremos o papel do estado
								nutricional pré-gestacional e gestacional na
								prevenção do sobrepeso e da obesidade, tanto em
								curto quanto em longo prazo. Outro ponto
								abordado será a importância de uma alimentação
								adequada e saudável ao longo da gestação para
								garantia da manutenção de um ganho de peso
								adequado. Também vamos acompanhar aspectos
								importantes do papel do profissional da saúde,
								da gestante e de sua família na garantia de um
								pré-natal que contemple os cuidados necessários
								para o período gestacional. Ao final da leitura
								dessa unidade, você será capaz de entender e
								orientar sobre a importância desses fatores na
								saúde da gestante. Desejamos uma ótima leitura!
							</p>
						</Col>
					</Row>
				</Container>
				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo3"
					}
					titulo={"PAPEL DO ESTADO NUTRICIONAL PRÉ-GESTACIONAL"}
				/>
				<Container className="">
					<Row>
						<Col md="6" lg="6" id="sec11">
							<p className="textoUnidade">
								A garantia de saúde das mães e dos seus filhos é
								um tema prioritário para a saúde pública no
								Brasil e no mundo. As evidências indicam que
								quando conseguimos manter um estado nutricional
								adequado na gestante, é possível trazer
								vantagens de saúde para a mulher e para o filho,
								não apenas na gestação, mas ao longo da sua vida
								(RASMUSSEN et al., 2009; GILMORE; REDMAN 2014).
								As taxas de excesso de peso nas mulheres em
								idade fértil vêm aumentando com o passar dos
								anos.
							</p>
						</Col>
						<Col md="6" lg="6">
							<p>
								A Pesquisa Vigitel, realizada em 2018, avaliou
								os fatores de risco para doenças crônicas em
								todo o Brasil (BRASIL, 2018) e indicou que mais
								da metade das mulheres brasileiras (53,9%)
								estavam com excesso de peso, sendo que 20,7% das
								mulheres eram obesas, ou seja, metade da
								população feminina chegará à gestação com algum
								nível de excesso de peso. Atualmente, entre as
								mulheres, há um aumento progressivo da
								prevalência de obesidade em faixas de idade
								reprodutiva, confira as taxas a seguir.
							</p>
						</Col>
					</Row>
				</Container>
				<Container className="espacamentoBottom">
					<Row>
						<Col md="12" lg="12">
							<img
								src={img01a}
								className="responsive w100 borda10"
							/>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Esses dados nos trazem o alerta sobre a
								importância de um bom aconselhamento voltado à
								alimentação adequada e saudável e ao cuidado no
								acompanhamento e monitoramento do estado
								nutricional da gestante, não apenas durante a
								gravidez, mas no processo de planejamento dessa
								gestação e em mulheres em idade fértil.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row></Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Você deverá avaliar o estado nutricional
								pré-gestacional através do cálculo do Índice de
								Massa Corporal, que é calculado dividindo o peso
								(em kg) pela altura (em metros) ao quadrado. Com
								o IMC pré-gestacional calculado, você será capaz
								de realizar o diagnóstico nutricional inicial da
								gestante e entender quais parâmetros são
								considerados como baixo peso, eutrofia,
								sobrepeso e obesidade, como pode ser observado
								no infográfico a seguir.
							</p>
						</Col>
					</Row>
				</Container>

				<div className="fundoIMC ">
					<Container>
						<Row>
							<Col
								md="12"
								lg="12"
								className="espacamento espacamentoBottom"
							>
								<img
									src={imgIMC1}
									className="responsive w100 centerImg"
								/>
							</Col>
						</Row>
					</Container>
				</div>

				<div className="linhaIMC"></div>

				<div className="fundoIMC">
					<Container>
						<Row>
							<Col md="12" lg="12" id="parent">
								<p className="centerText" id="child">
									<b>
										Como calcular o Índice de Massa Corporal
										(IMC)
									</b>{" "}
								</p>
							</Col>
						</Row>
					</Container>
				</div>
				<div className="linhaIMC"></div>

				<div className="fundoIMC">
					<div className="fundoIMC espacamento espacamentoBottom deslocarDireitaIMC ">
						<Container className="">
							<Row>
								<Col
									md="6"
									lg="6"
									className="degradeIMC width5 borda10"
								>
									<p className="textoIMC line deslocarDireita">
										Peso (em quilos)
									</p>
									<p className="textoIMC overLine deslocarDireita2">
										Altura x Altura (em metros)
									</p>
								</Col>
								<Col
									md="6"
									lg="6"
									className="degradeIMC marginLeft5 width borda10"
								>
									<span
										onClick={this.toggle}
										className="cursorClicavel"
									>
										<p className="blackLink centerText textoBotao textoIMC">
											<b>
												Clique e <br />
												calcule o IMC
											</b>
										</p>
									</span>
								</Col>
							</Row>
						</Container>
					</div>
				</div>

				<Modal
					isOpen={this.state.modal}
					toggle={this.toggle}
					className="sizeModal"
				>
					<Container>
						<Row className="fundoCinza">
							<Col md="12" lg="12">
								<Button
									color="secondary"
									onClick={this.toggle}
									className="alinharDireitaIcone"
								>
									X
								</Button>
							</Col>
							<Col md="12" lg="12">
								<iframe
									src="https://imc-calc.firebaseapp.com/"
									width="330"
									height="450"
									frameborder="0"
								></iframe>
							</Col>
						</Row>
					</Container>
				</Modal>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				{/*<div className="fundoTabRef espacamento">
					<Container className="espacamentoBottom">
						<Row>
							<Col md="6" lg="6">
								<Container className="tab1">
									<Row>
										<Col
											md="6"
											lg="6"
											className="cel1 celEdited"
										>
											<p className="centerText alignVCenter titulo1">
												Referência
											</p>
										</Col>

										<Col
											md="6"
											lg="6"
											className="cel1 celEdited"
										>
											<p className="centerText alignVCenter titulo1">
												IMC pré-gestacional
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col
											md="6"
											lg="6"
											className="cel2 celEdited"
										>
											<p className="centerText">
												Baixo peso
											</p>
										</Col>

										<Col
											md="6"
											lg="6"
											className="cel2 celEdited "
										>
											<p className="centerText">
												{" "}
												&le; 18,5 km/m²
											</p>
										</Col>
									</Row>
								</Container>

								<Container className="tab1">
									<Row>
										<Col
											md="6"
											lg="6"
											className="cel2 celEdited"
										>
											<p className="centerText">
												Eutrofia
											</p>
										</Col>

										<Col
											md="6"
											lg="6"
											className="cel2 celEdited"
										>
											<p className="centerText">
												18,5 a 24,9 km/m²
											</p>
										</Col>
									</Row>
								</Container>
							</Col>
							<Col md="6" lg="6">
								<Container className="tab1">
									<Row>
										<Col
											md="6"
											lg="6"
											className="cel1 celEdited"
										>
											<p className="centerText alignVCenter titulo1">
												Referência
											</p>
										</Col>

										<Col
											md="6"
											lg="6"
											className="cel1 celEdited"
										>
											<p className="centerText alignVCenter titulo1">
												IMC pré-gestacional
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col
											md="6"
											lg="6"
											className="cel2 celEdited"
										>
											<p className="centerText">
												Sobrepeso
											</p>
										</Col>

										<Col
											md="6"
											lg="6"
											className="cel2 celEdited"
										>
											<p className="centerText">
												25 a 29,9 km/m²
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col
											md="6"
											lg="6"
											className="cel2 celEdited"
										>
											<p className="centerText">
												Obesidade
											</p>
										</Col>

										<Col
											md="6"
											lg="6"
											className="cel2 celEdited"
										>
											<p className="centerText">
												&ge; 30
											</p>
										</Col>
									</Row>
								</Container>
							</Col>
						</Row>
					</Container>
				</div>*/}

				<Container className="">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								O cálculo desse indicador deve ser realizado em
								todas as gestantes, já que o estado nutricional
								pré-gestacional é um forte determinante do ganho
								de peso na gravidez e tem influência direta nos
								desfechos obstétricos e na probabilidade de
								retenção de peso pós-parto (DREHMER et al.,
								2010). Veja a seguir algumas complicações e sua
								relação com o estado nutricional:
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="1" lg="1">
							<img src={n1} className="responsive w60 numero" />
						</Col>
						<Col md="11" lg="11">
							<p className="textoUnidade espacamentoLeft23">
								mulheres com baixo peso pré gestacional têm
								quase o dobro de chance de ter bebês pequenos e
								1,5 vezes mais chance de ter bebês com baixo
								peso ao nascer;
							</p>
						</Col>
						<Col md="1" lg="1">
							<img src={n2} className="responsive w60 numero" />
						</Col>
						<Col md="11" lg="11">
							<p className="textoUnidade espacamentoLeft23">
								{" "}
								mulheres que tinham excesso de peso ou eram
								obesas antes da gravidez são mais propensas a
								ter diabetes mellitus e pré-eclâmpsia, durante a
								gravidez, macrossomia (bebês grandes para a
								idade gestacional), parto induzido, parto
								cesáreo, morte fetal tardia, baixos índices de
								Apgar.
							</p>
						</Col>
					</Row>
				</Container>

				<div className="fundoA">
					<Container className="espacamento">
						<Row>
							<Col md="6" lg="6">
								<p className="textoUnidade">
									Como profissionais da Atenção Primária à
									Saúde, devemos saber que a gestação e o
									período pós-parto são considerados momentos
									na vida da mulher que exigem atenção e
									cuidados específicos, por haver maior
									exposição a fatores que podem levar à
									obesidade. As gestantes constituem o foco do
									processo de pré-natal; porém, não se pode
									deixar de atuar, também, entre companheiros
									e familiares na conscientização do cuidado
									da gestante para prevenção de complicações
									futuras.
								</p>
							</Col>
							<Col md="6" lg="6">
								<p>
									A posição do homem na sociedade está mudando
									tanto quanto os papéis tradicionalmente
									atribuídos às mulheres. Portanto, os
									serviços devem promover o envolvimento dos
									homens (adultos e adolescentes), discutindo
									a sua participação responsável nas questões
									da saúde sexual e reprodutiva – eles poderão
									incentivar e participar ativamente do
									cuidado da gestante em relação à alimentação
									e práticas saudáveis na prevenção do
									sobrepeso e da obesidade (BRASIL, 2012a).{" "}
								</p>
							</Col>
						</Row>
					</Container>
				</div>
				{/*<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaDestaque"}
									icone={iconeDestaque}
									textoUnidade={
										"Sabendo dos efeitos negativos de iniciar uma gestação com IMC inadequado, recomenda-se que sejam realizadas ações de conscientização do cuidado e da manutenção de um peso adequado e saudável para prevenção de sobrepeso e obesidade ao planejar uma futura gestação."
									}
									inserirLink={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>{" "}
							</p>
						</Col>
					</Row>
				</Container>*/}

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								As consultas de rotina de mulheres em idade
								fértil que apresentam sobrepeso ou obesidade
								podem ser utilizadas para abordar os efeitos que
								a obesidade pode ter na fertilidade, assim como
								relatar os riscos associados com sobrepeso e
								obesidade no período gestacional e puerperal.
								Tanto a mulher e sua família quanto os
								profissionais de saúde devem considerar que uma
								perda de peso saudável e gradativa antes da
								gestação será mais benéfica e diminuirá riscos
								para a mulher e seu bebê. Saiba mais sobre essas
								recomendações
								<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaDestaque"}
									icone={iconeDestaque}
									textoUnidade={
										"Sabendo dos efeitos negativos de iniciar uma gestação com IMC inadequado, recomenda-se que sejam realizadas ações de conscientização do cuidado e da manutenção de um peso adequado e saudável para prevenção de sobrepeso e obesidade ao planejar uma futura gestação."
									}
									inserirLink={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p>
								Dessa forma, se evitará complicações futuras ao
								longo do pré-natal. Lembre-se de que devemos
								encorajar a perda de peso saudável, realizada
								através de modificações de hábitos de vida,
								alimentação adequada e exercícios físicos. Um
								cuidado empático e sem preconceitos é essencial
								nesse processo.{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo3"
					}
					titulo={"IMPORTÂNCIA DO GANHO DE PESO ADEQUADO NA GESTAÇÃO"}
				/>
				<Container className="" id="sec12">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								O aumento de peso na gravidez é um processo
								único e biologicamente complexo que suporta as
								funções de crescimento e desenvolvimento do
								feto. Esse ganho de peso na gestação é
								influenciado não só por mudanças na fisiologia e
								no metabolismo da gestante, mas também da
								placenta e das necessidades do feto (BRASIL,
								2012a).
							</p>
						</Col>
					</Row>
				</Container>

				<DivHide
					textoClique={[
						<b>Clique aqui</b>,
						" para conhecer os componentes fisiológicos do ganho de peso na gestação"
					]}
					fundoCaixa={"fundoCinza semBorda"}
					textoCaixa={
						<Container
							className="espacamentoBottom espacamentoLeftTabela"
							fluid={true}
						>
							<Row>
								<Col md="4" lg="4" className="tabEdited">
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel1 cel1Tab2"
											>
												<p className="centerText tabela2Text titulo1">
													Componentes fisiológicos
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													Feto
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													Placenta
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													Líquido amniótico
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2 cel3"
											>
												<p className="centerText tabela2Text">
													Subtotal ovular
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													Útero
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													Mamas
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													Volume sanguíneo
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2 cel3"
											>
												<p className="centerText tabela2Text">
													Subtotal materno
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel1 cel3"
											>
												<p className="centerText tabela2Text">
													Total
												</p>
											</Col>
										</Row>
									</Container>
								</Col>

								<Col md="4" lg="4" className="tabEdited">
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel1 cel1Tab2"
											>
												<p className="centerText tabela2Text alignVCenter titulo1">
													Ganho 1º trimestre
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													Desprezível
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													Desprezível
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													Desprezível
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2 cel3"
											>
												<p className="centerText tabela2Text">
													Desprezível
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													0,3
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													0,1
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													0,3
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2 cel3"
											>
												<p className="centerText tabela2Text">
													0,7
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel1 cel3"
											>
												<p className="centerText tabela2Text">
													0,7
												</p>
											</Col>
										</Row>
									</Container>
								</Col>

								<Col md="4" lg="4" className="tabEdited">
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel1 cel1Tab2"
											>
												<p className="centerText tabela2Text alignVCenter titulo1">
													Ganho 2º trimestre
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													1,0
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													0,3
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													0,4
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2 cel3"
											>
												<p className="centerText tabela2Text">
													1,7
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													0,8
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													0,3
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													1,3
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2 cel3"
											>
												<p className="centerText tabela2Text">
													2,4
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel1 cel3"
											>
												<p className="centerText tabela2Text">
													4,1
												</p>
											</Col>
										</Row>
									</Container>
								</Col>

								<Col md="4" lg="4" className="tabEdited">
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel1 cel1Tab2"
											>
												<p className="centerText tabela2Text alignVCenter titulo1">
													Ganho 3º trimestre
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													2,4
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													0,6
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													1,0
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2 cel3"
											>
												<p className="centerText tabela2Text">
													4,0
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													1,0
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													0,5
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2"
											>
												<p className="centerText tabela2Text">
													1,5
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel2 cel3"
											>
												<p className="centerText tabela2Text">
													3,0
												</p>
											</Col>
										</Row>
									</Container>
									<Container className="tab1">
										<Row>
											<Col
												md="12"
												lg="12"
												className="cel1 cel3"
											>
												<p className="centerText tabela2Text">
													7,0
												</p>
											</Col>
										</Row>
									</Container>
								</Col>
							</Row>
						</Container>
					}
				/>

				<Container className="espacamento">
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								No entanto, este aumento de peso varia
								consideravelmente entre as mulheres e, quando
								não controlado adequadamente, pode mesmo
								representar um risco potencial para o
								desenvolvimento de excesso de peso. O aumento de
								peso pode ocorrer devido a fatores biológicos
								como, por exemplo, idade e história familiar de
								obesidade; fatores comportamentais como
								alimentação, prática de atividade física,
								tabagismo e consumo de álcool; ou fatores
								sociais relacionados ao nível de escolaridade,
								renda familiar, acesso a alimentação saudável,
								dentre outros.
							</p>
						</Col>
						<Col md="6" lg="6">
							<p>
								O ganho de peso pode ser dividido em três
								categorias, considerando-se o estado nutricional
								pré-gestacional ou do primeiro trimestre
								gestacional, confira a seguir!
							</p>
							<img
								src={infob}
								className="responsive w100 borda10"
							/>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Observe no quadro a seguir, a partir do IMC
								pré-gestacional, as recomendações de ganho de
								peso no primeiro trimestre gestacional, de ganho
								de peso total, e as recomendações de ganho de
								peso por semana, considerando o segundo e
								terceiro trimestres gestacionais. Lembre-se de
								registrar as informações no prontuário, na
								caderneta da gestante, e informe a mesma sobre o
								seu estado nutricional atual. Esse procedimento
								deve ser realizado em todas as consultas.
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<img
								src={tabG}
								className="responsive w100 borda10"
							/>
						</Col>
					</Row>
				</Container>

				{/*<Container className="espacamentoBottom">
					<Row>
						<Col md="6" lg="6" className="tabEdited">
							<Container className="tab1">
								<Row>
									<Col
										md="12"
										lg="12"
										className="cel1 cel1Tab2"
									>
										<p className="centerText tabela2Text alignVCenter titulo1">
											IMC pré-gestacional (kg/m²)
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											Baixo peso (&lt;18,5)
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											Eutrofia (18,5 - 24,9)
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											Sobrepeso (25 - 29,9)
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											Obesidade (&#8807;30)
										</p>
									</Col>
								</Row>
							</Container>
						</Col>
						<Col md="6" lg="6" className="tabEdited">
							<Container className="tab1">
								<Row>
									<Col
										md="12"
										lg="12"
										className="cel1 cel1Tab2"
									>
										<p className="centerText tabela2Text alignVCenter titulo1">
											Ganho de peso (kg) até a 13ª semana
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											2,0
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											1,5
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											1,0
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											0,5
										</p>
									</Col>
								</Row>
							</Container>
						</Col>

						<Col md="6" lg="6" className="tabEdited">
							<Container className="tab1">
								<Row>
									<Col
										md="12"
										lg="12"
										className="cel1 cel1Tab2"
									>
										<p className="centerText tabela2Text alignVCenter titulo1 differentCel">
											Ganho de peso (kg) semanal no 2º e
											3º trimestes (a partir da 14ª
											semana)
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											0,51 (0,44 - 0,58)
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											0,42 (0,35 - 0,50)
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											0,28 (0,23 - 0,33)
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											0,22 (0,17 - 0,27)
										</p>
									</Col>
								</Row>
							</Container>
						</Col>

						<Col md="6" lg="6" className="tabEdited">
							<Container className="tab1">
								<Row>
									<Col
										md="12"
										lg="12"
										className="cel1 cel1Tab2"
									>
										<p className="centerText tabela2Text alignVCenter titulo1">
											Ganho de peso total (kg)
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											12,5 - 18,0
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											11,5 - 16,0
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											7,0 - 11,5
										</p>
									</Col>
								</Row>
							</Container>
							<Container className="tab1">
								<Row>
									<Col md="12" lg="12" className="cel2">
										<p className="centerText tabela2Text">
											5,0 - 9,0
										</p>
									</Col>
								</Row>
							</Container>
						</Col>
					</Row>
				</Container>*/}

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								As gestações gemelares apresentam demandas
								nutricionais e fisiológicas aumentadas quando
								comparadas a gestações únicas (BRASIL, 2012a).
								Veja na tabela a seguir as recomendações de
								ganho de peso em gestantes gemelares.
							</p>
						</Col>
					</Row>
				</Container>

				{/*<Col md="10" lg="10">
					<Container className="espacamentoBottom fundoTabUn1 borda10 tabEdited1">
						<Row>
							<Col md="2" lg="2" className="tabEdited">
								<Container className="tab1">
									<Row>
										<Col
											md="12"
											lg="12"
											className="cel1 cel1Tab2"
										>
											<p className="centerText tabela2Text alignVCenter titulo1">
												Estado Nutricional (kg/m²)
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												Baixo Peso (&#60; 18,5)
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												Adequado (18,5 a 24,5)
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												Sobrepeso (25 a 29,9)
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												Obesidade (&#8805; 30)
											</p>
										</Col>
									</Row>
								</Container>
							</Col>

							<Col md="2" lg="2" className="tabEdited">
								<Container className="tab1">
									<Row>
										<Col
											md="12"
											lg="12"
											className="cel1 cel1Tab2"
										>
											<p className="centerText tabela2Text alignVCenter titulo1">
												0-20 semanas
											</p>
										</Col>
									</Row>
								</Container>

								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												0,560 - 0,790
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												0,450 - 0,680
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												0,450 - 0,560
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												0,340 - 0,450
											</p>
										</Col>
									</Row>
								</Container>
							</Col>

							<Col md="2" lg="2" className="tabEdited">
								<Container className="tab1">
									<Row>
										<Col
											md="12"
											lg="12"
											className="cel1 cel1Tab2"
										>
											<p className="centerText tabela2Text alignVCenter titulo1">
												20 - 28 semanas
											</p>
										</Col>
									</Row>
								</Container>

								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												0,680 - 0,790
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												0,560 - 0,790
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												0,450 - 0,680
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												0,340 - 0,560
											</p>
										</Col>
									</Row>
								</Container>
							</Col>

							<Col md="2" lg="2" className="tabEdited">
								<Container className="tab1">
									<Row>
										<Col
											md="12"
											lg="12"
											className="cel1 cel1Tab2"
										>
											<p className="centerText tabela2Text alignVCenter titulo1">
												28 semanas ao parto
											</p>
										</Col>
									</Row>
								</Container>

								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												0,560
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												0,450
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												0,450
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												0,340
											</p>
										</Col>
									</Row>
								</Container>
							</Col>

							<Col md="2" lg="2" className="tabEdited">
								<Container className="tab1">
									<Row>
										<Col
											md="12"
											lg="12"
											className="cel1 cel1Tab2"
										>
											<p className="centerText tabela2Text alignVCenter titulo1">
												Ganho de peso total (kg)
											</p>
										</Col>
									</Row>
								</Container>

								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												22,5 - 27,9
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												18 - 24,3
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												17,1 - 21,2
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab1">
									<Row>
										<Col md="12" lg="12" className="cel2">
											<p className="centerText tabela2Text">
												13 - 17,1
											</p>
										</Col>
									</Row>
								</Container>
							</Col>
						</Row>
					</Container>
				</Col>*/}

				<Container>
					<Row>
						<Col md="12" lg="12">
							<img
								src={tabG2}
								className="responsive w100 borda10"
							/>
						</Col>
					</Row>
				</Container>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Em resumo, a recomendação de ganho de peso
								gestacional ocorre por trimestre em que:{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								<b>
									Primeiro trimestre (1 a 12 semanas
									gestacionais):
								</b>{" "}
								são situações previstas que não comprometem a
								saúde mãe/filho. Permite-se perda de peso de até
								3 kg, ou a manutenção do peso pré-gestacional ou
								um ganho de peso de até 2 kg.{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								<b>
									Segundo e terceiro trimestre (13 a 40
									semanas gestacionais):
								</b>{" "}
								o ganho de peso adequado vai depender do estado
								nutricional da gestante. De maneira
								simplificada, recomenda-se que gestantes com
								baixo peso ganhem em torno de 15kg, as
								eutróficas, entre 11,5 – 16 kg; e aquelas com
								sobrepeso entre 7 e 11,5kg e obesas 5 a 9 kg,
								durante todo o período gestacional.
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								<b>Gestante gemelar:</b> requer maior ganho de
								peso, que é de 18 kg – 23,4kg ou 680g/semana
								durante o segundo e terceiro trimestre.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								O ganho de peso materno abaixo dos níveis
								recomendados está associado ao baixo peso ao
								nascer, prematuridade, maior tempo de internação
								e, consequentemente, maiores custos relacionados
								à saúde. O ganho de peso excessivo, por outro
								lado, está associado a uma maior incidência de
								macrossomia, cirurgia cesariana, obesidade
								materna e infantil. Os impactos da obesidade
								materna na gestação, no parto, na saúde da
								mulher e no bebê, tendem a ser maiores quanto
								maior for a obesidade (CATALANO; SHANKAR, 2017).
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row></Row>
				</Container>
				<Container>
					<Row>
						<Col md="6" lg="6">
							<img
								src={img01}
								className="responsive w100 borda10 imgEdited"
							/>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								A saúde das gestantes e de seus bebês depende de
								uma nutrição adequada. Portanto, a literatura é
								consensual ao reconhecer que o estado
								nutricional materno é indicador de saúde e
								qualidade de vida tanto para a mulher quanto
								para o crescimento do seu filho, sobretudo no
								peso ao nascer, uma vez que a única fonte de
								nutrientes do bebê é constituída pelas reservas
								nutricionais e ingestão alimentar materna
								(CATALANO; SHANKAR, 2017).
							</p>
							<p>
								O seu papel, como profissional de saúde é
								entender que a avaliação do estado nutricional,
								antes e durante a gestação, serve como mecanismo
								de prevenção do ganho de peso excessivo, que
								levará em conta fatores como o peso
								pré-gestacional, hábitos alimentares da mãe e
								prática de atividade física regular.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								É muito importante que o processo de
								monitoramento e aconselhamento quanto ao estado
								nutricional e ganho de peso seja realizado com
								empatia e acolhimento. Explique para as
								gestantes que elas devem ficar tranquilas, pois
								ganhar peso é esperado e é possível fazê-lo de
								maneira saudável, para que tudo volte ao normal
								no tempo certo e seu bebê se desenvolva
								adequadamente.
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								<b>
									Impactos da obesidade e do ganho de peso
									gestacional excessivo na gestação e no parto
								</b>
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade" id="textoImgQuadro">
								O pior desfecho na gestação é a morte materna. O
								risco composto pela morte materna e morbidade
								materna grave aumenta conforme aumenta o grau de
								obesidade. Observe na tabela ao lado a relação
								entre o estado nutricional e o risco de
								morbidade materna grave. Dentre as morbidades
								maternas graves associadas ao ganho de peso
								excessivo na gestação podemos citar a
								necessidade de transfusão sanguínea por
								hemorragia, complicações cardíacas,
								respiratórias, cerebrais ou hematológicas
								graves, sepse, choque, falência renal ou
								hepática, complicações anestésicas e rotura
								uterina.
							</p>
						</Col>
						<Col md="6" lg="6">
							<img
								src={quadro6}
								className="responsive w100 borda10"
								style={{
									height: this.state.height02 + "px"
								}}
							/>
							{/*
							<div className="espacamentoBottom">
								<Container className="tab">
									<Row>
										<Col md="6" lg="6" className="cel2">
											<p className="centerText">
												Sem excesso de peso
											</p>
										</Col>

										<Col md="6" lg="6" className="cel2">
											<p className="centerText">
												IMC 143/10.000
											</p>
										</Col>
									</Row>
								</Container>

								<Container className="tab">
									<Row>
										<Col md="6" lg="6" className="cel2">
											<p className="centerText">
												Com sobrepeso
											</p>
										</Col>

										<Col md="6" lg="6" className="cel2">
											<p className="centerText">
												160/10.000
											</p>
										</Col>
									</Row>
								</Container>

								<Container className="tab">
									<Row>
										<Col md="6" lg="6" className="cel2">
											<p className="centerText">
												Com obesidade grau I{" "}
											</p>
										</Col>

										<Col md="6" lg="6" className="cel2">
											<p className="centerText">
												168/10.000
											</p>
										</Col>
									</Row>
								</Container>

								<Container className="tab">
									<Row>
										<Col md="6" lg="6" className="cel2">
											<p className="centerText">
												Com obesidade grau II{" "}
											</p>
										</Col>

										<Col md="6" lg="6" className="cel2">
											<p className="centerText">
												178/10.000
											</p>
										</Col>
									</Row>
								</Container>
								<Container className="tab">
									<Row>
										<Col md="6" lg="6" className="cel2">
											<p className="centerText">
												Com obesidade grau III
											</p>
										</Col>

										<Col md="6" lg="6" className="cel2">
											<p className="centerText">
												203/10.000
											</p>
										</Col>
									</Row>
								</Container>
							</div>*/}
						</Col>
					</Row>
				</Container>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12"></Col>
					</Row>
				</Container>

				<DivHide
					textoClique={[
						<b>Clique aqui </b>,
						" e conheça os riscos do excesso de peso"
					]}
					textoCaixa={
						<div>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											<b>
												Riscos de morbidade materna para
												mulheres:
											</b>
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											O ganho de peso gestacional
											excessivo pode ainda aumentar o
											risco de:
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="6" lg="6">
										<ul>
											<li>
												<p className="textoUnidade">
													perda gestacional precoce e
													mais de um aborto;
												</p>
											</li>
											<li>
												<p className="textoUnidade">
													diabetes gestacional e
													diabetes prévio
													diagnosticado na gestação;
												</p>
											</li>
										</ul>
									</Col>
									<Col md="6" lg="6">
										<ul>
											<li>
												<p className="textoUnidade">
													quadros hipertensivos
													relacionados com a gestação;
												</p>
											</li>
											<li>
												<p className="textoUnidade">
													parto prematuro (espontâneo
													e por indicação médica);
												</p>
											</li>
										</ul>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="6" lg="6">
										<ul>
											<li>
												<p className="textoUnidade">
													tromboembolismo;
												</p>
											</li>
											<li>
												<p className="textoUnidade">
													gestação pós-data;
												</p>
											</li>
											<li>
												<p className="textoUnidade">
													apneia obstrutiva;
												</p>
											</li>
											<li>
												<p className="textoUnidade">
													gemelaridade dizigótica;
												</p>
											</li>
										</ul>
									</Col>
									<Col md="6" lg="6">
										<ul>
											<li>
												<p className="textoUnidade">
													quadros hipertensivos
													relacionados com a gestação;
												</p>
											</li>
											<li>
												<p className="textoUnidade">
													síndrome do túnel do carpo;
												</p>
											</li>
											<li>
												<p className="textoUnidade">
													dor pélvica no final da
													gestação;
												</p>
											</li>
										</ul>
									</Col>
								</Row>
							</Container>
							{/*<Container>
					<Row>
						<Col md="12" lg="12">
							<ul>
								<li>
									<p className="textoUnidade">
										gestação pós-data;
									</p>
								</li>
								<li>
									<p className="textoUnidade">
										gemelaridade dizigótica;
									</p>
								</li>
								<li>
									<p className="textoUnidade">
										quadros hipertensivos relacionados com a
										gestação;
									</p>
								</li>
								<li>
									<p className="textoUnidade">
										apneia obstrutiva;
									</p>
								</li>
								<li>
									<p className="textoUnidade">
										síndrome do túnel do carpo;
									</p>
								</li>
								<li>
									<p className="textoUnidade">
										dor pélvica no final da gestação.
									</p>
								</li>
							</ul>
						</Col>
					</Row>
				</Container>*/}
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											<b>
												Riscos no processo de trabalho
												de parto, parto e cirurgia
												cesariana
											</b>
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											Embora o atendimento ao trabalho de
											parto e parto ou cirurgia cesariana
											não faça parte do cotidiano dos
											profissionais da Atenção Primária à
											Saúde, é importante que você conheça
											algumas complicações que as mulheres
											com sobrepeso e obesidade podem
											enfrentar nessa parte do processo
											para informá-las e providenciar
											encaminhamento para o serviço de
											referência, quando necessário.
											Acompanhe:
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<ul>
											<li>
												<p className="textoUnidade">
													tendem a ter um trabalho de
													parto mais longo
													independente do peso do
													bebê;
												</p>
											</li>
											<li>
												<p className="textoUnidade">
													quando é necessário realizar
													uma indução de parto
													(provocar modificações no
													colo do útero e/ou
													contrações uterinas de
													maneira artificial,
													geralmente com medicamentos)
													esta indução tem mais chance
													de funcionar quando a mulher
													tem estado nutricional e
													ganho de peso gestacional
													adequado;
												</p>
											</li>
											<li>
												<p className="textoUnidade">
													apresentam mais
													probabilidade de serem
													submetidas a cirurgia
													cesariana.{" "}
												</p>
											</li>
										</ul>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											<b>
												Riscos do ganho de peso
												excessivo para o bebê
											</b>
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											Os filhos de mães obesas tem risco
											aumentado para:
										</p>
									</Col>
								</Row>
							</Container>
							<div className="">
								<Container>
									<Row>
										<Col md="6" lg="6">
											<ul>
												<li>
													<p className="textoUnidade">
														anomalias congênitas;
													</p>
												</li>
												<li>
													<p>
														outras patologias na
														idade adulta (cardíacas,
														metabólicas);
													</p>
												</li>
												<li>
													<p className="textoUnidade">
														prematuridade;
													</p>
												</li>
											</ul>
										</Col>
										<Col md="6" lg="6">
											<ul>
												<li>
													<p className="textoUnidade">
														obesidade na infância e
														na vida adulta;
													</p>
												</li>
												<li>
													<p className="textoUnidade">
														asfixia perinatal e
														óbito fetal;
													</p>
												</li>
												<li>
													<p className="textoUnidade">
														asma;
													</p>
												</li>
											</ul>
										</Col>
									</Row>
								</Container>

								<Container>
									<Row>
										<Col md="12" lg="12">
											<ul>
												<li>
													<p className="textoUnidade">
														feto grande para a idade
														gestacional – fetos
														grandes para a idade
														gestacional aumentam o
														risco de cesariana, de
														complicações durante o
														parto, como a distócia
														de ombros e também tem
														uma predisposição maior
														a serem obesos na idade
														adulta.
													</p>
												</li>
											</ul>
										</Col>
									</Row>
								</Container>
							</div>
						</div>
					}
				/>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Você já ouviu falar nos primeiros 1.000 dias de
								vida e sobre o impacto positivo de se manter uma
								alimentação saudável e adequada na gestação na
								prevenção de sobrepeso e obesidade no bebê para
								o resto da sua vida? Há estudos atualmente que
								revelam, inclusive, que um desajuste alimentar
								intra-útero pode levar a disfunções tanto ao
								nascimento quanto na fase adulta, como a maior
								tendência à obesidade, hipertensão, diabetes,
								etc.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Os primeiros 1.000 dias de vida começam com a
								gravidez da mulher e seguem até os dois anos de
								vida do bebê, representando o melhor momento
								para a prevenção da obesidade e suas
								consequências adversas (PIETROBELLI; AGOSTI,
								2017). Há muitos impulsionadores do crescimento
								durante essa fase complexa da vida, entre eles
								os que estão expostos a seguir, confira!
							</p>
						</Col>
						<Col md="6" lg="6" className="growth">
							<Row>
								<Col md="4" lg="4">
									<p className="textoUnidade textGrowth0">
										Alguns impulsionadores do crescimento
										nos primeiros 1000 dias
									</p>
								</Col>
								<Col md="8" lg="8">
									<p className="textoUnidade textGrowth1">
										Regulação Hormonal
									</p>
									<p className="textoUnidade textGrowth2">
										Nutrição
									</p>
									<p className="textoUnidade textGrowth3">
										Fatores genéticos e epigenéticos
									</p>
								</Col>
							</Row>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								O desafio envolve, portanto, maximizar o
								potencial de crescimento normal sem aumentar o
								risco de distúrbios associados. Estudos recentes
								indicam fortes evidências de que o alto índice
								de massa corporal pré-gestacional, o ganho de
								peso gestacional excessivo, diabetes gestacional
								e exposição ao tabaco proporcionam um aumento no
								risco de obesidade infantil.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo3"
					}
					titulo={
						"IMPACTO DO ESTADO NUTRICIONAL DA GESTANTE E O PÓS-PARTO"
					}
				/>
				<Container className="" id="sec13">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								O acompanhamento contínuo do estado nutricional,
								durante a gestação, contribui para o ganho de
								peso ideal durante esse período, evitando o
								excesso e a retenção de peso no pós-parto, que
								são determinantes importantes do excesso de peso
								para a mulher. O pós-parto de mulheres obesas
								apresenta alguns riscos clínicos para os quais
								as equipes de saúde da Atenção Primária à Saúde
								devem estar atentas. Clique nas barras abaixo
								para conhecer cada uma delas:
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<Accordion
								title={
									<p className="">
										<b>Retenção de peso pós-parto</b>
									</p>
								}
								content={
									<div className="fundoA11 borda10 espacamento">
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p>
														O número de mulheres que
														iniciam a gestação com
														excesso de peso ou que
														ganham peso excessivo
														durante a gravidez é
														cada vez mais
														expressivo. Um estudo
														realizado em seis
														capitais brasileiras,
														com 5.564 gestantes,
														encontrou uma
														prevalência de 19,2% de
														sobrepeso e de 5,5% de
														obesidade entre as
														gestantes. A retenção do
														peso ganho se mostrou um
														fator determinante da
														obesidade em mulheres, e
														seu desenvolvimento.
														Estudos importantes
														demonstraram o efeito do
														ganho de peso
														gestacional elevado
														assim como do estado
														nutricional
														pré-gestacional
														inadequado, no aumento
														da retenção de peso
														pós-parto, que, por sua
														vez, pode perpetuar um
														quadro de obesidade e de
														comorbidades associadas
														para o resto da vida da
														mulher. Estes dados
														reforçam a importância
														da atuação no
														monitoramento e na
														orientação à gestante
														quanto ao ganho de peso
														adequado e saudável ao
														longo da gravidez.
													</p>
												</Col>
											</Row>
										</Container>
									</div>
								}
							/>
							<Container className="espacamentoAccordion">
								<Row>
									<Col md="12" lg="12"></Col>
								</Row>
							</Container>

							<Accordion
								title={
									<p>
										<b>Tromboembolismo venoso</b>
									</p>
								}
								content={
									<div className="fundoA11 borda10 espacamento">
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														A obesidade em si e o
														período puerperal são
														fatores de risco para o
														tromboembolismo venoso.
														Se o nascimento ocorreu
														por cesariana, soma-se
														mais um fator de risco.
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														As chances de ter um
														evento tromboembólico no
														pós-parto são maiores
														quanto maior for o grau
														de obesidade. Aumentando
														2,5, 2,9 e 4,6 vezes
														para mulheres com
														obesidade classe I, II e
														III respectivamente em
														comparação com as
														mulheres sem obesidade.
														O ganho de peso
														excessivo durante a
														gestação (mais de 22
														quilos) também aumenta o
														risco para eventos
														tromboembólicos em 1,5
														vezes (LISONKOVA et al.,
														2017).
													</p>
												</Col>
											</Row>
										</Container>
									</div>
								}
							/>

							<Container>
								<Row>
									<Col
										md="12"
										lg="12"
										className="espacamentoAccordion"
									></Col>
								</Row>
							</Container>

							<Accordion
								title={
									<p>
										<b>Infecção</b>
									</p>
								}
								content={
									<div className="fundoA11 borda10 espacamento">
										<Container id="sec14">
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														As mulheres com
														obesidade têm um risco
														aumentado de
														apresentarem quadros
														infecciosos no
														pós-parto. Independente
														da via de parto e apesar
														do uso de antibióticos
														profiláticos na
														cesariana.{" "}
													</p>
												</Col>
											</Row>
										</Container>
									</div>
								}
							/>
							<Container>
								<Row>
									<Col
										md="12"
										lg="12"
										className="espacamentoAccordion"
									></Col>
								</Row>
							</Container>
							<Accordion
								title={
									<p>
										<b>Depressão Pós-parto</b>
									</p>
								}
								content={
									<div className="fundoA11 borda10 espacamento">
										<Container id="sec14">
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														As desordens psíquicas
														não são raras nessa fase
														da vida, para todas as
														mulheres. Para adultos
														em geral, a obesidade
														está associada com
														distúrbios mentais como
														depressão, transtornos
														alimentares e transtorno
														bipolar. Uma revisão
														sistemática de 2014
														encontrou uma
														possibilidade maior de
														distúrbios depressivos
														na gestação e no pós
														parto entre mulheres com
														obesidade (MOLYNEAUX et
														al., 2014).
													</p>
												</Col>
											</Row>
										</Container>
									</div>
								}
							/>
						</Col>
					</Row>
				</Container>

				{/*<div className="slider">
					<Container>
						<Row>
							<Col
								md="12"
								lg="12"
								className="espacamentoBottom espacamento"
							>
								<Carousel
									activeIndex={activeIndex}
									next={this.next}
									previous={this.previous}
									pause={false}
									ride="carousel"
									interval={false}
									slide={true}
								>
									<CarouselIndicators
										items={items}
										activeIndex={activeIndex}
										onClickHandler={this.goToIndex}
									/>
									{slides}
									<CarouselControl
										direction="prev"
										directionText="Previous"
										onClickHandler={this.previous}
										className="seta"
									/>
									
									<CarouselControl
										direction="next"
										directionText="Next"
										onClickHandler={this.next}
										className="seta"
									/>
								</Carousel>
							</Col>
						</Row>
					</Container>
				</div>	*/}

				{/*<div className="">
					<Container>
						<Row>
							<Col md="8" lg="8">
								<p className="textoUnidade" id="titleImg1">
									<b>Retenção de peso pós-parto</b>
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="8" lg="8">
								<p className="textoUnidade">
									O número de mulheres que iniciam a gestação
									com excesso de peso ou que ganham peso
									excessivo durante a gravidez é cada vez mais
									expressivo. Um estudo realizado em seis
									capitais brasileiras, com 5.564 gestantes,
									encontrou uma prevalência de 19,2% de
									sobrepeso e de 5,5% de obesidade entre as
									gestantes. A retenção do peso ganho se
									mostrou um fator determinante da obesidade
									em mulheres, e seu desenvolvimento. Estudos
									importantes demonstraram o efeito do ganho
									de peso gestacional elevado assim como do
									estado nutricional pré-gestacional
									inadequado, no aumento da retenção de peso
									pós-parto, que, por sua vez, pode perpetuar
									um quadro de obesidade e de comorbidades
									associadas para o resto da vida da mulher.
									Estes dados reforçam a importância da
									atuação no monitoramento e na orientação à
									gestante quanto ao ganho de peso adequado e
									saudável ao longo da gravidez.
								</p>
							</Col>
						</Row>
					</Container>


					<Container>
						<Row>
							<Col md="8" lg="8">
								<p className="textoUnidade" id="titleImg2">
									<b>Tromboembolismo venoso</b>
								</p>
							</Col>
						</Row>
					</Container>

					<Container>
						<Row>
							<Col md="8" lg="8">
								<p className="textoUnidade">
									A obesidade em si e o período puerperal são
									fatores de risco para o tromboembolismo
									venoso. Se o nascimento ocorreu por
									cesariana, soma-se mais um fator de risco.
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="8" lg="8">
								<p className="textoUnidade">
									As chances de ter um evento tromboembólico
									no pós-parto são maiores quanto maior for o
									grau de obesidade. Aumentando 2,5, 2,9 e 4,6
									vezes para mulheres com obesidade classe I,
									II e III respectivamente em comparação com
									as mulheres sem obesidade. O ganho de peso
									excessivo durante a gestação (mais de 22
									quilos) também aumenta o risco para eventos
									tromboembólicos em 1,5 vezes (LISONKOVA et
									al., 2017).
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="8" lg="8">
								<p className="textoUnidade" id="titleImg3">
									<b>Infecção</b>
								</p>
							</Col>
						</Row>
					</Container>
					<Container id="sec14">
						<Row>
							<Col md="8" lg="8">
								<p className="textoUnidade">
									As mulheres com obesidade têm um risco
									aumentado de apresentarem quadros
									infecciosos no pós-parto. Independente da
									via de parto e apesar do uso de antibióticos
									profiláticos na cesariana.{" "}
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="8" lg="8">
								<p className="textoUnidade" id="titleImg4">
									<b>Depressão Pós-parto</b>
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="8" lg="8">
								<p className="textoUnidade">
									As desordens psíquicas não são raras nessa
									fase da vida, para todas as mulheres. Para
									adultos em geral, a obesidade está associada
									com distúrbios mentais como depressão,
									transtornos alimentares e transtorno
									bipolar. Uma revisão sistemática de 2014
									encontrou uma possibilidade maior de
									distúrbios depressivos na gestação e no pós
									parto entre mulheres com obesidade
									(MOLYNEAUX et al., 2014).
								</p>
							</Col>
						</Row>
					</Container>
				</div>*/}

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								O objetivo não deve ser focado na perda de peso
								durante a gestação para diminuir esses fatores
								de risco. A perda de peso não é alcançada pela
								maioria das mulheres e não há conhecimento na
								literatura suficiente para afirmar se a perda de
								peso durante a gestação modifica ou não esses
								fatores de risco. Essa população precisa de
								atenção redobrada para o diagnóstico e
								encaminhamento de possíveis complicações como
								diabetes, hipertensão e tromboembolismo.
							</p>
						</Col>
					</Row>
				</Container>
				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo3"
					}
					titulo={
						"ALIMENTAÇÃO DA GESTANTE NA PREVENÇÃO DO SOBREPESO E OBESIDADE"
					}
				/>
				<Container className="" id="sec15">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade" id="textoImg01">
								Antes de abordarmos os aspectos voltados à
								alimentação na gestação, precisamos definir o
								que é uma alimentação adequada e saudável, e
								qual é a nossa função enquanto profissionais da
								saúde na sua promoção dentro da Atenção Primária
								à Saúde. No Brasil, a alimentação adequada e
								saudável “é um direito humano básico que envolve
								a garantia ao acesso permanente e regular, de
								forma socialmente justa, a uma prática alimentar
								adequada aos aspectos biológicos e sociais do
								indivíduo” (BRASIL, 2013a).{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<div id="imgRolagemDeFundo1">
					<Container>
						<Row>
							<Col
								md="11"
								lg="11"
								className="caixaFundo borda10 espacoLeftFundoAzul"
							>
								<p className="textoUnidade espacamento2x espacamentoLateral">
									A alimentação saudável proporciona o
									sentimento de pertencimento social das
									pessoas, favorece a autonomia nas escolhas
									alimentares, na redescoberta de novas formas
									de colocar à mesa alimentos saudáveis, de
									preparar sua própria refeição, com o prazer
									propiciado pela alimentação e,
									consequentemente, com o seu estado de
									bem-estar. Mas essa autonomia só pode ser
									adquirida através de informação de
									qualidade, que deve ser repassada por você,
									profissional da rede, nas consultas e
									atividades coletivas realizados na Unidade
									Básica de Saúde.{" "}
								</p>
							</Col>
						</Row>
					</Container>
				</div>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								A adoção de uma alimentação saudável diz
								respeito à ingestão de alimentos que possuem sua
								composição nutricional balanceada; mas, é
								importante lembrar também, que a combinação dos
								alimentos entre si e como são preparados diz
								também respeito a características do modo de
								comer e às dimensões culturais e sociais das
								práticas alimentares, contribuindo de maneira
								essencial para a boa saúde da gestante.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade" id="textoImg02">
								A alimentação adequada e saudável ao longo do
								período gestacional exerce um papel ainda mais
								importante, pois é capaz de determinar os
								desfechos relacionados à mãe e ao bebê. Observe no quadro a seguir alguns benefícios da alimentação saudável durante a gestação.{" "}
							</p>
							<p id="textoImg03">
								Com uma alimentação equilibrada, a mãe pode
								diminuir os riscos de complicações na gravidez,
								além de modular a presença de outros
								desconfortos típicos desse período (enjoos e
								constipação intestinal).
							</p>
						</Col>
						<Col md="6" lg="6">
							<img
								src={info04}
								className="responsive borda10"
								style={{
									height: this.state.heightN + "px"
								}}
							/>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								A dieta habitual dos brasileiros é composta por
								diversas influências e na atualidade é
								fortemente caracterizada por uma combinação de
								uma dieta “tradicional” (baseada no arroz com
								feijão) em conjunto com alimentos classificados
								como ultraprocessados, com altos teores de
								gorduras, sódio e açúcar e com baixo teor de
								micronutrientes e alto conteúdo calórico. O
								consumo médio de frutas e hortaliças ainda é
								metade do valor recomendado pelo Guia Alimentar
								para a População Brasileira (BRASIL, 2014) e
								manteve-se estável na última década, enquanto
								{" "}<ModalPopUp
									conceito={"alimentos ultraprocessados"}
									textoConceito={[
										<p>
											<b>Alimentos ultraprocessados:</b> são
											alimentos produzidos com a adição de
											muitos ingredientes como sal,
											açúcar, óleos, gorduras, proteínas
											de soja, do leite, extratos de
											carne, além de substâncias
											sintetizadas em laboratório a partir
											de alimentos e de outras fontes
											orgânicas como petróleo e carvão.
											Assim, tais alimentos têm prazo de
											validade maior, alteração de cor,
											sabor, aroma e textura. São exemplos
											de ultraprocessados: biscoitos
											recheados, salgadinhos “de pacote”,
											refrigerantes e macarrão
											“instantâneo”.
										</p>
									]}
								/>{" "}
								, como doces e refrigerantes, têm o seu consumo
								aumentado a cada ano.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Percebe-se que o padrão de consumo também varia
								de acordo com a idade. Entre as pessoas mais
								jovens, é maior o consumo de alimentos
								ultraprocessados, o que tende a diminuir com o
								aumento da idade, acontecendo o inverso, ou
								seja, pessoas com mais idade tendem a consumir
								mais frutas e hortaliças. Adolescentes são o
								grupo com pior perfil da dieta, com as menores
								frequências de consumo de feijão, saladas e
								verduras em geral, apontando para um prognóstico
								de aumento do excesso de peso e doenças crônicas
								(BRASIL, 2013b). Essas informações devem ser
								levadas em consideração, dando um enfoque ainda
								maior nas orientações voltadas a uma alimentação
								saudável e redução do consumo de alimentos
								ultraprocessados entre gestantes adolescentes.
							</p>
						</Col>
					</Row>
				</Container>
				<div className="fundo01b">
					<Container className="espacamento">
						<Row>
							<Col md="12" lg="12">
								<div className="triangle1"></div>
								<p className="textoUnidade textOverlap">
									O estilo de vida atual favorece um maior
									número de refeições realizadas fora do
									domicílio. O padrão de alimentação entre
									pessoas que fazem refeições fora do
									domicílio, na maioria dos casos, está
									constituído por alimentos industrializados e
									ultraprocessados como refrigerantes,
									cerveja, sanduíches, salgados e salgadinhos
									industrializados, imprimindo um padrão de
									alimentação que, muitas vezes, é repetido no
									domicílio.
								</p>
							</Col>
						</Row>
					</Container>

					<Container>
						<Row>
							<Col md="6" lg="6">
								<div className="triangle1"></div>
								<p className="textoUnidade textOverlap">
									Nota-se uma relação entre o consumo de
									alimentos ultraprocessados com maiores
									riscos de diabetes, hipertensão, obesidade,
									câncer, dentre outras doenças crônicas não
									transmissíveis. Em gestantes, o consumo
									excessivo de alimentos ultraprocessados está
									associado a um maior ganho de peso
									gestacional e com um percentual de gordura
									corporal elevado no neonato, o que serve de
									alerta para orientarmos as gestantes quanto
									aos possíveis perigos desses alimentos na
									sua rotina alimentar (ROHATGI et al., 2017).
									Por isso, o consumo desses alimentos pela
									gestante deve ser desencorajado.
								</p>
							</Col>
							<Col md="6" lg="6">
								<div className="triangle3"></div>
								<p className="textoUnidade textOverlap">
									A gravidez é um dos melhores momentos para
									se pensar em alimentação saudável, pois não
									só a mãe se beneficiará, como, também, o
									bebê. Uma mãe bem nutrida é capaz de
									fornecer todos os nutrientes necessários e
									pode proporcionar as condições ideais para o
									desenvolvimento de seu filho. A alimentação
									adequada e saudável durante a gestação
									conseguirá garantir os nutrientes
									necessários para um estado nutricional
									adequado da mulher nas semanas iniciais de
									puerpério e na garantia das reservas
									nutricionais da mesma.
								</p>
							</Col>
						</Row>
					</Container>

					<Container>
						<Row>
							<Col md="12" lg="12">
								<div className="triangle2"></div>
								<p className="textoUnidade textOverlap2">
									A orientação da gestante quanto à adoção de
									uma alimentação adequada e saudável, que
									supra suas necessidades nutricionais e
									garanta o ganho de peso adequado e saudável,
									são os principais objetivos dos cuidados dos
									profissionais da rede de Atenção Primária à
									Saúde. Dessa forma, é importante que você e
									sua equipe utilizem estratégias de educação
									alimentar e nutricional e de promoção da
									alimentação saudável para valorizar as
									referências presentes na cultura alimentar
									da gestante e/ou de sua família, de modo a
									contribuir no processo de prevenção de
									sobrepeso e obesidade nessa fase da vida, e
									consequentemente, de diversas Doenças
									Crônicas Não Transmissíveis (DCNT).
								</p>
							</Col>
						</Row>
					</Container>
				</div>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								O encorajamento ao consumo de frutas e vegetais
								na alimentação da gestante deve ser uma prática
								constante, pois são ótimas fontes de vitaminas,
								minerais e fibras e, no caso das gestantes, são
								essenciais para a formação saudável do feto e a
								proteção da saúde materna (BRASIL, 2012).
								Estudos associam o consumo diário de frutas e
								vegetais na gestação com uma redução do risco de
								diabetes gestacional e uma melhor adequação do
								ganho de peso ao longo da gestação (SCHOENAKER
								et al., 2016). Em relação aos sucos de fruta,
								apesar de naturais, existe uma controversa entre
								a sua relação com o ganho de peso gestacional,
								fato pelo qual devemos incentivar a priorizar o
								consumo da fruta <i>in natura</i> ao invés dos
								sucos. {/*Saiba mais sobre este tema
								<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaDestaque"}
									icone={iconeDestaque}
									textoUnidade={
										"A gestante não deve nem precisa “comer por dois”, as recomendações alimentares devem seguir os princípios da população brasileira."
									}
									inserirLink={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>{" "}*/}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Já sabemos que, durante a gravidez, uma
								alimentação saudável é essencial para assegurar
								as necessidades nutricionais da mãe e do bebê.
								No entanto, a mãe não deve ser encorajada a
								“comer por dois” e, sim, a manter uma
								alimentação equilibrada e sem excessos.
							</p>
						</Col>
					</Row>
				</Container>

				<div className="">
					<Container className="fundoMulheres borda10">
						<Row>
							<Col md="6" lg="6" className="">
								<p className="textoUnidade espacamentoMulheres">
									As recomendações alimentares e nutricionais
									devem adaptar-se a cada mulher,
									considerando-se:
								</p>

								<ul className="cobrinhas espacamentoMulheresBullets">
									<li>
										<p>as diferenças individuais</p>
									</li>
									<li>
										<p>os padrões alimentares</p>
									</li>
									<li>
										<p>a cultura alimentar</p>
									</li>
								</ul>
							</Col>
							<Col md="6" lg="6">
								<img src={mulheres} className="mulheres" />
							</Col>
						</Row>
					</Container>
				</div>

				{/*<Container className="espacamentoBottom">
					<Row>
						<Col md="12" lg="12" className="womenSpace">
							<p className="textoUnidade text1">
								As recomendações alimentares e nutricionais
								devem adaptar-se a cada mulher, considerando-se:
							</p>
							<p className="textoUnidade text2">
								as diferenças individuais<br></br>
								os padrões alimentares<br></br>a cultura
								alimentar<br></br>
							</p>
						</Col>
					</Row>
				</Container>*/}

				<Container className="espacamento">
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Desta forma, recomenda-se adoção de um estilo de
								vida saudável, que deve iniciar-se mesmo antes
								da gravidez, para otimizar a saúde da mãe e
								reduzir o risco de sobrepeso e obesidade, de
								complicações durante a gravidez e de algumas
								doenças no bebê. As recomendações alimentares,
								de maneira geral, seguem as recomendações
								alimentares para toda a população e o
								profissional de saúde deve basear-se nas
								diretrizes e nos princípios do Guia Alimentar
								para a População Brasileira (BRASIL, 2014).
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								É importante, também, entender a diferença entre
								restrição alimentar e alimentação adequada e
								saudável durante a gestação. Ao longo da
								gestação, a sua atuação deve centrar-se na
								orientação de uma alimentação adequada e
								saudável, com base em alimentos <i>in natura</i>{" "}
								ou minimamente processados, preparações
								culinárias realizadas no âmbito familiar, com
								predomínio de frutas, verduras, vegetais,
								tubérculos, leguminosas, carnes, ovos, leite e
								derivados, evitando o consumo de alimentos
								ultraprocessados.
							</p>
						</Col>
					</Row>
				</Container>
				<div className="fundoTransparencia">
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									No entanto, não devemos restringir a
									alimentação nem o consumo de calorias
									visando uma perda de peso nesse período, já
									que o objetivo principal é o ganho de peso
									adequado.
								</p>
							</Col>
						</Row>
					</Container>

					<TituloSecaoBarraNova
						tipoBarra={
							this.props.tipoUnidade +
							" espacamento degradeTitulo2"
						}
						titulo={"Encerramento da unidade"}
					/>

					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									Nesta unidade, aprendemos sobre a
									importância da garantia de um estado
									nutricional adequado, tanto na fase
									pré-gestacional como ao longo da gestação
									para a prevenção de sobrepeso e obesidade ao
									longo da vida da mulher. Verificamos que o
									impacto da má alimentação da gestante pode
									ser decisivo em relação aos desfechos no
									parto, na maior retenção de peso no
									pós-parto, e na manutenção do sobrepeso e da
									obesidade ao longo da vida, trazendo consigo
									um aumento no risco de Doenças Crônicas Não
									Transmissíveis (DCNT), tanto para a mãe
									quanto para o bebê.
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									Esperamos que as informações aqui
									encontradas tenham sido relevantes para
									atualizá-lo e sensibilizá-lo quanto à
									importância do seu papel na orientação e no
									cuidado da gestante ao longo do
									acompanhamento de pré-natal para prevenção
									do sobrepeso e da obesidade e garantia de
									uma alimentação adequada e saudável.									
								</p>
							</Col>
						</Row>
					</Container>
				</div>
			</div>
		);
	}
}

const Caixa1 = () => (
	<Container className="espacamentoBottom">
		<Row>
			<Col md="12" lg="12">
				<CaixaTemplate
					textoClique={"clicando no ícone"}
					classImg={"iconeTexto12_2"}
					tipoCaixa={"caixaDestaque"}
					icone={iconeDestaque}
					textoUnidade={
						"Como tornar hábito uma alimentação saudável, utilizando os recursos econômicos disponíveis e estimulando o consumo dos alimentos produzidos localmente?"
					}
					inserirLink={false}
					proporcaoCol1={1}
					proporcaoCol2={11}
				></CaixaTemplate>
			</Col>
		</Row>
	</Container>
);

const Caixa2 = () => (
	<Container>
		<Row>
			<Col md="12" lg="12">
				<CaixaTemplate
					textoClique={"clicando no ícone"}
					classImg={"iconeTexto12_2"}
					tipoCaixa={"caixaReflexao"}
					icone={iconeReflexao}
					textoUnidade={
						"e acesse outras informações sobre os critérios para classificação do estado nutricional, de acordo com os índices antropométricos da criança."
					}
					inserirLink={true}
					link={
						"http://bvsms.saude.gov.br/bvs/publicacoes/estrategias_cuidado_doenca_cronica_obesidade_cab38.pdf"
					}
					inserirLinkInicio={true}
					textoLink={"Clique aqui "}
					proporcaoCol1={1}
					proporcaoCol2={11}
				></CaixaTemplate>
			</Col>
		</Row>
	</Container>
);

export default Unidade;
