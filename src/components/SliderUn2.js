import React from "react";
import "./css/Unidade.css";
import "./css/Utilitarios.css";
import "font-awesome/css/font-awesome.min.css";
import {
	Container,
	Row,
	Col,
	Button,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Carousel,
	CarouselItem,
	CarouselControl,
	CarouselIndicators,
	CarouselCaption
} from "reactstrap";

import seta from "../img/seta.png";

import Mooc3v268 from "../img/un2/Mooc3v2-68.png";
import Mooc3v269 from "../img/un2/Mooc3v2-69.png";

import "./css/Modal.css";
import "./css/Slider.css";

const items = [
	{
		texto: (
			<div className="fundoSliderTextoUnT">
				<Container>
					<Row>
						<Col md="12" lg="12">
							<img
								src={Mooc3v268}
								className="responsive w100 borda10"
							/>
						</Col>
					</Row>
				</Container>
			</div>
		)
	},
	{
		texto: (
			<div className="fundoSliderTextoUnT">
				
					<Container>
						<Row>
							<Col md="12" lg="12">
								<img
									src={Mooc3v269}
									className="responsive w100 borda10"
								/>
							</Col>
						</Row>
					</Container>
				

				{/*<Col md="4" lg="4">
					<img src={require("../img/frutas.jpeg")} className="responsive w100 imgEdited"/>
				</Col> */}
			</div>
		)
	}
];

class Slider extends React.Component {
	constructor() {
		super();
		this.state = { activeIndex: 0, isHiddenCaixa: true };
		this.next = this.next.bind(this);
		this.previous = this.previous.bind(this);
		this.goToIndex = this.goToIndex.bind(this);
		this.onExiting = this.onExiting.bind(this);
		this.onExited = this.onExited.bind(this);
	}

	onExiting() {
		this.animating = true;
	}

	onExited() {
		this.animating = false;
	}

	next() {
		if (this.animating) return;
		const nextIndex =
			this.state.activeIndex === items.length - 1
				? 0
				: this.state.activeIndex + 1;
		this.setState({ activeIndex: nextIndex });
	}

	previous() {
		if (this.animating) return;
		const nextIndex =
			this.state.activeIndex === 0
				? items.length - 1
				: this.state.activeIndex - 1;
		this.setState({ activeIndex: nextIndex });
	}

	goToIndex(newIndex) {
		if (this.animating) return;
		this.setState({ activeIndex: newIndex });
	}

	render() {
		const { activeIndex } = this.state;

		const slides = items.map(item => {
			return (
				<CarouselItem
					onExiting={this.onExiting}
					onExited={this.onExited}
					key={item.texto}
				>
					<Container>
						<Row>
							<Col md="12" lg="12">
								{item.texto}
							</Col>
						</Row>
					</Container>
				</CarouselItem>
			);
		});

		return (
			<div className="slider">
				<Container>
					<Row>
						<Col
							md="12"
							lg="12"
							className="espacamentoBottom espacamento"
						>
							<Carousel
								activeIndex={activeIndex}
								next={this.next}
								previous={this.previous}
								pause={false}
								ride="carousel"
								interval={false}
								slide={true}
							>
								<CarouselIndicators
									items={items}
									activeIndex={activeIndex}
									onClickHandler={this.goToIndex}
									className="espacamento zIndex"
								/>

								{slides}
								<br />
								<br />
								<br />
							</Carousel>
							<CarouselControl
								direction="prev"
								directionText="Previous"
								onClickHandler={this.previous}
								className=""
							>
								<img
									src={seta}
									className="responsive w100 setaIndex"
								/>
							</CarouselControl>
							{/*TODO: colocar seta aqui*/}
							<CarouselControl
								direction="next"
								directionText="Next"
								onClickHandler={this.next}
								className=""
							/>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default Slider;
