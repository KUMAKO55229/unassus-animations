/* eslint-disable import/first */
import React from "react";
import Testeira from "./TesteiraCreditos.js";
import CapaUnidade from "./capa/CapaPP.js";
import Sumario from "./Sumario.js";
import Creditos from "./Creditos.js";
import menuHamburguer from "../img/menuObesidade2.png";

const testeira = {
  nomeCursoPt1: "Promoção do Ganho de Peso",
  nomeCursoPt2: "Adequado na Gestação",
  nomeUnidadePt1: "GESTAÇÃO: IMPORTÂNCIA DO ESTADO NUTRICIONAL,",
  nomeUnidadePt2: "ALIMENTAÇÃO E NUTRIÇÃO",
  tipoBarra: "barraHamburguerLogo"
};



const unidade = {
  tipoUnidade: "barraUnidade",
  corBarra: "yellow"
};

function DescritorCreditos() {
  return (
    <div className="DescritorUnidade1">
      {/*<Testeira nomeCursoPt1={testeira.nomeCursoPt1} nomeCursoPt2 ={testeira.nomeCursoPt2} 
        nomeUnidadePt1={testeira.nomeUnidadePt1} nomeUnidadePt2={testeira.nomeUnidadePt2} 
        tipoBarra={testeira.tipoBarra} menuHamburguer={menuHamburguer}/> */}
      <div>
        <Testeira
          nomeCursoPt1={testeira.nomeCursoPt1}
          nomeCursoPt2={testeira.nomeCursoPt2}
          nomeUnidadePt1={testeira.nomeUnidadePt1}
          nomeUnidadePt2={testeira.nomeUnidadePt2}
          tipoBarra={testeira.tipoBarra}
          menuHamburguer={menuHamburguer}
        />
      </div>

      <Creditos tipoUnidade={unidade.tipoUnidade} corBarra={unidade.corBarra} />
    </div>
  );
}

export default DescritorCreditos;
