import React from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Row,
  Col
} from "reactstrap";

import iconeVideo from "../img/video.png";
import "./css/Utilitarios.css";
import "./css/Modal.css";

const styleModal = {};

class ModalVideo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  render() {
    const { link } = this.props.link;
    return (
      <div className="fundoVideo borda10">
        <Container>
          <Row>
            <Col md="12" lg="12" className="espacamento espacamentoBottom">
              <span className="cursorClicavel" onClick={this.toggle}>
                <img src={iconeVideo} className="imgCaixaVideo" />
              </span>
            </Col>
          </Row>
        </Container>

        <Modal isOpen={this.state.modal} toggle={this.toggle} className="height">
          <Container>
            <Row className="fundoVideoModal">
              <Col md="12" lg="12">
                <iframe
                  width="650"
                  height="415"
                  src={link}
                  frameborder="0"                  
                  allowfullscreen
                ></iframe>
              </Col>
            </Row>
          </Container>
        </Modal>
      </div>
    );
  }
}

export default ModalVideo;
