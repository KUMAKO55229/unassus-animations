import React from "react";
import "./css/Unidade.css";
import "./css/Utilitarios.css";
import "font-awesome/css/font-awesome.min.css";
import {
	Container,
	Row,
	Col,
	Button,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Carousel,
	CarouselItem,
	CarouselControl,
	CarouselIndicators,
	CarouselCaption
} from "reactstrap";

import seta from "../img/seta.png";

import "./css/Modal.css";
import "./css/Slider.css";

const items = [
	{
		texto: (
			<div className="fundoSliderTexto">
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"><b>COM BAIXO PESO</b></p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								• Caso sinta necessidade de um acompanhamento
								individualizado para a gestante, você pode
								solicitar apoio ao nutricionista da equipe NASF
								ou, ainda, encaminhar para atenção
								especializada.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								• Verifique também a existência de hiperêmese
								gravídica, infecções, parasitoses, anemias e
								doenças debilitantes. Em caso positivo,
								direcione a gestante para o profissional de
								saúde indicado. Em casos de hiperêmese oriente-a
								a comer de maneira mais fracionada e em pequenas
								quantidades, evitando alimentos gordurosos.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								• Forneça orientação nutricional, visando à
								promoção do peso adequado e de hábitos
								alimentares saudáveis.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								• Remarque a consulta em um intervalo menor que
								o calendário habitual para monitoramento.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								• Converse com a gestante sobre a importância de
								ganhar peso adequadamente, explique que o ganho
								de peso deve ocorrer para garantir o
								desenvolvimento do bebê, formação de placenta e
								manutenção do líquido amniótico, e que com uma
								alimentação adequada esse peso será naturalmente
								eliminado nos primeiros meses pós-parto. Muitas
								mães têm medo de ganhar peso e não voltar à
								antiga forma física.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								• Lembre-se de que o acolhimento e a informação
								são muito importantes. Evitemos o julgamento e
								apenas dar ordens para a gestante. É muito
								importante conhecer o contexto em que essa mãe
								vive e, em caso de incapacidade financeira,
								verifique a possibilidade de encaminhá-la para a
								assistência social.
							</p>
						</Col>
					</Row>
				</Container>
			</div>
		)
	},
	{
		texto: (
			<div className="fundoSliderTexto">
				<div className="container-text">
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade"><b>COM PESO ADEQUADO</b></p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade"></p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									• Siga o calendário habitual de pré-natal,
									avaliando o peso e IMC a cada consulta.
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									• Explique à gestante que seu peso está adequado
									para a idade gestacional, e encoraje-a a manter
									uma alimentação equilibrada e saudável, conforme
									recomendações já estudadas, visando à manutenção
									do peso adequado e à promoção de hábitos
									alimentares saudáveis.
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									• Sempre registre o IMC no gráfico de
									acompanhamento e mostre para a gestante a sua
									evolução.
								</p>
							</Col>
						</Row>
					</Container>
				</div>

				{/*<Col md="4" lg="4">
					<img src={require("../img/frutas.jpeg")} className="responsive w100 imgEdited"/>
				</Col> */}
			</div>
		)
	},
	{
		texto: (
			<div className="fundoSliderTexto">
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								<b>COM PESO ELEVADO OU GANHO EXCESSIVO</b>
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								• Adicionalmente, avalie a presença de edema,
								polidrâmnio, macrossomia, gravidez múltipla e
								doenças associadas (diabetes, pré-eclâmpsia,
								etc.). Nesses casos, encaminhe a gestante para
								atendimento individualizado com o nutricionista
								e/ou equipe multidisciplinar.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								• Em todas as situações, ofereça apoio e escuta.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								• Avalie os hábitos alimentares da gestante;
								verifique se o consumo de alimentos
								ultraprocessados é frequente. Você pode
								perguntar se a gestante faz consumo frequente de
								pães, embutidos, bolachas, doces e
								refrigerantes. Oriente a gestante a realizar
								caminhadas leves, hidratar-se adequadamente, e
								realizar consumo rotineiro de frutas e vegetais.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								• Remarque as consultas em intervalo menor que o
								fixado no calendário habitual para monitoramento
								do ganho de peso.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								• Ofereça orientação nutricional, visando à
								promoção do peso adequado e de hábitos
								alimentares saudáveis, ressaltando que, no
								período gestacional, não se deve perder peso,
								mas que o ganho de peso até o final da gestação
								não deve ultrapassar os 7 kg em situações de
								obesidade pré-gestacional ou os 11,5 kg em casos
								de sobrepeso pré-gestacional.{" "}
							</p>
						</Col>
					</Row>
				</Container>
			</div>
		)
	}
];

class Slider extends React.Component {
	constructor() {
		super();
		this.state = { activeIndex: 0, isHiddenCaixa: true };
		this.next = this.next.bind(this);
		this.previous = this.previous.bind(this);
		this.goToIndex = this.goToIndex.bind(this);
		this.onExiting = this.onExiting.bind(this);
		this.onExited = this.onExited.bind(this);
	}

	onExiting() {
		this.animating = true;
	}

	onExited() {
		this.animating = false;
	}

	next() {
		if (this.animating) return;
		const nextIndex =
			this.state.activeIndex === items.length - 1
				? 0
				: this.state.activeIndex + 1;
		this.setState({ activeIndex: nextIndex });
	}

	previous() {
		if (this.animating) return;
		const nextIndex =
			this.state.activeIndex === 0
				? items.length - 1
				: this.state.activeIndex - 1;
		this.setState({ activeIndex: nextIndex });
	}

	goToIndex(newIndex) {
		if (this.animating) return;
		this.setState({ activeIndex: newIndex });
	}

	render() {
		const { activeIndex } = this.state;

		const slides = items.map(item => {
			return (
				<CarouselItem
					onExiting={this.onExiting}
					onExited={this.onExited}
					key={item.texto}
				>
					<Container>
						<Row>
							<Col md="12" lg="12">
								{item.texto}
							</Col>
						</Row>
					</Container>
				</CarouselItem>
			);
		});

		return (
			<div className="slider1">
				<Container>
					<Row>
						<Col
							md="12"
							lg="12"
							className="espacamentoBottom espacamento"
						>
							<Carousel
								activeIndex={activeIndex}
								next={this.next}
								previous={this.previous}
								pause={false}
								ride="carousel"
								interval={false}
								slide={true}
							>
								<CarouselIndicators
									items={items}
									activeIndex={activeIndex}
									onClickHandler={this.goToIndex}
									className="espacamento zIndex"
								/>

								{slides}
								<br />
								<br />
								<br />
							</Carousel>
							<CarouselControl
								direction="prev"
								directionText="Previous"
								onClickHandler={this.previous}
								className=""
							>
								<img
									src={seta}
									className="responsive w100 setaIndex"
								/>
							</CarouselControl>
							{/*TODO: colocar seta aqui*/}
							<CarouselControl
								direction="next"
								directionText="Next"
								onClickHandler={this.next}
								className=""
							/>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default Slider;
