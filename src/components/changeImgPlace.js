import img01 from '../img/un2/fundo1.png';
import img02 from '../img/un2/fundo2.png';
import img03 from '../img/un2/fundo3.png';
import img04 from '../img/un2/fundo4.png';

var currentTopic = 0;
var imgSrc = [img01,img02, img03, img04];
function changeImgPlace() {
	let topics = [, 
				document.getElementById("titleImg2"), 
				document.getElementById("titleImg3"), 
				document.getElementById("titleImg4")];

	let observer1 = new IntersectionObserver(nextImg1);
	let observer2 = new IntersectionObserver(nextImg2);
	let observer3 = new IntersectionObserver(nextImg3);
	let observer4 = new IntersectionObserver(nextImg4);

	window.onload = function () {

		unfixImg();
	    let target = document.getElementById("titleImg1");
		//note this observe method
	    observer1.observe(target);
	    target = document.getElementById("titleImg2");
		//note this observe method
	    observer2.observe(target);
	    target = document.getElementById("titleImg3");
		//note this observe method
	    observer3.observe(target);
	    target = document.getElementById("titleImg4");
		//note this observe method
	    observer4.observe(target);
	    console.log("registered");

	    unfixImg();
	};
}


function nextImg1() {
	let img = document.getElementById("imgPlace");
	if (document.getElementById("titleImg1").style.bottom < 0 ||
	 document.getElementById("titleImg1").style.top > (window.innerHeight || document.documentElement.clientHeight)) {
		return;
	}
	fixImg();
	img.src = imgSrc[0];
}

function nextImg2() {
	let img = document.getElementById("imgPlace");
	if (document.getElementById("titleImg2").style.bottom < 0 ||
	 document.getElementById("titleImg2").style.top > (window.innerHeight || document.documentElement.clientHeight)) {
		return;
	}
	img.src = imgSrc[1];
}

function nextImg3() {
	let img = document.getElementById("imgPlace");
	if (document.getElementById("titleImg3").style.bottom < 0 ||
	 document.getElementById("titleImg3").style.top > (window.innerHeight || document.documentElement.clientHeight)) {
		return;
	}
	img.src = imgSrc[2];
}

function nextImg4() {
	let img = document.getElementById("imgPlace");
	if (document.getElementById("titleImg4").style.bottom < 0 ||
	 document.getElementById("titleImg4").style.top > (window.innerHeight || document.documentElement.clientHeight)) {
		return;
	}
	fixImg();
	img.src = imgSrc[3];
}

function fixImg() {
	let img = document.getElementById("imgPlace");
	img.style.position = "fixed";
}

function unfixImg() {
	let img = document.getElementById("imgPlace");
	img.style.position = "relative";
}

changeImgPlace();
