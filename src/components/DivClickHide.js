import React from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Row,
  Col
} from "reactstrap";
import iconeSaibaMais from "../img/saibaMais.png";
import iconeReflexao from "../img/reflexao.png";
import iconeDestaque from "../img/destaque.png";
import iconeNota from "../img/nota.png";
import cliqueAqui from "../img/clique.png";
import iconeLink from "../img/link.png";

import iconeVideo from "../img/video.png";
import ScrollAnimation from "react-animate-on-scroll";
import "./css/Utilitarios.css";
import {
  TituloSecaoBarra2,
  TituloSubSecaoBarra,
  CaixaTemplate,
  CaixaGeralRetorno,
  TituloSecaoBarraNova
} from "./Utilitarios.js";

class DivHide extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isHidden: true
    };
  }

  toggleHidden() {
    this.setState({
      isHidden: !this.state.isHidden
    });
  }
  render(props) {
    return (
      <div>
        <Container
          onClick={this.toggleHidden.bind(this)}
          className="clickHide borda10 cursorClicavel"
        >
          <Row>
            <Col md="10" lg="10" className="espacamento">
              <p className="textoUnidade">{this.props.textoClique}</p>
            </Col>
            <Col md="2" lg="2" xs="2" sm="2">
              <img src={cliqueAqui} className="responsive w40 imgTop" />
            </Col>
          </Row>
        </Container>

        {!this.state.isHidden && (
          <Child1
            textoCaixa={this.props.textoCaixa}
            fundoCaixa={this.props.fundoCaixa}
          />
        )}
      </div>
    );
  }
}

const Child1 = props => {
  return (
    <div className="espacamentoBottom">
      <Container className={"bordaCaixa espacamento " + props.fundoCaixa }>{props.textoCaixa}</Container>
    </div>
  );
};

export default DivHide;
