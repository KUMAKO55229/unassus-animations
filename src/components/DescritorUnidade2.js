/* eslint-disable import/first */
import React from "react";
import Testeira from "./Testeira.js";
import CapaUnidade from "./capa/CapaPP.js";
import Sumario from "./SumarioLink.js";
import Unidade from "./Unidade2.js";
import menuHamburguer from "../img/menuObesidade2.png";
import ReturnStickyButton from "./ReturnStickyButton.js";

import Botoes from "./botoes/Botoes.js";


import un1 from "../img/un2/un1.png";
import un2 from "../img/un2/un2.png";
import un3 from "../img/un2/un3.png";

const testeira = {
  nomeCursoPt1: "Promoção do Ganho de Peso",
  nomeCursoPt2: "Adequado na Gestação",
  nomeUnidadePt1: "AVALIAÇÃO DO ESTADO NUTRICIONAL E",
  nomeUnidadePt2: "RECOMENDAÇÕES PARA GESTANTES EM NÍVEL INDIVIDUAL",
  tipoBarra: "barraHamburguerLogo"
};

const linkCurso = "https://unasus-cp.moodle.ufsc.br/course/view.php?id=472";

const sumarioPt1 = [
  { id: "/un2#sec21", title: "AVALIAÇÃO DO ESTADO NUTRICIONAL NA GESTAÇÃO" },
  { id: "/un2#sec22", title: "AVALIAÇÃO DO ESTADO NUTRICIONAL PRÉ-GESTACIONAL" }
];

const sumarioPt2 = [
  {
    id: "/un2#sec23",
    title: "MONITORAMENTO E ACONSELHAMENTO DO GANHO DE PESO GESTACIONAL"
  },
  {
    id: "/un2#sec24",
    title:
      "RECOMENDAÇÕES PARA UMA ALIMENTAÇÃO SAUDÁVEL NA GESTAÇÃO"
  },
  {
    id: "/un2#sec25",
    title:
      "MICRONUTRIENTES NA GESTAÇÃO"
  },
  {
    id: "/un2#sec26",
    title:
      "ALIMENTOS QUE DEVEM SER EVITADOS OU CONSUMIDOS MODERADAMENTE DURANTE A GESTAÇÃO"
  },
  {
    id: "/un2#sec27",
    title:
      "ESTRATÉGIAS PARA INCLUSÃO DAS RECOMENDAÇÕES ALIMENTARES NA ROTINA DE PRÉ-NATAL DA UBS"
  },

];

const sumario = {
  qntColunas: 2,
  tipoSumario: "sumarioUnidade"
};

/*const sumarioTitulosPt1 = [
  "AVALIAÇÃO DO ESTADO NUTRICIONAL NA GESTAÇÃO",
  "AVALIAÇÃO DO ESTADO NUTRICIONAL PRÉ-GESTACIONAL"
];

const sumarioTitulosPt2 = [
  "MONITORAMENTO E ACONSELHAMENTO DO GANHO DE PESO GESTACIONAL",
  "RECOMENDAÇÕES PARA UMA ALIMENTAÇÃO SAUDÁVEL E SUA IMPORTÂNCIA NA GESTAÇÃO",
  "MICRONUTRIENTES NA GESTAÇÃO",
  "ALIMENTOS QUE DEVEM SER EVITADOS OU CONSUMIDOS MODERADAMENTE DURANTE A GESTAÇÃO"
];*/

const unidade = {
  tipoUnidade: "barraUnidade",
  corBarra: "blue"
};

function DescritorUnidade2() {
  return (
    <div className="DescritorUnidade2">
      {/*<Testeira nomeCursoPt1={testeira.nomeCursoPt1} nomeCursoPt2 ={testeira.nomeCursoPt2} 
        nomeUnidadePt1={testeira.nomeUnidadePt1} nomeUnidadePt2={testeira.nomeUnidadePt2} 
        tipoBarra={testeira.tipoBarra} menuHamburguer={menuHamburguer}/> */}
      <div id="top">
        <CapaUnidade
          tituloPt1={testeira.nomeUnidadePt1}
          tituloPt2={testeira.nomeUnidadePt2}
        />
        <Testeira
          nomeCursoPt1={testeira.nomeCursoPt1}
          nomeCursoPt2={testeira.nomeCursoPt2}
          nomeUnidadePt1={testeira.nomeUnidadePt1}
          nomeUnidadePt2={testeira.nomeUnidadePt2}
          tipoBarra={testeira.tipoBarra}
          menuHamburguer={menuHamburguer}
        />
        <Sumario
          tipoSumario={sumario.tipoSumario}
          titulosPt1={sumarioPt1}
          titulosPt2={sumarioPt2}
        />
      </div>
      <Unidade tipoUnidade={unidade.tipoUnidade} corBarra={unidade.corBarra} />
      <Botoes
        un1Path={"/un1#top"}
        un2Path={"/un2#top"}
        un3Path={"/un3#top"}
        un1={un1}
        un2={un2}
        un3={un3}
      />
    </div>
  );
}

export default DescritorUnidade2;
