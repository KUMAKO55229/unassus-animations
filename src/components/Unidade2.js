import React from "react";
import "./css/Unidade.css";
import "./css/Utilitarios.css";
import "./css/Unidade2.css";
import "font-awesome/css/font-awesome.min.css";
import {
	Container,
	Row,
	Col,
	Button,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter
} from "reactstrap";
import { caixa } from "./Utilitarios.js";
import { criarLink } from "./Utilitarios.js";
import {
	TituloSecaoBarra2,
	TituloSubSecaoBarra,
	CaixaTemplate,
	CaixaGeralRetorno,
	TituloSecaoBarraNova,
	TituloSubSecaoBarraNova
} from "./Utilitarios.js";

import IconeHide from "./IconeNota.js";
import DivHide from "./DivClickHide.js";
import arrow from "../img/bullet.png";
import gestograma from "../img/un2/gestograma.png";

import seta from "../img/arrow.png";

import Mooc3v268 from "../img/un2/Mooc3v2-68.png";
import Mooc3v269 from "../img/un2/Mooc3v2-69.png";
import tabFinal from "../img/un2/Mooc3v2-36.png";

import imgHeader1 from "../img/un2/icon1.png";
import imgExemplo from "../img/un2/exemplo.png";
import imgHeader3 from "../img/un2/header.png";
import imgHeader2 from "../img/un2/icon2.png";

import img01 from "../img/un2/caderneta.png";
import img02 from "../img/un2/grafico.png";
import densidade from "../img/un2/densidade.png";
import img03 from "../img/un2/img03.png";
import img04 from "../img/un2/img04.jpeg";
import img05 from "../img/un2/img05.jpg";
import img06 from "../img/un2/img06.png";
import img07 from "../img/un2/img07.jpeg";
import img07b from "../img/un2/img07b.png";
import cafe from "../img/un2/cafe.png";
import almoco from "../img/un2/almoco.png";

import imgUltra from "../img/un2/ultra.png";
import quadro01 from "../img/un2/quadroA.png";
import tab1 from "../img/un2/tab1.png";
import tab2 from "../img/un2/tab1.png";

import tab3a from "../img/un2/Mooc3v2-34.png";
import tab3b from "../img/un2/tab3b.png";
import tab3c from "../img/un2/tab3c.png";
import tab4a from "../img/un2/tab4a.png";

import tab5 from "../img/un2/Mooc3v2-72.png";

import tab6a from "../img/un2/tab6a.png";
import tab6b from "../img/un2/tab6b.png";

import tab7 from "../img/un2/tab7.png";
import tab8 from "../img/un2/tab8.png";

import img08 from "../img/un2/img08.png";
import img09 from "../img/un2/img09.png";
import img10 from "../img/un2/img10.jpeg";
import img11 from "../img/un2/img11.png";
import img13 from "../img/un2/img13.png";

import iconeSaibaMais from "../img/saibaMais.png";
import iconeReflexao from "../img/reflexao.png";
import iconeDestaque from "../img/destaque.png";
import iconeNota from "../img/nota.png";
import iconeLink from "../img/link.png";
import ModalPopUp from "./ModalExemplo.js";

import click from "../img/click.png";
import cliqueAqui from "../img/clique.png";
import ReturnStickyButton from "./ReturnStickyButton.js";

import Slider from "./Slider.js";
import SliderTab from "./SliderUn2.js";
import Slider2 from "./SliderUn2_2.js";

import DivHideFinal from "./DivClickHideFinal.js";
import Accordion from "./Accord2.js";

class Unidade extends React.Component {
	constructor() {
		super();
		this.state = {
			isHiddenCaixa: true,
			isHiddenCaixa2: true,
			isHiddenCaixa3: true
		};
	}
	toggleCaixa() {
		this.setState({
			isHiddenCaixa: !this.state.isHiddenCaixa
		});
	}

	toggleCaixa2() {
		this.setState({
			isHiddenCaixa2: !this.state.isHiddenCaixa2
		});
	}

	toggleCaixa3() {
		this.setState({
			isHiddenCaixa3: !this.state.isHiddenCaixa3
		});
	}

	componentDidMount() {
		setTimeout(() => {
			//var height01 = document.getElementById("textoImg01").clientHeight;
			var height02 =
				document.getElementById("textoImg01").clientHeight +
				document.getElementById("textoImg02").clientHeight +
				16;
			var height03 =
				document.getElementById("textoImg03").clientHeight +
				document.getElementById("textoImg04").clientHeight +
				16;

			var heightNM =
				document.getElementById("textoImgN").clientHeight +
				document.getElementById("textoImgM").clientHeight +
				16;
			var height05 = document.getElementById("textoImg05").clientHeight;
			var height06 = document.getElementById("textoImg06").clientHeight;
			var height07 = document.getElementById("textoImg07").clientHeight;
			this.setState({
				//height01,
				height02,
				height03,
				height05,
				height06,
				height07,
				heightNM
			});
		}, 1000);
	}

	render() {
		return (
			<div>
				<ReturnStickyButton />

				<TituloSecaoBarraNova
					titulo={"Introdução"}
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo25"
					}
				/>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Nesta unidade, conheceremos os instrumentos de
								avaliação nutricional, as formas de
								monitoramento e acompanhamento do ganho de peso
								gestacional, assim como as principais
								recomendações alimentares voltadas para as
								gestantes. Ao final da leitura dessa unidade,
								você será capaz de conhecer e aplicar as
								recomendações de avaliação e monitoramento do
								estado nutricional gestacional, além de orientar
								à gestante quanto aos cuidados na alimentação e
								nutrição no contexto da Atenção Primária à Saúde
								para prevenção do sobrepeso e obesidade.
							</p>
						</Col>
					</Row>
				</Container>

				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo3"
					}
					titulo={"AVALIAÇÃO DO ESTADO NUTRICIONAL NA GESTAÇÃO"}
				/>

				<Container id="sec21">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Mediante as consultas de pré-natal,
								conseguiremos acolher a mulher desde o início da
								gravidez, assegurando o acompanhamento contínuo
								do estado nutricional e contribuindo para o
								ganho de peso ideal durante a gestação. Assim,
								conseguiremos dar uma melhor qualidade de vida à
								gestante e evitar a retenção de peso no
								pós-parto, que é um dos fatores determinantes do
								excesso de peso para a mulher em curto e longo
								prazo. Veja mais sobre a avaliação do estado
								nutricional
								<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaDestaque"}
									icone={iconeDestaque}
									textoUnidade={
										"A avaliação do estado nutricional da gestante tem por objetivo acompanhar o aumento de peso durante toda a gestação, assim como identificar e providenciar intervenções adequadas nesta fase. "
									}
									inserirLink={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Antes de conseguirmos avaliar e definir as
								recomendações de ganho de peso gestacional para
								cada gestante é preciso que identifiquemos as
								gestantes em risco nutricional no início da
								gestação, considerando as categorias: baixo
								peso, sobrepeso ou obesidade pré-gestacional.
							</p>
						</Col>
					</Row>
				</Container>
				<div className="">
					<Container className="faixaDosNumerosGrandes borda10 espacamento">
						<Row>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											<b>
												A avaliação nutricional, quando
												possível é realizada em duas
												etapas:
											</b>
										</p>
									</Col>
								</Row>
							</Container>

							<Col md="6" lg="6">
								<Container>
									<Row>
										<Col md="2" lg="2">
											<p className="textoUnidade numeroGrande">
												1
											</p>
										</Col>
										<Col md="10" lg="10">
											<p className="textoUnidade">
												Diagnóstico nutricional:
												avaliação do estado nutricional
												pré-gestacional e gestacional
												por meio do cálculo do IMC.
											</p>
										</Col>
									</Row>
								</Container>
							</Col>
							<Col md="6" lg="6">
								<Container>
									<Row>
										<Col md="2" lg="2">
											<p className="textoUnidade numeroGrande">
												2
											</p>
										</Col>
										<Col md="10" lg="10">
											<p className="textoUnidade">
												Estimativa do ganho de peso: de
												acordo com o estado nutricional
												pré-gestacional e/ou
												gestacional.{" "}
											</p>
										</Col>
									</Row>
								</Container>
							</Col>
						</Row>
					</Container>
				</div>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								O primeiro passo para avaliar o estado
								nutricional da gestante é realizar o cálculo da
								idade gestacional. Para estimar a idade
								gestacional você precisa saber a data da última
								menstruação (DUM) da mãe. A DUM corresponde ao
								primeiro dia de sangramento do último ciclo
								menstrual referido pela mulher. É o método de
								escolha para se calcular a idade gestacional em
								mulheres com ciclos menstruais regulares e que
								não fazem uso de métodos anticoncepcionais
								hormonais.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Você pode calcular a DUM de duas formas: usando
								um calendário some o número de dias do intervalo
								entre a DUM e a data da consulta, dividindo o
								total por sete, e terá o resultado em semanas;
								usando o gestograma, conforme orientações a
								seguir:
							</p>
						</Col>
					</Row>
				</Container>

				{/*TO-DO - Trocar para referência interna esse link*/}
				<div className="fundoGestograma">
					<Container>
						<Row>
							<Col
								md="6"
								lg="6"
								className="espacamentoGestograma"
							>
								<p className="deslocarEsqGestograma txtGestograma">
									<b>Como utilizar o gestograma</b>
								</p>
								<br />
								<p className="deslocarEsqGestogramaTxt txtGestograma2">
									1. Coloque a seta sobre o dia e o mês
									correspondentes ao primeiro dia e mês do
									último ciclo menstrual
								</p>
								<p className="deslocarEsqGestogramaTxt txtGestograma2">
									2. Observe o número de semanas indicado no
									dia e mês da consulta atual
								</p>

								{/*<iframe
									src="https://unasus-quali.moodle.ufsc.br/mod/resource/view.php?id=1058"
									width="100%"
									height="665"
									frameBorder="no"
									scrolling="no"
									className=""
								></iframe>*/}
							</Col>
							<Col>
								<img
									src={gestograma}
									className="responsive w100 borda10"
								/>
							</Col>
						</Row>
					</Container>
				</div>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Quando a gestante desconhece a data da última
								menstruação, mas se conhece o período do mês em
								que ela ocorreu, pergunte se o período foi no
								início, meio ou fim do mês e considere como data
								da última menstruação os dias 5, 15 e 25,
								respectivamente. Proceda, então, à utilização do
								calendário ou do gestograma.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Existem situações em que a data e o período da
								última menstruação são desconhecidos pela
								gestante. Quando a data e o período do mês não
								forem conhecidos, a idade gestacional e a data
								provável do parto serão, inicialmente,
								determinadas por aproximação, basicamente pela
								medida da altura do fundo do útero, além da
								informação sobre a data de início dos movimentos
								fetais, que habitualmente ocorrem entre 16 e 20
								semanas. A idade gestacional pode ser calculada,
								também, por um ultrassom ou ecografia.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<div>
					<Container className="fundoIcones2">
						<Container className="">
							<Row>
								<Col md="6" lg="6">
									<img
										src={imgUltra}
										className="responsive w100 espacamento"/>
								</Col>
								<Col md="6" lg="6">
									<p className="textoUnidade espacamento">
										O ideal, para esse fim, é que a
										ecografia seja feita antes das 24
										semanas.</p><p> Após essa idade gestacional, é
										mais provável que o estado nutricional
										do feto influencie nas medidas do bebê
										e, por isso, no cálculo da idade
										gestacional.
									</p>
									</Col>
							</Row>
						</Container>
						<Container className="espacamento">
							<Row>						
							<Col md="12" lg="12">
									<p className="textoUnidade">
										Ao saber a idade gestacional pelo
										ultrassom, basta contar o número de dias
										desde o exame até a consulta e dividir
										por sete. Depois é só somar o resultado
										da idade gestacional dada pelo exame.
									</p>
								</Col>
							</Row>
						</Container>
						<Container className="clickExemplo borda10">
							<Row>
								<Col md="12" lg="12">
									<ModalPopUp
										conceito={
											<p className="">
												Clique aqui para conhecer um
												exemplo
											</p>
										}
										textoConceito={
											"A promoção de saúde é o “processo de capacitação da comunidade para atuar na melhoria da sua qualidade de vida e saúde, incluindo maior participação no controle desse processo” (CARTA DE OTTAWA, 1986)."
										}
										imgPopUp={
											<img
												src={imgExemplo}
												className="responsive w100 borda10 "
											/>
										}
									></ModalPopUp>
								</Col>
							</Row>
						</Container>

						<Container className="espacamentoBottom">
							<Row>
								<Col md="12" lg="12"></Col>
							</Row>
						</Container>
					</Container>
				</div>
				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								<b>DUM</b>: sempre considerar a idade
								gestacional pela DUM (Data da Última
								Menstruação) se for conhecida e confiável ou
								pela ecografia mais precoce (com menor idade
								gestacional).
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Em cada consulta gestacional, o peso deve ser
								aferido, e a altura deve ser medida na primeira
								consulta. Ao aferir o peso da gestante,
								considere os procedimentos do SISVAN (BRASIL,
								2008). Recomenda-se a utilização de uma balança
								eletrônica ou mecânica, certificando-se de que
								essas se encontram calibradas e em bom
								funcionamento, a fim de garantir a qualidade das
								medidas coletadas. Realizados esses
								procedimentos, você terá as informações
								necessárias para avaliação do estado nutricional
								da gestante.
							</p>
						</Col>
					</Row>
				</Container>

				<div className="espacamentoBottom">
					<DivHide
						textoClique={[
							<b>Clique aqui</b>,
							" para conhecer os procedimentos"
						]}
						fundoCaixa={"borda0"}
						textoCaixa={
							<Container>
								<Row>
									<Col
										md="12"
										lg="12"
										className="fundo01bFinal borda10"
									>
										<Row>
											<Col
												md="6"
												lg="6"
												className="bordaPeso"
											>
												<img
													src={imgHeader1}
													className="centerImg iconPesoAltura espacamento"
												/>

												<p className="textoUnidade tituloHeader">
													Peso
												</p>
											</Col>
											<Col md="6" lg="6" className="">
												<img
													src={imgHeader2}
													className="centerImg iconPesoAltura espacamento"
												/>
												<p className="textoUnidade tituloHeader">
													Estatura
												</p>
											</Col>
										</Row>
									</Col>

									<Col
										md="12"
										lg="12"
										className="fundoCinza borda10"
									>
										<Row>
											<Col
												md="6"
												lg="6"
												className="bordaPeso espacamento"
											>
												<ul>
													<li>
														<p>
															antes de cada
															pesagem, a balança
															deve ser destravada,
															zerada e calibrada;
														</p>
													</li>
													<li>
														<p>
															a gestante, descalça
															e vestida apenas com
															avental ou roupas
															leves, deve subir na
															plataforma e ficar
															em pé, de costas
															para o medidor, com
															os braços estendidos
															ao longo do corpo e
															sem qualquer outro
															apoio;
														</p>
													</li>
													<li>
														<p>
															se estiver
															utilizando a balança
															mecânica, mova o
															marcador maior (kg)
															do zero da escala
															até o ponto em que o
															braço da balança se
															incline para baixo;
															volte-o, então, para
															o nível
															imediatamente
															anterior (o braço da
															balança inclina-se
															para cima);
														</p>
													</li>
													<li>
														<p>
															mova o marcador
															menor (g) do zero da
															escala até o ponto
															em que haja
															equilíbrio entre o
															peso da escala e o
															peso da gestante (o
															braço da balança
															fica em linha reta,
															e o cursor aponta
															para o ponto médio
															da escala);
														</p>
													</li>
													<li>
														<p>
															leia o peso em
															quilogramas na
															escala maior e em
															gramas na escala
															menor. No caso de
															valores
															intermediários
															(entre os traços da
															escala), considere o
															menor valor. Por
															exemplo: se o cursor
															estiver entre 200g e
															300g, considere
															200g;
														</p>
													</li>

													<li>
														<p>
															anote o peso
															encontrado no
															prontuário e no
															Cartão da Gestante.
														</p>
													</li>
												</ul>
											</Col>
											<Col
												md="6"
												lg="6"
												className="espacamento"
											>
												<ul>
													<li>
														<p>
															a gestante deve
															estar em pé e
															descalça, no centro
															da plataforma da
															balança, com os
															braços estendidos ao
															longo do corpo.
															Quando disponível,
															poderá ser utilizado
															o antropômetro
															vertical;
														</p>
													</li>
													<li>
														<p>
															calcanhares, nádegas
															e espáduas devem se
															aproximar da haste
															vertical da balança;
														</p>
													</li>
													<li>
														<p>
															a cabeça deve estar
															erguida de maneira
															que a borda inferior
															da órbita fique no
															mesmo plano
															horizontal que o
															meato do ouvido
															externo;
														</p>
													</li>
													<li>
														<p>
															o encarregado de
															realizar a medida
															deverá baixar
															lentamente a haste
															vertical,
															pressionando
															suavemente os
															cabelos da gestante
															até que a haste
															encoste-se ao couro
															cabeludo;
														</p>
													</li>
													<li>
														<p>
															faça a leitura da
															escala da haste. No
															caso de valores
															intermediários
															(entre os traços da
															escala), considere o
															menor valor. Anote o
															resultado no
															prontuário.
														</p>
													</li>
												</ul>
											</Col>
										</Row>
									</Col>
								</Row>
							</Container>
						}
					/>
				</div>

				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo3"
					}
					titulo={"AVALIAÇÃO DO ESTADO NUTRICIONAL PRÉ-GESTACIONAL"}
				/>

				<Container id="sec22">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Ao avaliar o estado nutricional pré-gestacional
								você será capaz de conhecer qual era o estado
								nutricional da gestante antes da gravidez. Com
								essa informação você poderá ajudá-la a programar
								o ganho de peso adequado durante a evolução da
								gestação. O estado nutricional pré-gestacional é
								avaliado através do Índice de Massa Corporal
								(IMC). O IMC pré-gestacional deve ser calculado
								na primeira consulta de pré-natal.{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade" id="textoImg01">
								O peso pré-gestacional influenciará diretamente
								no tamanho do bebê ao nascimento, assim como no
								estado nutricional da mãe no pós-parto. O ideal
								é que o IMC considerado no diagnóstico inicial
								da gestante seja o IMC pré-gestacional referido,
								ou o IMC calculado a partir de medição realizada
								até a 13ª semana gestacional (calculada por meio
								da data da última menstruação – DUM).{" "}
							</p>
							<p className="textoUnidade" id="textoImg02">
								Caso isso não seja possível, inicie a avaliação
								da gestante com os dados da primeira consulta de
								pré-natal, mesmo que essa ocorra após a 13ª
								semana gestacional. Uma das limitações para a
								utilização do IMC durante a gestação é que não
								existe ainda uma curva de referência brasileira
								de valores de IMC por idade gestacional. Mas,
								como calcular Índice de massa corporal (IMC)?
								Acompanhe, a seguir, algumas recomendações.
							</p>
						</Col>
						<Col md="6" lg="6">
							<img
								src={quadro01}
								className="responsive borda10"
								style={{
									height: this.state.height02 + "px"
								}}
							/>
						</Col>
						{/*<Col md="6" lg="6" className="fundoItens">
							<p className="textoI1">
								Primeiro você precisa coletar os dados de peso
								corporal pré-gestacional e de altura da
								gestante. O peso pré-gestacional pode ser obtido
								de diversas formas.
							</p>
							<br />
							<br />
							<br />
							<p className="textoI2">
								A mais recomendada é utilizar o peso e altura
								contidos no registro médico no período de até
								dois meses anteriores à gestação ou realizar a
								aferição da mulher logo após a concepção.
							</p>
							<br />
							<br />
							<br />
							<p className="textoI3">
								Entretanto, caso isso não seja possível, como
								terceira opção você pode obter as informações do
								estado nutricional pré-gestacional através do
								relato da gestante com relação ao seu peso e
								altura até dois meses antes da gestação
								(lembrando que a informação está sujeita à
								memória da gestante).
							</p>
							<br />
							<br />
							<br />
							<p className="textoI4">Caso nenhuma destas opções tenha sido possível, realize a aferição de peso e altura durante o primeiro trimestre gestacional.
</p>
						</Col>*/}
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								De acordo com o estado nutricional inicial da
								gestante (baixo peso, adequado, sobrepeso ou
								obesidade) há uma faixa de ganho de peso
								recomendada por trimestre. É importante que na
								primeira consulta a gestante seja informada
								sobre o peso que deve ganhar.
							</p>
						</Col>
					</Row>
				</Container>

				{/*<Container>
					<Row>
						<Col md="12" lg="12">
							<img
								src={img01}
								className="responsive w60 centerImg"
							/>
						</Col>
					</Row>
				</Container>*/}

				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo3"
					}
					titulo={
						"MONITORAMENTO E ACONSELHAMENTO DO GANHO DE PESO GESTACIONAL"
					}
				/>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Após obtido o valor do IMC, será a hora de
								realizar o diagnóstico do estado nutricional.
								Lembre também que a recomendação do ganho de
								peso durante a gestação deve ser diferenciada de
								acordo com a classificação do IMC
								pré-gestacional, o estágio de vida da gestante
								(adulta ou adolescente) e se a gestação é única
								ou gemelar.
							</p>
						</Col>
					</Row>
				</Container>
				<Container id="sec23">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Devemos realizar o acompanhamento e
								monitoramento do estado nutricional ao longo de
								todas as consultas de pré-natal. Para realização
								do registro do IMC gestacional e acompanhamento
								da evolução do ganho de peso a cada consulta,
								são utilizadas duas ferramentas: o quadro com os
								dados necessários para avaliação do estado
								nutricional da gestante segundo o índice de
								massa corporal (
								{criarLink(
									"https://unasus-quali.moodle.ufsc.br/mod/resource/view.php?id=1056",
									"clique no ícone para conhecer",
									iconeLink,
									"iconeTexto12"
								)}
								) e o gráfico de acompanhamento do estado
								nutricional encontrado na caderneta da gestante.
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade" id="textoImg03">
								Você deverá calcular o IMC em cada consulta de
								pré-natal e realizar o registro do estado
								nutricional tanto no prontuário quanto no Cartão
								da Gestante, marcando a informação no gráfico de
								acompanhamento nutricional, conforme a figura a
								seguir, para poder realizar as intervenções
								necessárias no tempo certo, visando prevenir o
								ganho de peso excessivo ao longo da gestação.
								Contudo, vale ressaltar a importância da
								realização de outros procedimentos que possam
								complementar o diagnóstico nutricional ou
								alterar a interpretação deste, conforme a
								necessidade de cada gestante.{" "}
							</p>
							<p className="textoUnidade" id="textoImg04">
								Você deve estar atento à possibilidade de
								diagnóstico de diabetes gestacional, observar se
								há presença de edemas que podem acarretar o
								aumento do peso sem indicar um ganho de peso
								excessivo e verificar o padrão de alimentação da
								gestante, questionando o consumo de alimentos
								como refrigerantes, doces, frituras,
								achocolatados e doces industrializados,
								frequência de consumo de água, frutas e
								vegetais. Todos esses fatores precisam ser
								avaliados em conjunto para a tomada de decisão
								quanto às orientações que serão realizadas.
							</p>
						</Col>
						<Col md="6" lg="6">
							<img
								src={img02}
								className="responsive "
								style={{
									height: this.state.height03 + "px"
								}}
							/>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p>
								{" "}
								Nos casos em que o manejo exigir cuidado
								especializado, deve-se contar com o apoio do
								nutricionista do Núcleo Ampliado de Saúde da
								Família e Atenção Primária à Saúde (NASF-AB) ou,
								em caso de ausência desse profissional na
								equipe, utilizar a Rede de Atenção à Saúde
								(RAS). Veja um exemplo: se a gestante chega à
								consulta da semana 8 com 59kg e medindo 1,59m de
								altura, você calculará o IMC da seguinte forma:
								59/(1,59)²= 23,3 kg/m².
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Com esse resultado, você verificará que a
								gestante se encontra com peso adequado para a
								idade gestacional. Logo em seguida, registrará
								essa informação no gráfico de acompanhamento do
								estado nutricional da gestante. Agora que
								sabemos como fazer esses procedimentos e
								registros, vamos conhecer sua operacionalização
								em cada estado nutricional pré-gestacional.
							</p>
						</Col>
					</Row>
				</Container>
				<TituloSubSecaoBarraNova
					tipoBarra={this.props.tipoUnidade + " degradeTitulo3"}
					titulo={
						"Monitoramento e acompanhamento da gestante com baixo peso pré-gestacional"
					}
				/>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Ao monitorar o estado nutricional da gestante
								que iniciou a gravidez com baixo IMC, é
								necessário que você investigue a sua história
								alimentar. Pergunte se a gestante vem sofrendo
								náuseas ou vômitos, qual o número de refeições
								realizadas, quantas horas de sono tem dormido
								por noite, se prepara as refeições ou
								alimenta-se fora de casa. Com base nas
								respostas, será possível viabilizar o
								encaminhamento necessário para adequar o ganho
								de peso dela ao longo da gestação.{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								A gestante que inicia as consultas de pré-natal
								com IMC adequado para a idade gestacional deve
								mesmo assim ter o estado nutricional avaliado em
								cada consulta. É importante que esse
								monitoramento seja realizado a fim de evitar um
								ganho de peso excessivo ao longo da gestação.
							</p>
						</Col>
						<Col md="6" lg="6">
							<p>
								Em situações em que a mulher inicia a gestação
								com sobrepeso ou obesidade, os cuidados devem
								ser redobrados. Assim como nos casos em que a
								gestante iniciou a gestação com peso adequado,
								mas passou a ganhar peso excessivo ao longo das
								consultas.
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Confira, as recomendações para gestantes segundo
								estado nutricional, clicando nas barras a
								seguir.
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<Accordion
								title={
									<p>
										<b>Gestante com baixo peso</b>
									</p>
								}
								content={
									<div className="fundoA1 borda10 espacamento">
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• Caso sinta necessidade
														de um acompanhamento
														individualizado para a
														gestante, você pode
														solicitar apoio ao
														nutricionista da equipe
														NASF ou, ainda,
														encaminhar para atenção
														especializada.{" "}
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• Verifique também a
														existência de hiperêmese
														gravídica, infecções,
														parasitoses, anemias e
														doenças debilitantes. Em
														caso positivo, direcione
														a gestante para o
														profissional de saúde
														indicado. Em casos de
														hiperêmese oriente-a a
														comer de maneira mais
														fracionada e em pequenas
														quantidades, evitando
														alimentos gordurosos.
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• Forneça orientação
														nutricional, visando à
														promoção do peso
														adequado e de hábitos
														alimentares saudáveis.
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• Remarque a consulta em
														um intervalo menor que o
														calendário habitual para
														monitoramento.{" "}
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• Converse com a
														gestante sobre a
														importância de ganhar
														peso adequadamente,
														explique que o ganho de
														peso deve ocorrer para
														garantir o
														desenvolvimento do bebê,
														formação de placenta e
														manutenção do líquido
														amniótico, e que com uma
														alimentação adequada
														esse peso será
														naturalmente eliminado
														nos primeiros meses
														pós-parto. Muitas mães
														têm medo de ganhar peso
														e não voltar à antiga
														forma física.{" "}
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• Lembre-se de que o
														acolhimento e a
														informação são muito
														importantes. Evitemos o
														julgamento e apenas dar
														ordens para a gestante.
														É muito importante
														conhecer o contexto em
														que essa mãe vive e, em
														caso de incapacidade
														financeira, verifique a
														possibilidade de
														encaminhá-la para a
														assistência social.
													</p>
												</Col>
											</Row>
										</Container>
									</div>
								}
							/>
							<Container className="espacamentoAccordion">
								<Row>
									<Col md="12" lg="12"></Col>
								</Row>
							</Container>

							<Accordion
								title={
									<p>
										<b>Gestante com peso adequado</b>
									</p>
								}
								content={
									<div className="fundoA1 borda10 espacamento">
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• Siga o calendário
														habitual de pré-natal,
														avaliando o peso e IMC a
														cada consulta.
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• Explique à gestante
														que seu peso está
														adequado para a idade
														gestacional, e
														encoraje-a a manter uma
														alimentação equilibrada
														e saudável, conforme
														recomendações já
														estudadas, visando à
														manutenção do peso
														adequado e à promoção de
														hábitos alimentares
														saudáveis.
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• Sempre registre o IMC
														no gráfico de
														acompanhamento e mostre
														para a gestante a sua
														evolução.
													</p>
												</Col>
											</Row>
										</Container>
									</div>
								}
							/>
							<Container className="espacamentoAccordion">
								<Row>
									<Col md="12" lg="12"></Col>
								</Row>
							</Container>

							<Accordion
								title={
									<p>
										<b>
											Gestante com peso elevado ou ganho
											excessivo
										</b>
									</p>
								}
								content={
									<div className="fundoA1 borda10 espacamento">
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• Adicionalmente, avalie
														a presença de edema,
														polidrâmnio,
														macrossomia, gravidez
														múltipla e doenças
														associadas (diabetes,
														pré-eclâmpsia, etc.).
														Nesses casos, encaminhe
														a gestante para
														atendimento
														individualizado com o
														nutricionista e/ou
														equipe multidisciplinar.
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• Em todas as situações,
														ofereça apoio e escuta.{" "}
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• Avalie os hábitos
														alimentares da gestante;
														verifique se o consumo
														de alimentos
														ultraprocessados é
														frequente. Você pode
														perguntar se a gestante
														faz consumo frequente de
														pães, embutidos,
														bolachas, doces e
														refrigerantes. Oriente a
														gestante a realizar
														caminhadas leves,
														hidratar-se
														adequadamente, e
														realizar consumo
														rotineiro de frutas e
														vegetais.
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• Remarque as consultas
														em intervalo menor que o
														fixado no calendário
														habitual para
														monitoramento do ganho
														de peso.
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• Ofereça orientação
														nutricional, visando à
														promoção do peso
														adequado e de hábitos
														alimentares saudáveis,
														ressaltando que, no
														período gestacional, não
														se deve perder peso, mas
														que o ganho de peso até
														o final da gestação não
														deve ultrapassar os 7 kg
														em situações de
														obesidade
														pré-gestacional ou os
														11,5 kg em casos de
														sobrepeso
														pré-gestacional.{" "}
													</p>
												</Col>
											</Row>
										</Container>
									</div>
								}
							/>
						</Col>
					</Row>
				</Container>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p>
								Essas situações aumentam o risco de retenção de
								peso no pós-parto, diabetes gestacional,
								pré-eclâmpsia e complicações no parto. Em
								qualquer uma dessas situações, faça o
								acolhimento e encaminhe-a para a nutricionista
								do NASF para acompanhamento individualizado.
							</p>
						</Col>
					</Row>
				</Container>

				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo3"
					}
					titulo={
						"RECOMENDAÇÕES PARA UMA ALIMENTAÇÃO SAUDÁVEL NA GESTAÇÃO"
					}
				/>
				<div className="fundoRespiro1">
					<Container id="sec24">
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									A gestação é uma fase muito importante na
									vida da mulher e requer alguns cuidados
									especiais na alimentação. Os níveis de
									nutrientes nos tecidos e líquidos
									disponíveis para sua manutenção estão
									modificados por alterações fisiológicas
									(expansão do volume sanguíneo, alterações
									cardiovasculares, distúrbios
									gastrintestinais e variação da função renal)
									e por alterações químicas. Por tais motivos
									é necessário que realizemos uma adequação na
									alimentação da gestante, visto que seu
									estado nutricional pode afetar o resultado
									da gravidez, parto e pós-parto.{" "}
								</p>
							</Col>
						</Row>
					</Container>

					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									O que importa na orientação nutricional da
									gestante não é o consumo de calorias, já que
									as necessidades aumentam em apenas 300
									calorias a partir do segundo trimestre
									gestacional. Para exemplificar, alguns
									alimentos que possuem cerca de 300 calorias
									são: uma porção pequena de castanha + 1
									fruta, ou 1 iogurte +1 fruta ou 1 sanduíche
									natural. Nas consultas, foque em orientar
									sobre a qualidade das escolhas alimentares,
									que devem priorizar o consumo de frutas,
									vegetais, leguminosas, tubérculos, cereais e
									carnes magras, evitando ao máximo o consumo
									de alimentos ultraprocessados.{" "}
								</p>
							</Col>
						</Row>
					</Container>

					<Container>
						<Row>
							<Col md="6" lg="6">
								<p className="textoUnidade">
									Maiores frequências de consumo de alimentos
									como gorduras hidrogenadas(trans) e açúcares
									estão associados ao aumento do ganho de peso
									gestacional excessivo e a uma maior
									adiposidade durante a primeira infância da
									criança. Portanto, é importante garantir uma
									boa qualidade da dieta na gestação.{" "}
								</p>
							</Col>
							<Col md="6" lg="6">
								<p>
									Os alimentos ultraprocessados, além de
									apresentarem altas concentrações de gordura,
									açúcar e sal, têm uma alta densidade
									energética, baixa densidade nutricional e
									escassez de fibras, tornando o padrão de
									alimentação favorável ao ganho de peso
									excessivo e prejudicial à saúde da gestante
									e do seu bebê (ROHATGI, 2017).
								</p>
							</Col>
						</Row>
					</Container>
				</div>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Mas, o que é alta densidade energética e
								densidade nutricional? Acompanhe as definições a
								seguir.
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<img
								src={densidade}
								className="responsive w100 borda10"
							/>
						</Col>
					</Row>
				</Container>

				{/*<Slider />*/}

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>

				<TituloSubSecaoBarraNova
					tipoBarra={this.props.tipoUnidade + " degradeTitulo3"}
					titulo={"Hábitos relacionados a alimentação na gestação"}
				/>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Normalmente, as pessoas esquecem que a forma e o
								local em que se alimentam pode fazer toda a
								diferença nas suas escolhas alimentares. Estudos
								indicam que o consumo em locais inadequados, com
								a presença de televisão ligada ou o uso de
								outros aparelhos eletrônicos, como celulares e
								tablets, estão associados com um consumo
								alimentar inadequado e que favorece a incidência
								de sobrepeso e obesidade (BRASIL, 2014).{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Como profissionais da saúde, devemos orientar as
								gestantes quanto ao modo de se alimentar. Agora,
								vamos abordar brevemente alguns pontos que podem
								ser orientados ao longo das consultas de
								pré-natal:
							</p>
						</Col>
					</Row>
				</Container>
				<div className="fundo2-02b">
					<Container>
						<Row>
							<Col md="6" lg="6" className="espacamento">
								<p className="textoUnidade espacamento">
									<b>
										Dar preferência para locais tranquilos,
										limpos e agradáveis:
									</b>{" "}
									o guia alimentar para a população brasileira
									reforça a importância de a gestante e sua
									família se alimentarem em casa ou em
									ambientes limpos e agradáveis, caso a
									refeição seja realizada fora do domicílio. É
									importante, também, que quando não for
									possível realizar as refeições em casa, a
									gestante escolha restaurantes a quilo ou com
									pratos feitos sem alimentos
									ultraprocessados.
								</p>
							</Col>
							<Col
								md="6"
								lg="6"
								className="espacamento espacamentoBottom"
							>
								<img
									src={img04}
									className="responsive w100 borda10"
								/>
							</Col>
						</Row>
					</Container>
				</div>
				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Oriente a gestante a evitar frequentar locais
								com fast-foods e lanches, já que estes não
								suprem as necessidades nutricionais e aumentam o
								risco de ganho de peso excessivo. Ao comer fora
								de casa, lugares como bufês ou aqueles onde se
								oferecem segundas ou terceiras porções sem custo
								devem ser limitados a ocasiões especiais. Também
								é importante orientar a gestante a evitar comer
								na mesa de trabalho, comer em pé ou andando ou
								comer dentro do carro ou de transportes
								públicos, embora, infelizmente, essas práticas
								possam ser comuns nos dias de hoje.
							</p>
						</Col>
					</Row>
				</Container>
				<div className="fundo02b">
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									<b>
										Explique para a gestante e sua família
										sobre a importância de comer em
										companhia:
									</b>{" "}
									segundo o nosso guia alimentar, seres
									humanos são seres sociais e o hábito de
									comer em companhia está impregnado em nossa
									história, assim como a divisão da
									responsabilidade por encontrar ou adquirir,
									preparar e cozinhar alimentos.{" "}
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									Converse com a gestante e sua família sobre
									a importância de compartilhar o comer e as
									atividades envolvidas neste ato, explique
									que esse é um modo simples e profundo de
									criar e desenvolver relações entre as
									pessoas e que, ao comer em companhia, ela
									será capaz de se alimentar com mais
									tranquilidade e menos pressa. Refeições
									compartilhadas demandam mesas e utensílios
									apropriados e compartilhar com outra pessoa
									o prazer que sentimos quando apreciamos uma
									receita favorita redobra esse prazer.
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="6" lg="6">
								<p className="textoUnidade">
									<b>
										Oriente a gestante e sua família sobre a
										importância de manter os telefones
										celulares e aparelhos de televisão
										desligados na hora das refeições:
									</b>{" "}
									já que esses dificultam a percepção dos
									sinais de saciedade pelo corpo. Explique que
									ao ter esse tipo de distrações dividimos a
									atenção do nosso cérebro entre o que comemos
									e o que estamos assistindo nas telinhas,
									comendo mais do que deveríamos, propiciando
									o ganho de peso excessivo.
								</p>
							</Col>
							<Col md="6" lg="6">
								<p className="textoUnidade">
									<b>
										Estimule a mulher a ter consciência em
										relação às quantidades colocadas no
										prato:
									</b>{" "}
									as pessoas tendem a comer mais que o
									necessário quando estão diante de grandes
									quantidades de alimentos ou quando há oferta
									de grandes porções. Nesses casos as escolhas
									são feitas sem pensar muito. Oriente a
									gestante a pensar em cada alimento que
									colocará no prato e na quantidade que será
									colocada.
								</p>
							</Col>
						</Row>
					</Container>
				</div>

				<Container id="sec24">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Lembre-a de que não é preciso nem recomendado
								“comer por dois”. Oriente a servir-se apenas uma
								vez ou, pelo menos, aguardar algum tempo para se
								servir uma segunda vez. Frequentemente, a
								segunda porção excede às nossas necessidades.
							</p>
						</Col>
					</Row>
				</Container>

				<TituloSubSecaoBarraNova
					tipoBarra={this.props.tipoUnidade + " degradeTitulo3"}
					titulo={"Escolhas alimentares na gestação"}
				/>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								As escolhas alimentares são fundamentais para a
								saúde da gestante, fase da vida em que as
								demandas nutricionais, ou seja, do consumo de
								nutrientes e não tanto do consumo de calorias,
								são essenciais, já que é preciso garantir um
								adequado estado nutricional para a gestante e
								para o bebê.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								É importante que você oriente as gestantes
								quanto à realização de escolhas alimentares
								baseadas em uma classificação que considera o
								nível de processamento dos alimentos, já que as
								escolhas alimentares não são feitas com base nos
								nutrientes (pois é difícil de quantificar no
								momento de escolher o que será consumido).{" "}
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Pense em quando você vai a um restaurante ou ao
								supermercado. Quando você escolhe uma pizza
								congelada para o seu jantar, provavelmente irá
								comprar um refrigerante ou suco de caixinha para
								acompanhar a sua escolha, isso conforma um
								padrão de alimentação não saudável e baseado em
								alimentos industrializados.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								No entanto, se você comprasse um pedaço de
								frango, feijão, arroz e vegetais para assar no
								forno, provavelmente as suas escolhas para o
								resto do dia também teriam maior chance de ser
								mais saudáveis. As orientações para as gestantes
								devem focar na qualidade dos alimentos, já que
								normalmente uma alimentação saudável e variada
								será capaz de suprir todas as necessidades
								nutricionais. A seguir, estão as orientações que
								você deve problematizar com a gestante nas
								consultas de pré-natal em relação às suas
								escolhas alimentares, acompanhe.
								{/*<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaReflexao"}
									icone={iconeReflexao}
									textoUnidade={
										"As orientações para as gestantes devem focar na qualidade dos alimentos, já que normalmente uma alimentação saudável e variada será capaz de							suprir todas as necessidades nutricionais."
									}
									inserirLink={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>*/}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="6" lg="6">
							<img
								src={img06}
								className="responsive w100 borda10"
							/>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Segundo o Guia Alimentar para População
								Brasileira (BRASIL, 2017), os alimentos{" "}
								<i>in natura</i> ou minimamente processados são
								a base para uma alimentação nutricionalmente
								balanceada, saborosa, culturalmente apropriada e
								promotora de um sistema alimentar socialmente e
								ambientalmente sustentável.
							</p>
							<p className="textoUnidade">
								Alimentos <i>in natura</i> ou minimamente
								processados fornecerão toda a qualidade
								nutricional que a gestante necessita para a
								manutenção de um estado nutricional adequado
								tanto do bebê quanto da mulher.
							</p>
							<p className="textoUnidade">
								Oriente a gestante a escolher frutas, vegetais,
								tubérculos e leguminosas da época, já que serão
								mais saborosos, acessíveis economicamente e
								conterão menos agrotóxicos.
							</p>
						</Col>
					</Row>
				</Container>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Veja alguns exemplos de alimentos{" "}
								<i>in natura</i> ou minimamente processados:
							</p>
						</Col>
					</Row>
				</Container>
				<div className="fundo2-01">
					<Container className="">
						<Row>
							<Col md="6" lg="6" className="espacamento">
								<ul>
									<li>
										<p className="textoUnidade">
											legumes, verduras, frutas, batata,
											mandioca e outras raízes e
											tubérculos <i>in natura</i> ou
											embalados, fracionados, refrigerados
											ou congelados;
										</p>
									</li>
									<li>
										<p className="textoUnidade">
											arroz branco, integral ou
											parboilizado, a granel ou embalado;{" "}
										</p>
									</li>
									<li>
										<p className="textoUnidade">
											{" "}
											milho em grão ou na espiga, grãos de
											trigo e de outros cereais;{" "}
										</p>
									</li>

									<li>
										<p className="textoUnidade">
											frutas secas, sucos de frutas e
											sucos de frutas pasteurizados e sem
											adição de açúcar ou outras
											substâncias;{" "}
										</p>
									</li>

									<li>
										<p className="textoUnidade">
											feijão de todas as cores, lentilhas,
											grão de bico e outras leguminosas;{" "}
										</p>
									</li>
									<li>
										<p className="textoUnidade">
											água potável;
										</p>
									</li>
								</ul>
							</Col>
							<Col md="6" lg="6" className="espacamento">
								<ul>
									<li>
										<p className="textoUnidade">
											{" "}
											castanhas, nozes, amendoim e outras
											oleaginosas sem sal ou açúcar;{" "}
										</p>
									</li>

									<li>
										<p className="textoUnidade">
											cravo, canela, especiarias em geral
											e ervas frescas ou secas;
										</p>
									</li>

									<li>
										<p className="textoUnidade">
											farinhas de mandioca, de milho ou de
											trigo e macarrão ou massas frescas
											ou secas feitas com essas farinhas e
											água;{" "}
										</p>
									</li>

									<li>
										<p className="textoUnidade">
											carnes de gado, de porco e de aves e
											pescados frescos, resfriados ou
											congelados;{" "}
										</p>
									</li>

									<li>
										<p className="textoUnidade">
											leite pasteurizado,
											ultrapasteurizado (“longa vida”) ou
											em pó, iogurte (sem adição de
											açúcar);{" "}
										</p>
									</li>

									<li>
										<p className="textoUnidade">ovos.</p>
									</li>
								</ul>
							</Col>
						</Row>
					</Container>
				</div>
				<Container className="espacamento espacamentoBottom">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								No momento de orientar a gestante quanto à
								escolha dos alimentos, lembre que nas refeições
								principais o prato deve conter:{" "}
							</p>
						</Col>
						<Col md="12" lg="12">
							<img
								src={tab1}
								className="responsive w100 borda10"
							/>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Dessa forma, a gestante será capaz de atingir
								todos os nutrientes necessários de maneira
								saborosa, acessível e completa no seu dia a dia.
								Lembre-se de respeitar e considerar o padrão
								cultural da região no momento de orientar a
								gestante quanto ao consumo alimentar. A
								preservação e o respeito da cultura alimentar é
								um dos princípios do guia alimentar (BRASIL,
								2017).
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Os ingredientes culinários como óleos, sal e
								açúcar devem ser utilizados com moderação no
								preparo dos alimentos. Consideramos como
								ingredientes culinários todos os ingredientes
								utilizados para o preparo de uma refeição: você
								pode orientar a gestante e sua família a
								utilizar óleos, gorduras, sal e açúcar em
								pequenas quantidades ao temperar e cozinhar
								alimentos e criar preparações culinárias como
								bolos caseiros, pães e biscoitos.{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Desde que utilizados com moderação em
								preparações culinárias com base em alimentos{" "}
								<i>in natura</i> ou minimamente processados, os
								óleos, as gorduras, o sal e o açúcar contribuem
								para diversificar e tornar mais saborosa a
								alimentação sem que fique nutricionalmente
								desbalanceada.
							</p>
						</Col>
					</Row>
				</Container>
				<div className="fundoA">
					<Container className="espacamento">
						<Row>
							<Col md="6" lg="6">
								<p>
									Você pode explicar para a gestante que óleos
									ou gorduras, por exemplo, serão utilizados
									para cozinhar arroz e feijão, para refogar
									legumes, verduras e carnes, para fritar ovos
									e tubérculos e no preparo de caldos e sopas.
									Óleos são também adicionados em saladas de
									verduras e legumes como forma de tempero.
									Mas oriente-as a utilizá-los com moderação.
									Existem pessoas que têm o costume de
									adicionar óleo no arroz e macarrão após o
									seu preparo, ultrapassando as necessidades
									nutricionais e favorecendo o ganho de peso
									excessivo. Por isso, desencoraje esse tipo
									de prática.
								</p>
							</Col>
							<Col md="6" lg="6">
								<p className="textoUnidade">
									O sal pode ser usado como tempero em todas
									essas preparações. Ele também é usado no
									preparo culinário de conservas de legumes
									feitas em casa e é adicionado à massa de
									farinha de trigo e água usada no preparo
									culinário de tortas e pães caseiros. Uma
									forma interessante de reduzir o consumo de
									sal é adicionar ervas aromáticas
									desidratadas como orégano, sálvia,
									cebolinha, cúrcuma e salsinha ao sal de mesa
									– ervas aromáticas possuem propriedades
									anti-inflamatórias e trazem um sabor
									adicional às preparações. Saiba sobre outras
									indicações{" "}
									<b
										className="cursorIcone"
										onClick={this.toggleCaixa3.bind(this)}
									>
										clicando no ícone{" "}
										<img
											src={iconeDestaque}
											className={"iconeTexto6"}
										/>
									</b>
								</p>
							</Col>
						</Row>
					</Container>
					{!this.state.isHiddenCaixa3 && <Caixa3 />}
				</div>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								O consumo excessivo de sódio e de gorduras
								saturadas aumenta o risco de doenças do coração,
								enquanto o consumo excessivo de açúcar aumenta o
								risco de cárie dental, de obesidade e de várias
								outras doenças crônicas. Óleos e gorduras têm
								seis vezes mais calorias por grama do que grãos
								cozidos e 20 vezes mais do que legumes e
								verduras após cozimento. O açúcar tem cinco a
								dez vezes mais calorias por grama do que a
								maioria das frutas. Entretanto, seu impacto
								sobre a qualidade nutricional da alimentação
								dependerá essencialmente da quantidade utilizada
								nas preparações culinárias. Por isso, oriente o
								uso desses produtos com moderação e equilíbrio!
								Alimentos processados devem ser consumidos pela
								gestante com moderação e combinados a
								preparações caseiras.
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Sabe aquele pepino ou vegetal em conserva, o
								pêssego em calda, o queijo feito de leite e sal
								ou aquele pãozinho feito de forma tradicional (a
								base de farinha de trigo, água, sal e leveduras
								usadas para fermentar a farinha)? Esses são os
								alimentos processados, os quais são produtos
								relativamente simples e antigos, fabricados
								essencialmente com a adição de sal ou açúcar (ou
								outra substância de uso culinário, como óleo ou
								vinagre) a um alimento <i>in natura</i> ou
								minimamente processado.
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								O consumo de alimentos processados na gestação
								(assim como em outras fases da vida) deve ser
								limitado, consumindo-os em pequenas quantidades.
								Eles podem ser usados como ingredientes de
								preparações culinárias ou como parte de
								refeições baseadas em alimentos <i>in natura</i>{" "}
								ou minimamente processados, já que embora o
								alimento processado mantenha a identidade básica
								e a maioria dos nutrientes do alimento do qual
								deriva, os ingredientes e os métodos de
								processamento alteram de modo desfavorável a sua
								composição nutricional. Em função do seu elevado
								conteúdo de sal ou açúcar, geralmente muito
								superior ao utilizado em preparações culinárias,
								o seu consumo excessivo está associado a doenças
								cardíacas, obesidade ou outras doenças crônicas.
							</p>
						</Col>
						<Col md="6" lg="6">
							<img
								src={img07}
								className="responsive w100 borda10"
							/>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Na consulta de pré-natal, você pode explicar
								para a gestante que alimentos processados, como
								no caso do queijo, podem ser usados com
								moderação, adicionado ao macarrão ou salada, por
								exemplo. Outro alimento comum são as carnes
								salgadas adicionadas ao feijão. Em outras vezes,
								como no caso de pães e peixes enlatados,
								alimentos processados podem compor refeições
								baseadas em alimentos <i>in natura</i> ou
								minimamente processados. Mas, deixe bem claro
								para a gestante, que sozinhos ou em combinação
								com outros alimentos processados ou
								ultraprocessados, eles podem ser prejudiciais à
								saúde, pelo seu elevado conteúdo de sal, açúcar
								e/ou gordura, não sendo recomendados, segundo o
								guia alimentar (BRASIL, 2017). Saiba mais sobre
								este tema
								<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaDestaque"}
									icone={iconeDestaque}
									textoUnidade={
										"Estimule a gestante e sua família a evitarem os alimentos ultraprocessados na rotina alimentar. Explique a eles os seus malefícios para a saúde da gestante."
									}
									inserirLink={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Nas consultas de pré-natal avalie a frequência
								de consumo de biscoitos recheados, chips e
								outros salgadinhos, macarrão instantâneo,
								refrigerantes, dentre outros alimentos
								ultraprocessados. Devido a seus ingredientes,
								são nutricionalmente desbalanceados e
								prejudiciais à saúde da gestante e do bebê.
								Devido a sua formulação e apresentação, tendem a
								ser consumidos em excesso, substituindo os{" "}
								<i>in natura</i> ou minimamente processados.
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Lembre-se que o problema principal com alimentos
								ultraprocessados reformulados é o risco de serem
								vistos como produtos saudáveis, cujo consumo não
								precisaria mais ser limitado. A publicidade
								desses produtos explora suas alegadas vantagens
								diante dos produtos regulares (‘menos calorias’,
								‘adicionado de vitaminas e minerais’),
								aumentando as chances de que sejam vistos como
								saudáveis pelas pessoas. (BRASIL, 2014)
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Classificamos como alimentos ultraprocessados
								vários tipos de guloseimas, bebidas adoçadas com
								açúcar ou adoçantes artificiais, pós para
								refrescos, embutidos e outros produtos derivados
								de carne e gordura animal, produtos congelados
								prontos para aquecer, produtos desidratados
								(como misturas para bolo, sopas em pó,
								“macarrão” instantâneo e “tempero“ pronto), e
								uma infinidade de novos produtos que chegam ao
								mercado todos os anos, incluindo vários tipos de
								salgadinhos “de pacote”, cereais matinais,
								barras de cereal, bebidas energéticas, entre
								muitos outros.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Pães e produtos panificados tornam-se alimentos
								ultraprocessados quando, além da farinha de
								trigo, leveduras, água e sal, seus ingredientes
								incluem substâncias como gordura vegetal
								hidrogenada, açúcar, amido, soro de leite,
								emulsificantes e outros aditivos.
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Uma forma prática para ajudar a gestante a
								distinguir alimentos ultraprocessados de
								alimentos processados é ensinando-as a consultar
								a lista de ingredientes que, por lei, deve
								constar dos rótulos de alimentos embalados que
								possuem mais de um ingrediente.
							</p>
						</Col>
					</Row>
				</Container>

				<DivHide
					textoClique={[
						<b>Clique aqui</b>,
						" para saber como reconhecer alimentos ultraprocessados"
					]}
					textoCaixa={
						<div>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p>
											<b>
												Como reconhecer alimentos
												ultraprocessados?
											</b>
										</p>
										<ul>
											<li>
												<p className="textoUnidade">
													eles têm um número elevado
													de ingredientes
													(frequentemente cinco ou
													mais);
												</p>
											</li>
											<li>
												<p className="textoUnidade">
													apresentam ingredientes com
													nomes pouco familiares e não
													usados em preparações
													culinárias, ingredientes que
													provavelmente você não teria
													na prateleira de sua casa
													(gordura vegetal
													hidrogenada, óleos
													interesterificados, xarope
													de frutose, isolados
													proteicos, agentes de massa,
													espessantes, emulsificantes,
													corantes, aromatizantes,
													realçadores de sabor e
													vários outros tipos de
													aditivos);
												</p>
											</li>
											<li>
												<p className="textoUnidade">
													diferentemente dos alimentos
													processados, a imensa
													maioria dos ultraprocessados
													é consumida, ao longo do
													dia, substituindo alimentos
													como frutas, leite e água
													ou, nas refeições
													principais, no lugar de
													preparações culinárias;{" "}
												</p>
											</li>
											<li>
												<p className="textoUnidade">
													alimentos ultraprocessados
													costumam vir em embalagens
													coloridas, com marketing
													bastante intenso e
													propagandas na televisão e
													redes sociais.
												</p>
											</li>
										</ul>

										<p className="textoUnidade">
											<b>
												Ao conversar com a gestante nas
												consultas de pré-natal, você
												pode enumerar os motivos pelos
												quais os alimentos
												ultraprocessados devem ser
												evitados na sua alimentação:{" "}
											</b>
										</p>
									</Col>
									<Col md="12" lg="12">
										<p className="textoUnidade deslocarEsqNum">
											1. Eles são frequentemente
											fabricados com gorduras que resistem
											à oxidação, mas que tendem a
											obstruir as artérias que conduzem o
											sangue dentro do nosso corpo.{" "}
										</p>
										<p className="textoUnidade deslocarEsqNum">
											2. Alimentos ultraprocessados tendem
											a ser muito pobres em fibras, que
											são essenciais para a prevenção de
											doenças do coração, diabetes e
											vários tipos de câncer.
										</p>
										<p className="textoUnidade deslocarEsqNum">
											3. Eles são pobres em vitaminas,
											minerais e outras substâncias com
											atividade biológica, aumentando a
											necessidade de uso de suplementos
											nutricionais de difícil acesso
											financeiro.
										</p>
									</Col>

									<Col md="12" lg="12">
										<p className="textoUnidade deslocarEsqNum">
											4. Quanto maior o consumo de
											alimentos ultraprocessados na rotina
											alimentar, maior será o ganho de
											peso gestacional, que poderá trazer
											complicações no parto e retenção de
											peso pós-parto. Quando as pessoas
											consomem alimentos ultraprocessados,
											tendem, sem perceber, a ingerir mais
											calorias do que necessitam; e
											calorias ingeridas e não gastas
											inevitavelmente acabam estocadas no
											corpo na forma de gordura, trazendo
											junto a obesidade.
										</p>
									</Col>

									<Col md="12" lg="12">
										<p className="textoUnidade deslocarEsqNum">
											5. Pelo seu elevado teor de sódio e
											açúcar de adição, alimentos
											ultraprocessados devem ser evitados
											completamente em casos de diabetes
											gestacional e pré-eclâmpsia. Ainda
											não existem estudos especificamente
											realizados em gestantes, mas
											diversos estudos realizados em
											adultos comprovam a relação entre o
											consumo de alimentos
											ultraprocessados e o risco aumentado
											de hipertensão arterial e de outras
											doenças crônicas não-transmissíveis.
										</p>
									</Col>
								</Row>
							</Container>
						</div>
					}
				/>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Atente-se com as calorias líquidas!
								Refrigerantes, refrescos e muitos outros
								produtos prontos para beber aumentam
								expressivamente o risco de obesidade e ganho de
								peso gestacional excessivo.
								{/*<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaDestaque"}
									icone={iconeDestaque}
									textoUnidade={
										"Atente-se com as calorias líquidas! Refrigerantes, refrescos e muitos outros produtos prontos para beber aumentam expressivamente o risco de obesidade e ganho de peso gestacional excessivo. "
									}
									inserirLink={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>*/}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade" id="textoImgN">
								O organismo humano apresenta menor capacidade
								para “registrar” calorias provenientes de
								bebidas adoçadas. Bebidas açucaradas possuem
								alto volume de calorias em pequenas quantidades
								do produto, são hiperpalatáveis, encontram-se em
								todo lugar e apresentam custo baixo, induzindo o
								consumo excessivo de calorias pelas gestantes.
								Fique atento e oriente quanto aos malefícios
								desse tipo de bebida na rotina alimentar.
							</p>
							<p className="textoUnidade" id="textoImgM">
								Estimule o consumo adequado de água na gestação.
								A hidratação é fundamental neste período e traz
								alguns benefícios elencados na figura a seguir.{" "}
							</p>
						</Col>
						<Col md="6" lg="6">
							<img
								src={img08}
								className="responsive w100 img08 borda10"
								style={{
									height: this.state.heightNM + "px"
								}}
							/>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								O total de água existente no corpo dos seres
								humanos corresponde a 75% do peso na infância e
								a mais da metade na idade adulta. É essencial
								que a água bebida seja potável para o consumo
								humano. A água fornecida pela rede pública de
								abastecimento deve atender a esses critérios,
								mas, na dúvida, filtrá-la e fervê-la antes do
								consumo garante sua qualidade.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Reforce para a gestante como é importante
								prestar atenção aos primeiros sinais de sede e
								satisfazer de prontidão a necessidade de água
								sinalizada pelo seu organismo. A água ingerida
								deve vir predominantemente do consumo de água
								como tal e da água contida nos alimentos e
								preparações culinárias. Oriente a gestante a
								ingerir, no mínimo, 2 litros de água ao dia, que
								equivale a aproximadamente 8 copos diários.
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								A maioria dos alimentos <i>in natura</i> ou
								minimamente processados e das preparações desses
								alimentos têm alto conteúdo de água. O leite e a
								maior parte das frutas contêm entre 80% e 90% de
								água. Verduras e legumes cozidos ou na forma de
								saladas costumam ter mais do que 90% do seu peso
								em água. Após o cozimento, macarrão, batata ou
								mandioca têm cerca de 70% de água. Um prato de
								feijão com arroz é constituído de dois terços de
								água.{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12"></Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12"></Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Quando a alimentação é baseada nesses alimentos
								e preparações, é usual que eles forneçam cerca
								de metade da água que precisamos ingerir. Já no
								caso de alimentos ultraprocessados, ocorre o
								contrário, já que a concentração de água nesses
								alimentos é pequena e assim necessita maior
								ingestão de água.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<TituloSubSecaoBarraNova
					tipoBarra={this.props.tipoUnidade + " degradeTitulo3"}
					titulo={"Planejamento de refeições saudáveis na gestação"}
				/>

				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								É comum a impressão de que a alimentação
								saudável é necessariamente muito cara e, ainda
								mais importante, muito mais cara do que a
								alimentação não saudável. Mas engana-se quem
								pensa assim: cálculos realizados com base nas
								Pesquisas de Orçamentos Familiares (POF) do IBGE
								mostram que, no Brasil, a alimentação baseada em
								alimentos <i>in natura</i> ou minimamente
								processados e em preparações culinárias feitas
								com esses alimentos não é apenas mais saudável
								do que a alimentação baseada em alimentos
								ultraprocessados, mas também mais barata, quando
								pensamos no gasto total diário com alimentação.
							</p>
						</Col>
						<Col md="6" lg="6">
							<img
								src={img09}
								className="responsive w100 borda10 img09"
							/>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Embora legumes, verduras e frutas possam ter
								preço superior ao de alguns alimentos
								ultraprocessados, o custo total de uma
								alimentação baseada em alimentos{" "}
								<i>in natura</i> ou minimamente processados
								ainda é menor no Brasil do que o custo de uma
								alimentação baseada em alimentos
								ultraprocessados.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container className="faixa01 espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Pesquisa realizada no Brasil com dados da POF
								mostrou que, efetivamente, comer uma preparação
								caseira custa mais barato que uma refeição
								baseada em alimentos ultraprocessados.
							</p>
						</Col>

						<Col md="12" lg="12">
							<p className="textoUnidade">
								<span className="cifrao">$</span> O preço médio
								resultante da associação dos alimentos{" "}
								<i>in natura </i>e minimamente processados aos
								ingredientes culinários, condição necessária
								para o preparo dos alimentos, foi de R$ 1,56,
								enquanto a média do preço verificada para os
								alimentos processados e ultraprocessados foi R$
								2,40.
							</p>
							<p className="textoUnidade">
								<span className="cifrao">$</span> Essa diferença
								sugere uma vantagem econômica no preparo de
								refeições no lar em comparação a sua
								substituição por refeições prontas e produtos
								alimentícios industrializados.{" "}
							</p>
							{/*<ul className="bodyBullet2">
								<li>
									<p className="textoUnidade">
										O preço médio resultante da associação
										dos alimentos <i>in natura</i> e minimamente
										processados aos ingredientes culinários,
										condição necessária para o preparo dos
										alimentos, foi de R$ 1,56, enquanto a
										média do preço verificada para os
										alimentos processados e ultraprocessados
										foi R$ 2,40.
									</p>
								</li>
								<li>
									<p className="textoUnidade">
										Essa diferença sugere uma vantagem
										econômica no preparo de refeições no lar
										em comparação a sua substituição por
										refeições prontas e produtos
										alimentícios industrializados.{" "}
									</p>
								</li>
							</ul>*/}
						</Col>
					</Row>
				</Container>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Quando a gestante chegar à consulta você pode
								conversar com ela sobre as suas escolhas
								alimentares. Pergunte o que ela costuma comer no
								café da manhã, no almoço, lanche da tarde e
								jantar, para ter uma ideia da frequência de
								consumo de preparações caseiras e de alimentos
								ultraprocessados.{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Quando a gestante referir consumir muitos
								alimentos ultraprocessados na rotina alimentar
								você pode explicar os malefícios de uma
								alimentação baseada nesses produtos, tanto para
								ela quanto para o bebê, e se ela referir que
								acha o custo dos alimentos <i>in natura</i> e
								minimamente processados pouco acessíveis, você
								pode ajuda-la, dando algumas dicas:
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="1" lg="1" className="no-gutters">
							<img src={seta} className="responsive w80" />
						</Col>
						<Col md="11" lg="11" className="alinharBulletP ">
							<p className="textoBullets2">
								Oriente a gestante a consumir frutas e vegetais,
								aliados a alimentos com menor preço como arroz,
								feijão, batata, mandioca, entre tantos outros
								que fazem parte das tradições culinárias
								brasileiras.{" "}
							</p>
						</Col>
						<Col md="1" lg="1" className="no-gutters">
							<img src={seta} className="responsive w80" />
						</Col>
						<Col md="11" lg="11" className="alinharBulletP ">
							<p className="textoBullets2">
								Nem todas as variedades de legumes, verduras e
								frutas são caras: oriente a compra de frutas e
								verduras da época e em locais onde se
								comercializam grandes quantidades de alimentos,
								em feiras ou diretamente dos produtores.{" "}
							</p>
						</Col>
						<Col md="1" lg="1" className="no-gutters">
							<img src={seta} className="responsive w80" />
						</Col>
						<Col md="11" lg="11" className="alinharBulletP ">
							<p className="textoBullets2">
								Para reduzir o custo de refeições feitas fora de
								casa, sem abrir mão de alimentos{" "}
								<i>in natura</i> ou minimamente processados,
								novamente são boas opções levar comida de casa
								para o trabalho ou comer em restaurantes que
								oferecem comida a quilo.
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Em muitos lugares do Brasil, existem também
								restaurantes populares e cozinhas comunitárias,
								que são espaços públicos que oferecem refeições
								variadas e saudáveis a preço reduzido. Se na sua
								região de atuação tiver esse tipo de opção,
								recomende para a gestante e sua família.
								Lembre-a de que levar comida feita em casa para
								o local de trabalho ou estudo é outra boa opção.
								Acesse mais recomendações
								<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaDestaque"}
									icone={iconeDestaque}
									textoUnidade={
										"Outra dica importante: oriente a gestante a cozinhar os alimentos sempre em uma quantidade um pouco maior da que será consumida no dia, assim ela poderá congelar os alimentos em potes porcionados para dias posteriores. "
									}
									inserirLink={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container className="espacamentoBottom">
					<Row>
						<Col md="12" lg="12">
							<img
								src={img13}
								className="responsive w100 borda10"
							/>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								A família pode preparar dois tipos de
								leguminosas como feijão branco e feijão preto
								para toda a semana e porcionar para os sete dias
								de consumo. A carne pode render um molho
								bolonhesa, uma carne refogada com legumes e
								almôndegas, e tudo pode ser preparado no mesmo
								dia e congelado para a semana; outra fonte de
								proteína pode ser o ovo frito ou cozido na hora
								e o frango pode ser porcionado, rendendo
								sobrecoxas ensopadas, coxas assadas, peito de
								frango que pode virar hambúrguer, frango
								grelhado ou à milanesa. Como fonte de
								carboidrato o mesmo arroz integral, preparado em
								um dia só, pode virar arroz com brócolis, arroz
								com lentilha, arroz colorido (refogado com
								cenoura, tomate, cebola e pimentão) e, de forma
								bem rápida, pode sair uma polenta ou macarrão
								para acompanhar as comidas em outros dias.
								Grande parte desses alimentos podem ser
								congelados e armazenados para serem consumidos
								na semana ou quinzena. Basta ter planejamento.
								Confira outras informações sobre como orientar
								esse planejamento
								{criarLink(
									"https://www.youtube.com/watch?v=Ltt6si2U39I&list=PLx-RfqJiTFaqc8_ei1-eHVBNB32hyP9aQ",
									"clicando no ícone",
									iconeLink,
									"iconeTexto12"
								)}
								{/*<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaSaibaMais"}
									icone={iconeSaibaMais}
									textoUnidade={
										" e confira outras informações sobre como orientar esse planejamento para as gestantes assistindo aos vídeos que foram elaborados especialmente pela equipe do Núcleo de pesquisa em Nutrição e Saúde Pública da Universidade de São Paulo em conjunto com a apresentadora Rita Lobo."
									}
									inserirLinkInicio={true}
									inserirLink={true}
									textoLink="Clique aqui"
									link={
										"https://www.youtube.com/watch?v=Ltt6si2U39I&list=PLx-RfqJiTFaqc8_ei1-eHVBNB32hyP9aQ"
									}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>{" "}*/}
							</p>
						</Col>
					</Row>
				</Container>

				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo3"
					}
					titulo={"MICRONUTRIENTES NA GESTAÇÃO"}
				/>

				<Container  id="sec25">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								O período gestacional, devido suas mudanças
								biológicas, traz algumas demandas de
								micronutrientes específicas. Deste modo,
								apresentaremos as recomendações de Ferro, Ácido
								Fólico e Vitamina D para mulheres durante a
								gestação.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<TituloSubSecaoBarraNova
					tipoBarra={this.props.tipoUnidade + " degradeTitulo3"}
					titulo={"Ferro: suplementação e fontes alimentares"}
				/>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								A gestação a termo confere quantidades
								suficientes de ferro para o feto, mesmo em
								situações de anemia ou desnutrição da mãe, pois
								a eritropoiese fetal é assegurada, utilizando-se
								as reservas maternas, mesmo que limitadas. Para
								garantir as reservas maternas de ferro,
								recomenda-se a ingestão de 27 mg/dia de ferro no
								segundo e terceiro trimestre da gestação, sendo
								a suplementação medicamentosa uma medida
								profilática recomendada pela OMS (OMS, 2013).{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								O Programa Nacional de Suplementação de Ferro
								consiste na suplementação profilática de ferro
								para todas as gestantes ao iniciarem o
								pré-natal, independentemente da idade
								gestacional até o terceiro mês pós-parto, e na
								suplementação de gestantes com ácido fólico. A
								suplementação deve ser recomendada pela equipe
								da atenção primária à saúde como parte do
								cuidado no pré-natal para reduzir o risco de
								baixo peso ao nascer da criança, anemia e
								deficiência de ferro na gestante (PNSF, 2013).
								Você deve, concomitantemente, recomendar o
								aumento na ingestão de ferro na dieta juntamente
								com alimentos ricos em vitamina C. Conheça
								alguns alimentos ricos em ferro que você pode
								orientar para sua paciente:
							</p>
						</Col>
					</Row>
				</Container>

				<SliderTab />

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Veja um esquema de alimentos que podem ser
								consumidos em um dia para suprir parte das
								necessidades de ferro:{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="6" lg="6">
							<img src={tab3a} className="" />
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Apesar de apresentar um consumo adequado de
								alimentos ricos em ferro, ainda se torna
								necessária a suplementação de ferro fornecida
								pelo Programa Nacional de Suplementação de
								Ferro. Oriente a gestante a consumir uma fruta
								cítrica (ou suco), ou a temperar a salada com
								limão. Laranja, tangerina, acerola, limão, kiwi,
								abacaxi são frutas cítricas ricas em vitamina C.
							</p>
						</Col>
					</Row>
				</Container>

				<TituloSubSecaoBarraNova
					tipoBarra={this.props.tipoUnidade + " degradeTitulo3"}
					titulo={"Ácido fólico: suplementação e fontes alimentares"}
				/>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								As gestantes devem consumir 400 microgramas/dia
								de ácido fólico. No Brasil, em 2002, a Agência
								Nacional de Vigilância Sanitária (ANVISA)
								instituiu a adição de 100 mg de ácido fólico
								para cada 100 gramas de farinha de trigo e
								milho, além de produtos derivados do milho
								comercializados no Brasil, alimentos que devem
								fazer parte do esquema alimentar para fornecer
								boa quantidade de ácido fólico.
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Veja um esquema de combinação de alimentos que
								podem atingir as necessidades em um dia comum:{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<img src={tab5} className="centerImg" />
						</Col>
					</Row>
				</Container>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Conheça alguns alimentos ricos em ácido fólico
								que você pode orientar para sua paciente:
							</p>
						</Col>
					</Row>
				</Container>

				<Slider2 className="" />

				<Container className="espacamentoBottom">
					<Row>
						<Col md="12" lg="12"></Col>
					</Row>
				</Container>

				<TituloSubSecaoBarraNova
					tipoBarra={this.props.tipoUnidade + " degradeTitulo3"}
					titulo={"A exposição ao sol para garantia dos níveis de vitamina D"}
				/>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								A recomendação para gestantes não difere daquela
								recomendada pra mulheres não grávidas, que é de
								5 mg/dia de vitamina D. Mulheres grávidas ou
								não, a exposição ao sol deve ser realizada
								diariamente, em torno de 15 a 20 minutos por
								dia, com o rosto, braços e colo expostos e sem o
								uso de proteção solar, sendo os melhores
								horários até as 10 horas e após as 16 horas.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								A vitamina D é um hormônio fundamental para um
								bom funcionamento do organismo, e em mulheres
								grávidas é ainda mais importante. Isso porque,
								além de ajudar a diminuir o risco de aborto
								espontâneo, a vitamina D também promove o
								crescimento saudável da placenta e pode reduzir
								risco de pré-eclâmpsia e de diabetes
								gestacional. Ela ajuda a regular a absorção de
								cálcio e fósforo, além de atuar na formação
								óssea, na liberação de insulina e no
								funcionamento do sistema imunológico.
							</p>
						</Col>
					</Row>
				</Container>
				<div className="">
					<TituloSecaoBarraNova
						tipoBarra={
							this.props.tipoUnidade +
							" espacamento degradeTitulo3 barraUnidade2"
						}
						titulo={
							" ALIMENTOS QUE DEVEM SER EVITADOS OU	CONSUMIDOS MODERADAMENTE DURANTE A GESTAÇÃO"
						}
					/>

					<Container>
						<Row>
							<Col md="12" lg="12">
								<p>
									<b>Chás e café</b>
								</p>
							</Col>
						</Row>
					</Container>
					<Container id="sec26">
						<Row>
							<Col md="6" lg="6">
								<p className="textoUnidade">
									O consumo de altas doses de cafeína na
									gestação pode estar associado ao aumento do
									risco de recém-nascido com baixo peso e de
									aborto. Sugere-se que o consumo inferior a
									300 mg de cafeína não se associa a efeitos
									adversos. A cafeína pode ser encontrada no
									café e em bebidas à base de café, chá-preto,
									chocolate, chimarrão, refrigerante a base de
									cola e em bebidas energéticas.{" "}
								</p>
							</Col>
							<Col md="6" lg="6">
								<p className="textoUnidade">
									A dose diária segura de cafeína para a
									gestante sem riscos para o bebê segue essas
									recomendações: até 3 xícaras (200 ml) de
									café coado, ou até 2 xícaras pequenas (50
									ml) de café expresso, ou até 2 xícaras (200
									ml) de café instantâneo. Vale salientar que
									o chocolate, seja sólido ou em pó, também
									possui cafeína: dois tabletes pequenos
									equivalem, em média, a 1 xícara de café
									coado.
								</p>
							</Col>
						</Row>
					</Container>

					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									Veja, a seguir, a concentração média de
									cafeína em alguns alimentos.{" "}
								</p>
							</Col>
						</Row>
					</Container>

					<Container>
						<Row>
							<Col md="12" lg="12">
								<img
									src={tabFinal}
									className="responsive w100 borda10"
								/>
							</Col>
						</Row>
					</Container>

					<Container className="espacamento">
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									Em relação aos chás e os riscos do seu
									consumo na gestação, ainda não há um
									consenso quanto ao seu uso por gestantes,
									mas devem ser evitados por não saber o
									possível efeito na saúde da gestante e no
									feto. Alguns estudos de revisão sistemática
									da literatura indicam efeitos abortivos ou
									relacionados a um maior risco de nascimento
									prematuro relacionados ao consumo de chás de
									camomila e de erva doce na gestação. Por
									esse motivo, desencoraje o consumo desses
									chás ao longo da gestação (FACCHINETTI et
									al., 2012; TRABACE et al., 2015).
								</p>
							</Col>
						</Row>
					</Container>

					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									<b>Adoçantes artificiais</b>{" "}
								</p>
							</Col>
						</Row>
					</Container>

					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									O uso de adoçantes artificiais deve ser
									desencorajado na gestação e só deve ser
									utilizado com moderação e avaliando o seu
									custo-benefício, por gestantes com diabetes
									gestacional ou diabetes tipo 1. Apesar de
									parecerem seguros para a saúde da gestante e
									do bebê, ainda não existe evidência
									suficiente para que o seu consumo seja
									liberado na gestação. Adicionalmente, o
									consumo de alimentos adoçados com adoçantes
									artificiais traz um paladar excessivamente
									adocicado e que parece estimular o consumo
									excessivo de calorias e de alimentos
									ultraprocessados, além de reduzir a
									palatabilidade de frutas e vegetais,
									aumentando consequentemente, o risco de
									ganho de peso excessivo na gestação (MALIK
									et al., 2013).
								</p>
							</Col>
						</Row>
					</Container>

					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									Em relação aos adoçantes que podem ser
									recomendados para gestantes diabéticas,
									alguns adoçantes como a sucralose, o
									acessulfame-k, o aspartame e a estévia
									parecem ser seguros, mas ainda não existem
									estudos controlados em humanos suficientes
									para realizar tal afirmação. O uso do
									sorbitol deve ser moderado, pois aumenta a
									excreção de minerais essenciais, como o
									cálcio. As gestantes devem restringir o uso
									de sacarina e ciclamato. Veja no quadro a
									seguir as características e indicação de uso
									dos principais edulcorantes.
								</p>
							</Col>
						</Row>
					</Container>

					<Container>
						<Row>
							<Col md="12" lg="12">
								<img
									src={tab8}
									className="responsive w100 espacamentoBottom"
								/>
							</Col>
						</Row>
					</Container>
				</div>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								<b>Bebidas alcóolicas</b>
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								O álcool, quando consumido pela gestante,
								atravessa a barreira placentária, expondo o feto
								às mesmas concentrações de álcool que o sangue
								materno é submetido. Porém, o efeito no feto é
								maior, devido ao metabolismo e à eliminação
								serem mais lentos, fazendo com que o líquido
								amniótico permaneça impregnado de álcool. Por
								isso, não existe uma dose segura recomendada
								para as gestantes. Adote a perspectiva de
								redução de danos e evite posições ameaçadoras ou
								julgamentos, que podem afastar a mulher do
								cuidado pré-natal. Oriente-a sobre os riscos do
								consumo de bebidas alcoólicas durante a
								gestação, principalmente nos primeiros 3 meses.
								Ofereça apoio no processo de construção de
								estratégias de prevenção/redução/eliminação do
								uso e, caso necessário, compartilhe o cuidado
								com os serviços da Rede de Atenção Psicossocial
								do território.
							</p>
						</Col>
					</Row>
				</Container>

				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade +
						" espacamento degradeTitulo3 barraUnidade2"
					}
					titulo={
						"ESTRATÉGIAS PARA INCLUSÃO DAS RECOMENDAÇÕES	ALIMENTARES NA ROTINA DE PRÉ-NATAL DA UBS"
					}
				/>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Agora, vamos abordar algumas recomendações,
								baseadas no Guia Alimentar para a População
								Brasileira, que podem ser realizadas nas
								consultas de pré-natal para garantir uma melhor
								e maior adesão a uma alimentação variada e
								baseada em preparações caseiras, com predomínio
								de alimentos <i>in natura</i> ou minimamente
								processados.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container id="sec27">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								As recomendações quanto a uma alimentação
								saudável e sobre os cuidados em relação ao ganho
								de peso devem ser abordadas ao longo das
								consultas de pré-natal, levando em consideração
								a situação da gestante, sua evolução ao longo
								dos trimestres e as dúvidas trazidas pela mulher
								e sua família.{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<DivHide
					textoClique={[
						<b>Clique aqui</b>,
						" e veja alguns pontos importantes que devem ser abordados"
					]}
					textoCaixa={
						<div>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											• O consumo de refeições baseadas em
											alimentos <i>in natura</i> ou
											minimamente processados pressupõe a
											seleção e aquisição dos alimentos, o
											pré-preparo, o tempero e cozimento e
											apresentação dos pratos, além da
											limpeza de utensílios e da cozinha
											após o término das refeições. Isso
											evidentemente requer tempo da
											própria pessoa ou de quem, na sua
											casa, é responsável pela preparação
											das refeições. Por isso, estimule a
											participação de todos os membros da
											casa para a divisão de tarefas e
											planejamento das refeições.
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											• Lembre-se de guiar a gestante e
											sua família a terem horários
											regulares e comerem em locais
											apropriados.
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											• É importante conversar com a
											família que na hora de comer não
											devem estar envolvidos em outra
											atividade, a desfrutar os alimentos,
											e a comer em companhia, de
											preferência.
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											• Dê exemplos práticos, mostrando
											que a diferença entre o preparo de
											uma refeição saudável pode não ser
											tão grande. Por exemplo, preparando
											um prato de macarrão com molho de
											tomate e temperos naturais o tempo é
											de apenas cinco minutos a mais do
											que a gestante gastaria para
											dissolver em água quente um pacote
											de “macarrão instantâneo” carregado
											de gordura, sal e aditivos.
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											• Peça para a família fazer compras
											de alimentos em mercados, feiras
											livres e feiras de produtores e em
											outros locais que comercializam
											variedades de alimentos{" "}
											<i>in natura</i> ou minimamente
											processados, dando preferência a
											alimentos orgânicos da agroecologia
											familiar.
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											• Lembre a gestante da importância
											de colocar frutas e vegetais no
											prato. As leguminosas como feijão,
											grão de bico, lentilha e ervilha
											também são de extrema importância e
											podem ser cozinhadas, fracionadas e
											congeladas para serem consumidas ao
											longo das semanas.
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											• Aconselhe a família sobre o
											planejamento das compras de
											alimentos, organização da despensa
											doméstica e definição com
											antecedência do cardápio da semana.
											Tudo pode ser feito com alimentos
											simples e com preço acessível.
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											• No dia a dia, para gestantes
											impossibilitadas de cozinhar, peça
											para procurar locais que servem
											refeições feitas na hora e a preço
											justo. Restaurantes de comida a
											quilo podem ser boas opções, assim
											como refeitórios que servem comida
											caseira em escolas ou no local de
											trabalho.
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											Quanto às orientações gerais sobre a
											escolha dos alimentos, segundo o
											Guia Alimentar, pequenas mudanças no
											consumo dos brasileiros que baseiam
											sua alimentação em alimentos in
											natura ou minimamente processados,
											incluindo o aumento na ingestão de
											legumes e verduras e a redução no
											consumo de carnes vermelhas,
											tornariam o perfil nutricional de
											sua alimentação praticamente ideal.
										</p>
									</Col>
								</Row>
							</Container>
						</div>
					}
				/>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Oriente a gestante a variar as frutas consumidas
								dentre as opções disponíveis na safra. As
								variações em torno dos alimentos de um mesmo
								grupo agradam também aos sentidos na medida em
								que permitem diversificar sabores, aromas, cores
								e texturas da alimentação. Esta é uma forma de
								garantir as necessidades nutricionais de forma
								prática e fácil. Confira as informações sobre a
								safra dos alimentos no Brasil{" "}
								{criarLink(
									"http://ftp.medicina.ufmg.br/omenu/safra_26_09_2014.pdf",
									"clicando no ícone",
									iconeLink,
									"iconeTexto12"
								)}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								<b>No café da manhã/lanches:</b>
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade" id="textoImg05">
								Com relação alimentos do café da manhã, a
								variedade é grande, incluindo o consumo de
								preparações à base de cereais (como cuscuz, pão
								integral ou francês, aveia, tapioca, ou até
								mesmo um bolinho feito em casa) ou de tubérculos
								(como mandioca/aipim, batata salsa/mandioquinha
								ou batata doce). O ovo é uma ótima fonte de
								proteína para o café da manhã ou para regiões
								onde o queijo branco é acessível, também pode
								ser uma escolha. A fruta não deve faltar no café
								da manhã da gestante, é ela que trará as
								vitaminas necessárias para o dia a dia. A
								variedade da alimentação deve refletir as
								preferências regionais.
							</p>
						</Col>
						<Col md="6" lg="6">
							<img
								src={cafe}
								className="responsive w100 borda10"
								style={{
									height: this.state.height05 + "px"
								}}
							/>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								<b>Almoço e jantar:</b>
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade" id="textoImg06">
								O almoço da gestante e de sua família deve
								incluir 1 porção de proteína animal ou vegetal,
								leguminosas (fonte de proteína vegetal), cereais
								ou tubérculos e vegetais/folhosos. Todos esses
								são alimentos <i>in natura</i> ou minimamente
								processados.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								A mistura de feijão com arroz é básica no Brasil
								e traz um aporte importante de ferro,
								carboidratos e proteína vegetal. O feijão pode
								ser o preto, vermelho, carioca ou fradinho, por
								exemplo. Nessa categoria também estão as
								leguminosas: ervilha, lentilha, grão de bico.
								Além do feijão cozido da forma tradicional, você
								pode lembrar a gestante da presença de
								preparações como tutu à mineira, grão-de-bico em
								salada, feijão tropeiro, sopa de feijão, de
								ervilha ou de lentilha, acarajé, entre muitas
								outras possibilidades. Ervilhas, lentilhas e
								grão-de-bico cozidos são consumidos também em
								saladas, por exemplo. O arroz pode ser
								substituído por outros cereais ou tubérculos da
								região como mandioca, farinha de mandioca,
								polenta, farinha e milho, macarrão, inhame,
								batata doce ou batata inglesa e aveia ou cevada.
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade" id="textoImg07">
								Carnes vermelhas (de gado ou de porco) devem ser
								restritas a um terço das refeições apresentadas,
								priorizando-se cortes magros e preparações
								grelhadas ou assadas. Visando apresentar opções
								de alimentos para substituir carnes vermelhas,
								podemos selecionar preparações grelhadas,
								assadas ou ensopadas de frango ou peixe, ovos
								(omelete) ou legumes (abóbora com quiabo, por
								exemplo). Quando a gestante não estiver em
								condições de comprar uma fonte de proteína
								animal, você pode orientá-la a aumentar a
								quantidade de leguminosa servida no prato.
							</p>
						</Col>
						<Col md="6" lg="6">
							<img
								src={almoco}
								className="responsive w100 borda10"
								style={{
									height: this.state.height07 + "px"
								}}
							/>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Boa parte dos legumes e verduras é
								comercializada em quase todos os meses em todas
								as regiões do país. No entanto, tipos e
								variedades produzidos localmente e no período de
								safra, quando a produção é máxima, apresentam
								menor preço, além de maior qualidade e mais
								sabor.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<div className="fundoUni2F">
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									A escolha da forma de preparo pode variar
									bastante de acordo com o tipo de legume ou
									verdura. Legumes e verduras são alimentos
									muito saudáveis. São excelentes fontes de
									várias vitaminas e minerais e, portanto,
									muito importantes para a prevenção de
									deficiências de micronutrientes. Além de
									serem fontes de fibras, fornecem, de modo
									geral, muitos nutrientes em uma quantidade
									relativamente pequena de calorias,
									características que os tornam ideais para a
									prevenção do consumo excessivo de calorias e
									do excesso de peso.
								</p>
							</Col>
						</Row>
					</Container>

					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade"></p>
							</Col>
						</Row>
					</Container>
					{/*<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								DESTAQUE{" "}
								<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaDestaque"}
									icone={iconeDestaque}
									textoUnidade={
										"Legumes e verduras são alimentos muito saudáveis. São excelentes fontes de várias vitaminas e minerais e, portanto, muito importantes para a prevenção de deficiências de micronutrientes. Além de serem fontes de fibras, fornecem, de modo geral, muitos nutrientes em uma quantidade relativamente pequena de calorias, características que os tornam ideais para a prevenção do consumo excessivo de calorias e do excesso de peso."
									}
									inserirLink={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>{" "}
							</p>
						</Col>
					</Row>
				</Container>*/}

					<TituloSecaoBarraNova
						tipoBarra={
							this.props.tipoUnidade +
							" espacamento degradeTitulo3"
						}
						titulo={"Encerramento da unidade"}
					/>

					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									Nesta unidade, abordamos os passos
									necessários para que você consiga avaliar
									adequadamente o estado nutricional da
									gestante. Lembre-se de que o primeiro passo
									sempre será calcular a idade gestacional da
									mulher, e o segundo será a avaliação do
									estado nutricional pré-gestacional, que
									servirá de base para a definição do estado
									nutricional inicial e definição do ganho de
									peso ao longo da gestação.{" "}
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									Também foram discutidos os principais pontos
									que devem ser abordados nas recomendações
									alimentares na gestação, alimentos que devem
									ser evitados e alimentos que devem fazer
									parte da base da alimentação. Abordamos a
									importância do consumo de água, os
									comportamentos alimentares e a sua
									influência na quantidade e qualidade da
									alimentação da gestante e a importância da
									suplementação de ferro e ácido fólico
									conforme a Política Nacional de
									Suplementação.{" "}
								</p>
							</Col>
						</Row>
					</Container>
				</div>
			</div>
		);
	}
}

const Caixa1 = () => (
	<Container className="espacamentoBottom">
		<Row>
			<Col md="12" lg="12">
				<CaixaTemplate
					textoClique={"clicando no ícone"}
					classImg={"iconeTexto12_2"}
					tipoCaixa={"caixaDestaque"}
					icone={iconeDestaque}
					textoUnidade={
						"Como tornar hábito uma alimentação saudável, utilizando os recursos econômicos disponíveis e estimulando o consumo dos alimentos produzidos localmente?"
					}
					inserirLink={false}
					proporcaoCol1={1}
					proporcaoCol2={11}
				></CaixaTemplate>
			</Col>
		</Row>
	</Container>
);

const Caixa3 = () => (
	<Container className="espacamentoBottom">
		<Row>
			<Col md="12" lg="12">
				<CaixaTemplate
					textoClique={"clicando no ícone"}
					classImg={"iconeTexto12_2"}
					tipoCaixa={"caixaDestaque"}
					icone={iconeDestaque}
					textoUnidade={[
						"Óleos, gorduras, sal e açúcar não substituem alimentos ",
						<i>in natura</i>,
						" ou minimamente processados. Esses são produtos alimentícios com alto teor de		calorias, gorduras saturadas (presentes em óleos e gorduras, em particular nessas últimas), sódio (componente básico do sal de cozinha) e açúcar livre (presente no açúcar de mesa)."
					]}
					inserirLink={false}
					proporcaoCol1={1}
					proporcaoCol2={11}
				></CaixaTemplate>
			</Col>
		</Row>
	</Container>
);

const Caixa2 = () => (
	<Container>
		<Row>
			<Col md="12" lg="12">
				<CaixaTemplate
					textoClique={"clicando no ícone"}
					classImg={"iconeTexto12_2"}
					tipoCaixa={"caixaReflexao"}
					icone={iconeReflexao}
					textoUnidade={
						"e acesse outras informações sobre os critérios para classificação do estado nutricional, de acordo com os índices antropométricos da criança."
					}
					inserirLink={true}
					link={
						"http://bvsms.saude.gov.br/bvs/publicacoes/estrategias_cuidado_doenca_cronica_obesidade_cab38.pdf"
					}
					inserirLinkInicio={true}
					textoLink={"Clique aqui "}
					proporcaoCol1={1}
					proporcaoCol2={11}
				></CaixaTemplate>
			</Col>
		</Row>
	</Container>
);

export default Unidade;
