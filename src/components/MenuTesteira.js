import './css/Testeira.css';
import 'font-awesome/css/font-awesome.min.css';
import { Container, Row, Col } from 'reactstrap';

var React = require('react');



class Menu extends React.Component {

	render() {

		return (
			<div className={this.props.tipoMenu}>
				<Container fluid={true}> 
					<Row >
						<Col md="5" lg="5">
							<p className = "tituloCurso">{this.props.nomeCurso}</p>
						</Col>

						<Col md="5" lg="5">						
							<p className = "tituloUnidade">{this.props.nomeUnidade}</p>
						</Col>
						<Col md="2" lg="2">
							<i className="fa fa-bars fa-3x logoRight"></i>

						</Col>
						

					</Row>

				</Container>
			</div>
			);
	}
};

export default Testeira;
