import 'font-awesome/css/font-awesome.min.css';
import '../css/animate.css';
import './CapaUnidade.css';
import { Container, Row, Col } from 'reactstrap';
import {Animated} from "react-animated-css";
import srcCapa from './img/camada01.png';
import srcNome from './img/camadaNome.png';




var React = require('react');









class CapaUnidade extends React.Component {

	render() {
/*animar o background ao inves da imagem capa1*/
		return (
			<div className="capa yellowBackground">
					<Container fluid={true}>
						<Row>
							<Col>
								<Animated animationIn="slideInLeft" animationInDuration={2000} animationOutDuration={1000} isVisible={true}>
									<img src={srcCapa} className="camada01"/>

								</Animated>	
									<Animated animationIn="fadeIn" animationInDuration={3000} animationInDelay={1000} animationOutDuration={1000} isVisible={true}>
										<img src={srcNome} className="camada02"/>
									</Animated>	
							</Col>
						</Row>
					</Container>
			</div>

			);
		}
	};

	export default CapaUnidade;
