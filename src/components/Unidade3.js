import React from "react";
import "./css/Unidade.css";
import "./css/Utilitarios.css";
import "./css/Unidade3.css";
import "font-awesome/css/font-awesome.min.css";
import {
	Container,
	Row,
	Col,
	Button,
	TabContent,
	TabPane,
	Nav,
	NavItem,
	NavLink,
	Card,
	CardTitle,
	CardText
} from "reactstrap";

import classnames from "classnames";

import clique from "../img/clique.png";

import { caixa } from "./Utilitarios.js";
import { criarLink } from "./Utilitarios.js";
import {
	TituloSecaoBarra2,
	TituloSubSecaoBarra,
	CaixaTemplate,
	CaixaGeralRetorno,
	TituloSecaoBarraNova,
	TituloSubSecaoBarraNova
} from "./Utilitarios.js";

import Modal from "./Modal.js";
import iconeSaibaMais from "../img/saibaMais.png";
import iconeReflexao from "../img/reflexao.png";
import iconeDestaque from "../img/destaque.png";
import iconeNota from "../img/nota.png";
import iconeLink from "../img/link.png";
import circle1 from "../img/circle1.png";
import circle2 from "../img/circle2.png";
import circle3 from "../img/circle3.png";

import imgInicial from "../img/info01.png";
import img01 from "../img/un3/img01.jpeg";
import img02 from "../img/un3/img02.png";
import img03 from "../img/un3/img03.png";
import tab01 from "../img/un3/tab01.png";
import tab02 from "../img/un3/tab02.png";
import tab03 from "../img/un3/tab03.png";

import img04 from "../img/un3/img04.png";

import testeiraIcones from "../img/uni3/testeiraIcones.png";

import iconeVideo from "../img/video.png";

import testeiraInfografico from "./../img/testeiraInfografico.png";

import IconeHide from "./IconeNota.js";
import DivHide from "./DivClickHide.js";
import Accordion from "./Accord3.js";

import AOS from "aos";
import "aos/dist/aos.css";

class Unidade extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			activeTab: "0"
		};
	}
	toggle(tab) {
		if (this.state.activeTab !== tab) {
			this.setState({
				activeTab: tab
			});
		}
	}

	render() {
		return (
			<div>
				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo3"
					}
					titulo={"Introdução"}
				/>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Nesta unidade, apresentaremos propostas de ações
								coletivas de educação nutricional na gestação
								para prevenção do sobrepeso e da obesidade com
								enfoque na Atenção Primária à Saúde, espaço
								preferencial para o desenvolvimento de ações de
								promoção de saúde.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Dada a importância da alimentação adequada e
								saudável para a promoção da saúde, a prevenção e
								o controle da obesidade nesse período, esta
								unidade foi criada com o objetivo de apoiar o
								planejamento e o desenvolvimento de ações
								coletivas de promoção da alimentação adequada e
								saudável durante a gestação, no âmbito da
								Atenção Primária à Saúde.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Recomendamos que ideias colocadas nessa unidade
								sejam implementadas em grupos de gestantes já
								existentes dentro da sua Unidade Básica de Saúde
								(UBS), visando trazer informações por meio de
								oficinas, rodas de conversa e folders que podem
								ser distribuídos nos grupos ou afixados na UBS.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade + " espacamento degradeTitulo3"
					}
					titulo={
						"ACOLHIMENTO, AÇÕES EDUCATIVAS E TROCA DE EXPERIÊNCIAS PARA GESTANTES"
					}
				/>

				<Container id="sec31">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								As intervenções pautadas no incentivo à
								alimentação adequada e saudável apresentam
								resultados satisfatórios, especialmente sobre o
								consumo alimentar e o perfil antropométrico dos
								indivíduos, dentre eles, as gestantes. As ações
								coletivas são a melhor escolha para atividades
								de educação nutricional, sobretudo por promover
								uma maior participação do usuário no processo
								educativo, no envolvimento da equipe com o
								participante e na otimização do trabalho
								(BRASIL, 2012). Veja mais sobre esse tema
								<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaDestaque"}
									icone={iconeDestaque}
									textoUnidade={
										"Por ser um local acessível para a população, a Unidade Básica de Saúde destaca-se como lócus prioritário por estar próximo à comunidade."
									}
									inserirLink={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Na UBS, abre-se a possibilidade de intercâmbio
								de experiências e conhecimentos relacionados a
								alimentação e nutrição, emoções e outros fatores
								ligados à gestação, favorecendo a compreensão do
								processo. A criação de espaços de educação em
								saúde sobre a alimentação adequada e saudável no
								pré-natal é de suma importância; afinal, nesses
								espaços, as gestantes podem ouvir e falar sobre
								suas vivências e consolidar informações
								importantes sobre o tema junto com outros
								assuntos que envolvem a saúde da criança, da
								mulher e da família dentro dos grupos de
								gestantes já existentes na UBS (BRASIL, 2012).{" "}
							</p>
						</Col>
						<Col md="6" lg="6">
							<img
								src={img01}
								className="responsive w100 borda10"
							/>{" "}
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Além da realização de atividades nos grupos
								específicos para gestantes, algumas ações
								simples podem ser realizadas em salas de espera,
								com cartazes afixados nos murais da UBS. Entre
								as diferentes formas de realização do trabalho
								educativo, destacam-se as discussões em grupo,
								as dramatizações e outras dinâmicas que
								facilitam a fala e a troca de experiências entre
								os componentes do grupo.{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row></Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Aproveite as intervenções coletivas como um
								espaço cooperativo para troca de conhecimentos
								entre as gestantes, seus parceiros e os
								profissionais, para o fortalecimento da
								sociabilidade, reflexão sobre a realidade
								vivenciada e criação de vínculo entre as
								famílias assim como entre você, profissional de
								saúde e a gestante. Ao atuar como facilitador do
								grupo ou oficina realizada, tente evitar o
								estilo “palestra”, que é pouco produtivo e
								ofusca questões subjacentes que podem ser mais
								relevantes para as pessoas presentes do que um
								roteiro preestabelecido. Confira mais
								informações sobre a importância das ações
								educativas e ferramentas de educação nutricional{" "}
								{criarLink(
									"http://bvsms.saude.gov.br/bvs/publicacoes/instrutivo_metodologia_trabalho_alimentacao_nutricao_atencao_basica.pdf",
									"clicando no ícone",
									iconeLink,
									"iconeTexto12"
								)}
								{/*<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaSaibaMais"}
									icone={iconeSaibaMais}
									textoUnidade={
										"e confira mais informações sobre a importância das ações educativas e ferramentas de educação nutricional acessando o “Instrutivo: metodologia de trabalho em grupos para ações de alimentação e nutrição na Atenção Primária à Saúde”."
									}
									inserirLink={true}
									textoLink={"Clique aqui "}
									link={
										"http://bvsms.saude.gov.br/bvs/publicacoes/instrutivo_metodologia_trabalho_alimentacao_nutricao_atencao_basica.pdf"
									}
									proporcaoCol1={1}
									proporcaoCol2={11}
									inserirLinkInicio={true}
								></IconeHide>{" "}*/}
							</p>
						</Col>
					</Row>
				</Container>

				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade +
						" espacamento degradeTitulo3 barraUnidade2"
					}
					titulo={
						"AÇÕES E PRÁTICAS COLETIVAS DE EDUCAÇÃO ALIMENTAR E NUTRICIONAL PARA UMA ALIMENTAÇÃO ADEQUADA E SAUDÁVEL NA GESTAÇÃO"
					}
				/>

				<Container id="sec32">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Um dos aspectos fundamentais a ser considerado
								no planejamento e na execução das ações
								coletivas de Educação Alimentar e Nutricional é
								a comunicação, devido à sua influência decisiva
								nos resultados das ações. Para o sucesso das
								ações, a comunicação deve ultrapassar os limites
								da transmissão de informações e a forma verbal,
								compreendendo um conjunto de processos
								mediadores da Educação Alimentar e Nutricional.
								Dessa maneira, recomenda-se que a comunicação
								seja pautada nos seguintes aspectos (BRASIL,
								2016):
							</p>
						</Col>
					</Row>
				</Container>
				<div className="fundo3-01">
					<Container className="espacamento">
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									• Escuta ativa e próxima: a gestante e sua
									família devem ter a oportunidade de se
									colocar dentro do grupo, tirando dúvidas e
									sugerindo temas que poderão ser abordados ao
									longo das oficinas.
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									• Construção partilhada de saberes, práticas
									e soluções.
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									• Valorização do conhecimento, da cultura e
									do patrimônio alimentar: na realização das
									atividades educativas, as práticas
									alimentares regionais e culturas alimentares
									devem ser respeitadas e levadas em
									consideração.
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									• Atender às necessidades dos indivíduos e
									grupos: é importante realizar um
									levantamento das características de idade,
									status socioeconômico e condições de saúde
									das pessoas que irão participar do grupo.
									Isto ajudará a planificar de forma mais
									acolhedora as ações a serem realizadas.
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									• Busca da formação de vínculo entre os
									diferentes sujeitos que integram o processo:
									como um grupo que irá se encontrar em várias
									ocasiões e que faz parte do território da
									UBS, é importante que seja criado um vínculo
									tanto entre as pessoas que participam do
									grupo como entre os profissionais atuantes e
									as gestantes. Isso ajuda a trazer uma maior
									confiança e sensação de acolhimento em
									relação ao aconselhamento realizado ao longo
									da gestação, além de proporcionar uma rede
									de apoio entre as gestantes do grupo.
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									• Busca de soluções contextualizadas: a
									apresentação das soluções apresentadas aos
									problemas e situações levantadas no grupo,
									devem estar contextualizadas à região,
									situação social e cultural das gestantes
									acompanhadas.
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									• Relações horizontais: a troca de saberes e
									a interação no grupo são horizontais e não
									devem apresentar nenhum tipo de hierarquia.
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									• Monitoramento permanente dos resultados.
								</p>
							</Col>
						</Row>
					</Container>
				</div>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Segundo o “Instrutivo - metodologia de trabalho
								em grupos para ações de alimentação e nutrição
								na Atenção Primária à Saúde” do Ministério da
								Saúde, as ações podem ser realizadas em formato
								de oficinas, de painéis e fixação de cartazes ou
								distribuição de folders informativos na rede de
								saúde. Conheça mais a fundo os formatos de
								atividades de educação nutricional que podem ser
								inseridos nos grupos de gestantes em relação à
								prevenção de sobrepeso e obesidade a seguir,
								confira.
							</p>
						</Col>
					</Row>
				</Container>

				<Container className="espacamentoBottom">
					<Row>
						<Col md="12" lg="12">
							<img
								src={img02}
								className="responsive w100 borda10 centerImg"
							/>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Para realizar um painel, a partir de um modelo
								base fixado no espaço do serviço, serão expostas
								diferentes atividades desenvolvidas durante os
								encontros, como imagens do grupo nas atividades,
								textos informativos sobre alimentação e
								nutrição, reflexões sobre como construir modos
								mais saudáveis na rotina de vida, entre outros.
								O painel ficará exposto no serviço e/ou no
								espaço para a apreciação dos usuários, sendo
								incentivada a participação em sua construção,
								bem como a sua exploração. Já o desenvolvimento
								da oficina pode ser estruturado em três
								momentos:
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Agora, vamos conhecer melhor cada uma das
								etapas:
							</p>
						</Col>
					</Row>
				</Container>
				<div className="fundoNav">
					<Container className="">
						<Row>
							<Col md="12" lg="12">
								<div className="" id="col-container">
									<Nav tabs className="deslocarNav">
										<Col
											md="3"
											lg="3"
											className="no-gutters col"
										>
											<NavItem className="">
												<NavLink
													className={classnames(
														"cursorPointer tamanhoNav bordaColuna degradeIMCUni3",
														{
															active:
																this.state
																	.activeTab ===
																"1"
														}
													)}
													onClick={() => {
														this.toggle("1");
													}}
												>
													<Col md="12" ls="12">
														<img
															src={clique}
															className="responsive imgClique"
														/>
													</Col>
													<Col
														md="12"
														ls="12"
														className="heightNav"
													>
														<p className="textCenter fontTab">
															<b>Inicial</b>
														</p>
													</Col>
												</NavLink>
											</NavItem>
										</Col>
										<Col
											md="3"
											lg="3"
											className="no-gutters col"
										>
											<NavItem className="">
												<NavLink
													className={classnames(
														"cursorPointer tamanhoNav bordaColuna degradeIMCUni3",
														{
															active:
																this.state
																	.activeTab ===
																"2"
														}
													)}
													onClick={() => {
														this.toggle("2");
													}}
												>
													<Col md="12" ls="12">
														<img
															src={clique}
															className="responsive imgClique"
														/>
													</Col>
													<Col
														md="12"
														ls="12"
														className="heightNav"
													>
														<p className="textCenter fontTab">
															<b>Intermediário</b>
														</p>
													</Col>
												</NavLink>
											</NavItem>
										</Col>

										<Col
											md="3"
											lg="3"
											className="no-gutters col"
										>
											<NavItem>
												<NavLink
													className={classnames(
														" cursorPointer tamanhoNav bordaColuna degradeIMCUni3",
														{
															active:
																this.state
																	.activeTab ===
																"3"
														}
													)}
													onClick={() => {
														this.toggle("3");
													}}
												>
													<Col md="12" ls="12">
														<img
															src={clique}
															className="responsive imgClique"
														/>
													</Col>
													<Col
														md="12"
														ls="12"
														className="heightNav"
													>
														<p className="textCenter fontTab">
															<b>
																Sistematização e
																avaliação
															</b>
														</p>
													</Col>
												</NavLink>
											</NavItem>
										</Col>
									</Nav>
									<div className="espacamentoBottom">
										<TabContent
											activeTab={this.state.activeTab}
											className="fundoAzulNav borda10"
										>
											<TabPane tabId="1">
												<Container>
													<Row>
														<Col md="12" lg="12">
															<Button
																color=""
																className="rightAlign corBotao"
																onClick={() => {
																	this.toggle(
																		"0"
																	);
																}}
															>
																<span className="corX">
																	X
																</span>
															</Button>
														</Col>
														<Col
															md="12"
															lg="12"
															className="esp1"
														>
															<p>
																Você e outros
																membros da
																unidade
																envolvidos no
																grupo realizarão
																a preparação dos
																participantes
																com apresentação
																dos objetivos e
																das atividades.
																Pode ser
																realizada por
																técnicas de
																“relaxamento”
																e/ou
																“aquecimento”,
																com utilização
																de atividades,
																brincadeiras,
																conversas,
																apresentação dos
																participantes do
																grupo e
																descrição de
																principais
																características
																de cada um, etc.
																Podem perguntar
																a idade
																gestacional,
																nome do bebê,
																com quem acham
																que o bebê irá
																se parecer,
																dentre outras
																dinâmicas para
																quebrar o gelo e
																depois
																apresentar os
																objetivos do
																grupo, abrindo
																sempre um espaço
																para sugestão
																pelas
																participantes e
																acompanhantes.
															</p>
														</Col>
													</Row>
												</Container>
											</TabPane>
											<TabPane tabId="2">
												<Container>
													<Row>
														<Col md="12" lg="12">
															<Button
																color=""
																className="rightAlign corBotao"
																onClick={() => {
																	this.toggle(
																		"0"
																	);
																}}
															>
																<span className="corX">
																	X
																</span>
															</Button>
														</Col>
														<Container className="esp1">
															<Row>
																<Col
																	md="12"
																	lg="12"
																>
																	<p className="textoUnidade">
																		Depois,
																		é
																		realizado
																		o
																		desenvolvimento
																		das
																		atividades.
																		Essas
																		devem
																		facilitar
																		a
																		reflexão
																		e a
																		elaboração
																		do tema.
																		Este
																		momento
																		pode ser
																		subdividido
																		em
																		quatro
																		pontos:{" "}
																	</p>{" "}
																</Col>
																<Col md="6" lg="6">
															<p className="textoUnidade">
																a) Utilização de
																técnicas
																lúdicas,
																sensibilização,
																motivação,
																reflexão e
																comunicação.{" "}
															</p>
															<p className="textoUnidade">
																c) Expansão das
																vivências,
																relacionando-as
																com situações do
																cotidiano.{" "}
															</p>
														</Col>
														<Col md="6" lg="6">
															<p className="textoUnidade">
																b) Conversa e
																reflexão sobre
																os sentimentos e
																ideias do grupo
																sobre as
																situações
																vivenciadas.{" "}
															</p>
															<p className="textoUnidade">
																d) Exposição e
																análise das
																informações
																sobre o tema
																comparadas às
																experiências dos
																participantes
																para
																esclarecimentos.{" "}
															</p>
														</Col>
															</Row>
														</Container>

														
													</Row>
												</Container>
											</TabPane>
											<TabPane tabId="3">
												<Container>
													<Row>
														<Col md="12" lg="12">
															<Button
																color=""
																className="rightAlign corBotao"
																onClick={() => {
																	this.toggle(
																		"0"
																	);
																}}
															>
																<span className="corX">
																	X
																</span>
															</Button>
														</Col>
														<Col
															md="12"
															lg="12"
															className="esp1"
														>
															<p>
																Após a
																realização da
																oficina, é
																momento de
																visualizar a
																produção do
																grupo,
																acompanhar o
																desenvolvimento
																da reflexão, do
																crescimento da
																autonomia e
																conhecimento dos
																participantes em
																relação ao tema
																abordado. Os
																participantes do
																grupo devem
																participar da
																tomada de
																decisões sobre
																os próximos
																encontros, ou
																seja, o produto
																da oficina deve
																ser construído
																coletivamente.
															</p>
														</Col>
													</Row>
												</Container>
											</TabPane>
										</TabContent>
									</div>
								</div>
							</Col>
						</Row>
					</Container>
				</div>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Lembre-se que o trabalho em grupo não deve ser
								pensado somente como forma de atender à demanda,
								mas sim como um espaço que propicia
								socialização, integração, apoio psíquico, trocas
								de experiências e de saberes e construção de
								projetos coletivos. Cada grupo de gestantes e
								seus parceiros trará prioridades e
								características próprias, e essas devem ser
								consideradas e contempladas levando em
								consideração as recomendações citadas acima, a
								região e as características da cultura alimentar
								local, assim como as angústias e questionamentos
								trazidos pelas gestantes.
							</p>
						</Col>
					</Row>
				</Container>

				<TituloSubSecaoBarraNova
					tipoBarra={this.props.tipoUnidade + " degradeTitulo3"}
					titulo={
						"Trabalhando crenças sobre a alimentação na gestação"
					}
				/>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Existem diversas crenças culturais que não
								apresentam nenhuma evidência científica em
								relação à alimentação na gestação e ao ganho de
								peso nessa fase. Dentre tantas, podemos citar
								algumas, tais como: grávida deve “comer por
								dois”, chás devem ser tomados livremente na
								gestação, as frutas podem engordar ou se comer
								muita fruta o bebê cresce demais. Pensando
								nisso, elaboramos uma proposta de atividade que
								você pode realizar no seu grupo de gestantes
								para desmistificar esse tipo de crenças,
								baseando-se em evidências científicas. A seguir
								explicamos o passo a passo de uma atividade, com
								as respostas corretas que devem ser repassadas
								nos grupos, levando como base evidências
								científicas.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Antes de começar a atividade, tentem conhecer um
								pouco sobre cada gestante e seu parceiro,
								identificando aquelas que já têm outros filhos e
								podem compartilhar as suas experiências
								anteriores, enriquecendo o diálogo entre o
								grupo.{" "}
							</p>
						</Col>
					</Row>
				</Container>
				<DivHide
					textoClique={[
						<b>Clique aqui</b>,
						" para conhecer melhor cada uma das etapas"
					]}
					textoCaixa={
						<Container>
							<Row>
								<Col md="12" lg="12">
									<p className="textoUnidade ">
										<b>OBJETIVO DA ATIVIDADE:</b>
									</p>
								</Col>
							</Row>

							<Container>
								<Row>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											• Desmistificar as crenças
											relacionadas à alimentação, nutrição
											e ganho de peso da gestante mediante
											uma atividade lúdica e descontraída.
										</p>
									</Col>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											• Sensibilizar as gestantes e seus
											parceiros sobre a real importância
											da alimentação saudável e
											equilibrada sem restrições ou medos.
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row></Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade ">
											<b>ATIVIDADES</b>
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											• Promover a integração do grupo
											mediante a atividade em turma e com
											a participação do casal.
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											• Realizar a atividade de educação
											de forma participativa, onde as
											gestantes e seus parceiros terão 2
											plaquinhas montadas pela equipe de
											saúde, uma escrita “CRENÇA” e outra
											escrita “VERDADE” que serão
											levantadas toda vez que o
											facilitador do grupo levantar uma
											questão relacionada à alimentação na
											gestação.
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade ">
											<b>
												MATERIAL NECESSÁRIO PARA A
												ATIVIDADE
											</b>
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											• Cartolinas brancas ou folhas
											brancas
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade ">
											<b>COMO APLICAR A ATIVIDADE</b>
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											Antes da realização do grupo: você e
											a equipe da unidade devem montar
											duplas de plaquinhas, com uma
											dizendo “CRENÇA” e outra dizendo
											“VERDADE”.{" "}
										</p>
									</Col>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											Monte o número de plaquinhas,
											considerando que cada pessoa deverá
											receber uma plaquinha “CRENÇA” e uma
											plaquinha “VERDADE”.
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											Além das placas, você deverá ter, em
											uma folha, as crenças mais comuns
											que são ditas em relação à
											alimentação e nutrição na gestação,
											juntamente com a resposta correta,
											informações que forneceremos a
											seguir. Se possível, convide a
											nutricionista do NASF para
											participar dessa atividade junto com
											você. No dia da oficina no grupo de
											gestantes:
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade"></p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											• Monte uma roda com as cadeiras
											para que todos os participantes
											possam se enxergar.
										</p>
									</Col>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											• Entregue, a cada participante, uma
											plaquinha de cartolina dizendo
											“CRENÇA” e outra dizendo “VERDADE”.
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											• Explique aos participantes do
											grupo, que você irá falar sobre
											algum assunto relacionado à
											alimentação na gestação e que
											gostaria que cada participante
											levantasse uma plaquinha, dizendo se
											acredita que o dizer é verdade ou
											crença.
										</p>
									</Col>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											• Após falar sobre o assunto,
											aguarde que todos os participantes
											do grupo levantem as suas
											plaquinhas, contabilize as respostas
											e anote em um quadro (se tiver
											disponível) ou caderno.
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											• Logo depois, você pode perguntar
											para um ou dos participantes, o
											motivo pelo qual eles acham que a
											frase falada é crença ou verdade,
											trazendo assim a participação e voz
											dos membros do grupo.
										</p>
									</Col>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											• E aí você pode levantar a
											plaquinha com a resposta certa,
											esclarecendo brevemente a questão.
											Você pode levantar entre 4 e 6
											assuntos diferentes. Mas, sinta qual
											está sendo a reação do grupo, se
											perceber que eles já estão cansados,
											pode encerrar a atividade com no
											mínimo 4 questões abordadas.
										</p>
									</Col>
								</Row>
							</Container>
						</Container>
					}
				/>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Para conhecer mais sobre a forma de abordagem
								das crenças e verdades sobre a alimentação na
								gestação, clique no infográfico a seguir.
							</p>
						</Col>
					</Row>
				</Container>

				<Container className="espacamentoIframe">
					<Row>
						<Col md="12" lg="12">
							<iframe
								src="https://unasus-quali.moodle.ufsc.br/mod/resource/view.php?id=1059"
								width="100%"
								height="700px;"
								frameborder="0"
								scrolling="no"
								className="centerIframe"
							></iframe>
						</Col>
					</Row>
				</Container>

				{/*<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p>
								<b>
									CRENÇAS E VERDADES QUE PODEM SER ABORDADAS:
								</b>{" "}
							</p>{" "}
						</Col>
					</Row>
				</Container>
				<Container className="fundo3-02 borda10">
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade centerText espacamento">
								<b>Crença</b>
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade centerText espacamento">
								<b>Verdade</b>{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container className="fundo3-02 borda10">
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade espacamento">
								1. Primeiro ponto: Gestante pode e deve “comer
								por dois”{" "}
								<b className="crenca">&#10006; Crença!</b>
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade espacamento">
								2. Segundo ponto: Gestante deve consumir café,
								chimarrão e chocolates com moderação. Já o
								refrigerante deve ser evitado sempre que
								possível{" "}
								<b className="verdade">&#10004; Verdade!</b>
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12"></Col>
					</Row>
				</Container>

				<Container className="fundo3-03 borda10 espacamento">
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Por muito tempo ouvimos falar que a gestação é o
								momento de se permitir e comer tudo o que
								quisermos, já que temos um bebê dentro da gente
								e por isso devemos “comer por dois”. Mas isso
								não é mais do que uma crença! A gestante deve
								comer a mesma quantidade de calorias que comia
								antes de engravidar, e a partir das 14 semanas
								de gestação só se acrescentam 300 calorias, que
								são facilmente supridas com um lanchinho
								adicional composto por uma vitamina de frutas +
								uma fatia de pão integral com manteiga, por
								exemplo, ou um punhado de castanhas e uma fruta,
								ou uma tapioca com um copo de suco de laranja
								(BRASIL, 2012a).
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Café, chimarrão, refrigerantes e chocolates são
								alimentos ricos em cafeína, e o consumo elevado
								de cafeína pode estar associado ao aumento do
								risco de recém-nascido com baixo peso e de
								aborto. Mas, calma, que eles podem ser
								consumidos, desde que com moderação.
								Recomenda-se que as gestantes tomem até 3
								xícaras de café coado ao dia (preferencialmente
								com ao menos um pouquinho de leite) ou 2 xícaras
								de café instantâneo (BRASIL, 2012a). Ah! E dois
								tabletes pequenos de chocolate equivalem a 1
								xicara de café coado!
							</p>

							<p className="textoUnidade">
								Já o refrigerante deve ser evitado por conter
								muitos aditivos químicos como corantes e
								saborizantes, além de um elevado nível de
								açúcar, que pode ser prejudicial e aumentar o
								ganho de peso excessivo na gestação.
							</p>
						</Col>
					</Row>
				</Container>

				<Container className="fundo3-02 borda10">
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade espacamento">
								3. Terceiro ponto: Comer comida feita em casa
								junto com frutas e vegetais é mais caro que
								comer alimentos ultraprocessados como salsichas
								com macarrão instantâneo ou produtos congelados
								<b className="crenca"> &#10006; Crença!</b>
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade espacamento">
								4. Quarto ponto: É melhor utilizar adoçantes
								artificiais do que adoçar as preparações com
								açúcar na gestação
								<b className="crenca"> &#10006; Crença!</b>
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12"></Col>
					</Row>
				</Container>

				<Container className="fundo3-03 borda10 espacamento">
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Já foi comprovado por estudos realizados no
								Brasil, que comer uma refeição feita em casa e
								que inclua frutas e/ou vegetais, tem um custo
								menor do que comer alimentos ultraprocessados!
								Em média, um prato feito em casa com alimentos
								saudáveis custa R$1,54 contra R$ 2,40 de
								alimentos ultraprocessados.
							</p>
							<p className="textoUnidade">
								E você que é gestante e está saudável e
								controlando o seu ganho de peso direitinho, pode
								utilizar o açúcar em preparações como bolos ou
								para adoçar o seu cafezinho, desde que com
								moderação!
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								O adoçante deve ser utilizado apenas por
								gestantes com diabetes ou diabetes gestacional.
								Mesmo assim o adoçante escolhido deve ser
								utilizado em quantidades e frequência reduzida.
								A gestante deve evitar o uso de ciclamato e
								sacarina e preferir o aspartame ou estévia.
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12"></Col>
					</Row>
				</Container>

				<Container className="fundo3-02 borda10">
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade espacamento">
								5. Quinto ponto: A gestante deve limitar seu
								consumo de frutas para não ganhar peso excessivo
								<b className="crenca"> &#10006; Crença!</b>
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade espacamento">
								6. Sexto ponto: Praticar atividade física leve é
								recomendado na gravidez e não traz nenhum risco
								para o bebê{" "}
								<b className="verdade"> &#10004; Verdade!</b>
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12"></Col>
					</Row>
				</Container>

				<Container className="fundo3-03 borda10 espacamento">
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Frutas podem e devem ser consumidas todos os
								dias na alimentação da gestante. Elas são fontes
								de fibra e vitaminas importantes para o
								desenvolvimento do bebê e para a manutenção da
								saúde da gestante e auxiliam na prevenção da
								constipação da mãe assim como na prevenção do
								ganho de peso excessivo (BRASIL, 2012a).
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								As atividades físicas recreativas, como
								caminhadas leves, são seguras durante a
								gravidez. No entanto, devem ser evitados
								exercícios que coloquem as gestantes em risco de
								quedas ou trauma abdominal (como esportes de
								contato ou de alto impacto). Recomenda-se a
								prática de exercícios moderados por 30 minutos,
								diariamente (BRASIL, 2012a).
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row></Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>*/}
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								No final da atividade, abra espaço para que as
								gestantes e seus parceiros façam questionamentos
								adicionais. Se o nutricionista ou obstetra
								estiverem presentes na oficina, eles poderão
								responder às questões adicionais. Caso eles não
								estejam presentes, você pode anotar os
								questionamentos e trazer as respostas no próximo
								encontro do grupo de gestantes.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>

				<TituloSubSecaoBarraNova
					tipoBarra={this.props.tipoUnidade + " degradeTitulo3"}
					titulo={
						"Reconhecendo os grupos de alimentos segundo nível de processamento industrial"
					}
				/>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Uma forma de trazer conhecimento sobre os grupos
								de alimentos segundo nível de processamento
								industrial e a importância das escolhas
								adequadas ao longo da vida é mediante a afixação
								de cartazes e entrega de folders aos usuários da
								UBS, incluindo gestantes e suas famílias.
								Confira, a seguir, alguns modelos.{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<img
								src={tab01}
								className="responsive w100 borda10"
							/>
						</Col>
					</Row>
				</Container>

				{/*	<Container >
							<Row >

								<Col md="1" lg="1" className="tab01 bordaTab">
									
								</Col>
								<Col md="11" lg="11" className="tab02">
									<p className="textoUnidade centerText espacamento"><b>Recomendações</b></p>
								</Col>
							</Row>
						</Container>*/}

				<Container>
					<Row>
						<Col md="12" lg="12">
							<img
								src={tab02}
								className="responsive w100 borda10"
							/>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>

				<TituloSubSecaoBarraNova
					tipoBarra={this.props.tipoUnidade + " degradeTitulo3"}
					titulo={"Orientação humanizada sobre o ganho de peso adequado na gestação"}
				/>

				<Container>
					<Row>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								No mesmo dia em que será realizada a oficina
								sobre crenças e verdades da gestação, você pode
								reservar um espaço para conversar com as
								gestantes e seus parceiros sobre os medos,
								dúvidas e preocupações em relação ao ganho de
								peso na gestação. Converse com o grupo,
								explicando que é normal e necessário que a
								gestante ganhe peso durante a gestação, e que um
								inadequado ganho de peso pode aumentar o risco
								de atraso de crescimento intrauterino e
								mortalidade perinatal.{" "}
							</p>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Ao longo da conversa, pare e ouça os
								questionamentos dos membros do grupo, e reforce
								a importância de mães e pais participarem das
								consultas de pré-natal para acompanhamento do
								ganho de peso a cada consulta. Explique que,
								nessas consultas, também serão abordadas as
								estratégias alimentares que poderão auxiliar na
								realização de escolhas adequadas para a gestante
								e sua família.
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Reforce também que a UBS será um espaço de
								acolhimento ao longo de todo o período de
								pré-natal, auxiliando-as na manutenção de um
								ganho de peso adequado e saudável, que levará em
								conta as especificidades de cada mulher.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade"></p>
						</Col>
					</Row>
				</Container>
				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade +
						" espacamento degradeTitulo3 barraUnidade2"
					}
					titulo={
						"ESTRATÉGIAS PARA LIDAR COM SINTOMAS COMUNS NA GESTAÇÃO"
					}
				/>

				<Container>
					<Row>
						<Col md="12" lg="12" id="sec33">
							<p className="textoUnidade">
								Existem diversos sintomas que são comuns na
								gestação, variando de gestante para a gestante
								na sua frequência, intensidade e aparição.
								Sintomas como náuseas, vômitos, excesso de
								apetite, dentre outros podem interferir na
								qualidade da alimentação da gestante aumentando
								o risco de ganho de peso insuficiente ou
								excessivo na gestação. Veja outras forma de
								lidar com esses sintomas
								<IconeHide
									textoClique={"clicando no ícone"}
									classImg={"iconeTexto12_2"}
									tipoCaixa={"caixaDestaque"}
									icone={iconeDestaque}
									textoUnidade={
										"A forma de lidar com esses sintomas deve ser discutida nos grupos de gestantes, assim como por meio de distribuição de folders na sala de espera das consultas de pré-natal."
									}
									inserirLink={false}
									proporcaoCol1={1}
									proporcaoCol2={11}
								></IconeHide>{" "}
							</p>
						</Col>
					</Row>
				</Container>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								A seguir, listamos os sinais e sintomas mais
								comuns na gestação que devem ser incluídos nos
								materiais de divulgação a serem distribuídos na
								UBS e discutidos nos grupos de gestantes. Clique
								nas barras abaixo para conhecê-los.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<Accordion
								title={
									<p>
										<b>Náuseas, vômitos e azia</b>
									</p>
								}
								content={
									<div className="fundoA1 borda10 espacamento">
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														Por volta de 70% das
														gestantes apresentam
														pelo menos um desses
														sintomas. Náuseas,
														vômitos e azia ocorrem
														geralmente nos primeiros
														meses de gravidez como
														resultado das alterações
														hormonais comuns na
														gestação. Tranquilize a
														gestante informando que
														esses sintomas são
														normais e que existem
														algumas estratégias para
														aliviá-los. Em folders e
														cartazes você pode
														referir a importância
														de:{" "}
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• fazer pequenas
														refeições em ambiente
														arejado, com intervalos
														de 2 horas;
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• restringir os
														alimentos com odores
														fortes e consumi-los em
														pequenas quantidades;{" "}
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• optar pelos cereais
														bem cozidos, os pães e
														torradas caseiras com
														geleia, as batatas bem
														cozidas, os ovos cozidos
														e a carne magra;{" "}
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• evitar os alimentos
														irritantes (exemplo: o
														café, o chá preto/verde,
														o chocolate e comida
														muito condimentada);{" "}
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• ingerir, de acordo com
														a tolerância individual,
														líquidos frios, cerca de
														1 a 2 horas, antes e
														após as refeições.
													</p>
												</Col>
											</Row>
										</Container>
									</div>
								}
							/>
							<Container className="espacamentoAccordion">
								<Row>
									<Col md="12" lg="12"></Col>
								</Row>
							</Container>
							<Accordion
								title={
									<p>
										<b>Constipação</b>
									</p>
								}
								content={
									<div className="fundoA1 borda10 espacamento">
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														Aproximadamente 35% a
														40% das mulheres
														grávidas passam por
														crises de constipação na
														gestação. Para prevenir
														ou aliviar esse sintoma,
														a gestante pode:
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• beber bastantes
														líquidos, nomeadamente
														água (2 litros por dia);
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														• aumentar a ingestão de
														alimentos ricos em fibra
														(pão integral, arroz
														integral, cereais
														integrais, legumes e
														frutas frescas e secas,
														especialmente ameixas e
														figos, quando
														acessíveis).
													</p>
												</Col>
											</Row>
										</Container>
									</div>
								}
							/>
							<Container className="espacamentoAccordion">
								<Row>
									<Col md="12" lg="12"></Col>
								</Row>
							</Container>
							<Accordion
								title={
									<p>
										<b>Apetite em excesso/ansiedade</b>
									</p>
								}
								content={
									<div className="fundoA1 borda10 espacamento">
										<Container>
											<Row>
												<Col md="12" lg="12">
													<p className="textoUnidade">
														Muitas mulheres referem
														sentir mais apetite que
														o normal depois que
														engravidam e sentem-se
														mal quando não ingerem
														alimentos ou bebidas em
														um curto espaço de
														tempo. Nessas situações,
														oriente a realização de
														pequenas refeições com
														intervalos menores de
														tempo.
													</p>
												</Col>
											</Row>
										</Container>
										<Container>
											<Row>
												<Col md="6" lg="6">
													<p className="textoUnidade">
														Você pode sugerir que as
														gestantes façam o
														desjejum, dois pequenos
														lanches matinais
														(compostos por
														fruta/castanhas, em
														regiões de fácil
														acesso), almoço com
														fruta de sobremesa, dois
														pequenos lanches
														vespertinos, jantar e
														ceia. Nesses casos, as
														refeições principais
														devem ser completas e os
														lanchinhos servidos em
														pequenas porções. Esses
														aspectos podem ser
														realizados em formato de
														oficina, solicitando
														para as gestantes
														reproduzirem, em uma
														cartolina, os horários e
														alimentos que colocam no
														prato nas diversas
														refeições.
													</p>
												</Col>
												<Col md="6" lg="6">
													<p className="textoUnidade">
														{" "}
														Questione quantas delas
														sofrem de apetite em
														excesso e ansiedade.
														Depois, apresente, em
														slides ou em cartolina,
														um esquema do número de
														refeições recomendado e
														a importância de se
														alimentar em pequenas
														quantidades várias vezes
														ao dia. Também é
														importante que você
														lembre às gestantes
														sobre a importância do
														consumo adequado de
														água, já que, muitas
														vezes, situações de
														desidratação inicial
														podem ser confundidas
														com fome.
													</p>
												</Col>
											</Row>
										</Container>
									</div>
								}
							/>
						</Col>
					</Row>
				</Container>

				<Container className="espacamento">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Reforce a importância de apreciar cada refeição,
								comer devagar, mastigar bem e de maneira que
								evite qualquer tipo de distração ou estresse na
								hora da alimentação. Ter um bom relacionamento
								com a comida ajudará a evitar o consumo
								excessivo, e a mastigação adequada auxiliará na
								digestão e ativação dos sinais de saciedade.
							</p>
						</Col>
					</Row>
				</Container>

				<TituloSecaoBarraNova
					tipoBarra={
						this.props.tipoUnidade +
						" espacamento degradeTitulo3 barraUnidade2"
					}
					titulo={
						"PLANEJAMENTO EM FAMÍLIA PARA PREVENÇÃO DE SOBREPESO E OBESIDADE"
					}
				/>

				<Container id="sec34">
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								É imprescindível que as gestantes e seus
								acompanhantes – sejam eles os(as)
								companheiros(as) ou membros da família ou seus
								amigos – tenham contato com atividades de
								educação, pois muitas vezes este é o espaço onde
								se compartilham dúvidas e experiências que
								normalmente não são discutidas em consultas
								formais, dentro dos consultórios dos médicos,
								enfermeiros ou dentistas (BRASIL, 2016).
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Segundo o Guia Alimentar para a População
								Brasileira (BRASIL, 2014), o planejamento
								familiar é uma das ferramentas essenciais para a
								garantia de uma alimentação adequada e saudável,
								e o compartilhamento das tarefas tem papel
								fundamental na garantia do bem-estar da mulher
								na gestação e pós-parto imediato. Reduzindo
								situações de sobrecarga física e emocional da
								gestante, reduzimos o estresse e a ansiedade,
								que são considerados dois gatilhos importantes
								para o comer não-saudável na gestação e para a
								redução dos níveis de ocitocina e prolactina no
								pós-parto, o que dificulta a descida e ejeção do
								leite materno.
							</p>
						</Col>
					</Row>
				</Container>

				<Container className="espacamentoBottom">
					<Row>
						<Col md="6" lg="6">
							<img
								src={img04}
								className="responsive w100 borda10"
							/>
						</Col>
						<Col md="6" lg="6">
							<p className="textoUnidade">
								Reforce que a amamentação é essencial, pois além
								de nutrir o bebê, trazer proteção imunológica,
								prevenindo infecções gastrointestinais e
								respiratórias, assim como doenças crônicas não
								transmissíveis em longo prazo, dentre outros
								fatores, também traz benefícios importantes para
								a mulher, dentre eles, uma maior facilidade de
								recuperação do estado nutricional
								pré-gestacional, maior rapidez na descida do
								útero, prevenção de anemia, câncer de útero e de
								mamas.
							</p>
						</Col>
					</Row>
				</Container>

				<TituloSubSecaoBarraNova
					tipoBarra={this.props.tipoUnidade + " degradeTitulo3"}
					titulo={"Orientações básicas para inclusão e incentivo à participação dos parceiros nas consultas de pré-natal e na divisão de tarefas culinárias"}
				/>

				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Considerando a importância do compartilhamento
								de tarefas culinárias para a garantia de uma
								alimentação saudável e adequada durante toda a
								gestação, nas consultas individuais e nas salas
								de espera, converse com os casais ou gestantes e
								acompanhantes sobre a importância da
								participação do acompanhante nos grupos de
								gestantes e durante as atividades realizadas, e
								reforce a importância da participação do
								parceiro em todos os acompanhamentos de
								pré-natal.
							</p>
						</Col>
					</Row>
				</Container>
				<Container>
					<Row>
						<Col md="12" lg="12">
							<p className="textoUnidade">
								Para incentivar a participação de toda a família
								na divisão de tarefas relacionadas ao preparo
								das refeições, sugerimos uma atividade que
								funciona como um diário de bordo em que cada
								integrante da família definirá a sua função em
								cada dia da semana com o intuito de facilitar a
								rotina familiar. A seguir descrevemos a
								atividade em detalhes.
							</p>
						</Col>
					</Row>
				</Container>
				<DivHide
					textoClique={[
						<b>Clique aqui</b>,
						" e leia o objetivo da atividade"
					]}
					textoCaixa={
						<div>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade ">
											<b>OBJETIVO DA ATIVIDADE:</b>
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade"></p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											• De forma descontraída e divertida,
											mostrar a necessidade da
											participação de todos os membros da
											casa na divisão de tarefas
											culinárias.
										</p>
									</Col>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											• Apresentar um diário de bordo como
											estratégia de participação de toda a
											família no preparo dos alimentos.
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade ">
											<b>ATIVIDADES</b>
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											• Atividade 1: Roda de participação
											nas atividades culinárias
										</p>
									</Col>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											• Atividade 2: Montagem de um diário
											de bordo com sugestão de divisão de
											tarefas culinárias da família
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade ">
											<b>
												MATERIAL NECESSÁRIO PARA A
												ATIVIDADE
											</b>
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											• Folhas impressas com a tabelinha
											do diário de bordo
										</p>
									</Col>
									<Col md="6" lg="6">
										<p className="textoUnidade">• Caneta</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											<b>COMO APLICAR A ATIVIDADE</b>
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											Para Imprimir o diário de
											bordo/tabelinha de divisão de
											tarefas{" "}
											<a
												href="https://unasus-quali.moodle.ufsc.br/mod/resource/view.php?id=1060"
												target="_blank"
												className="cursorIcone"
											>
												<b>clique aqui</b>
											</a>
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											Exemplo de diário de bordo
											preenchido:
										</p>

										<img
											src={tab03}
											className="responsive w100 borda10 espacamentoBottom"
										/>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											Diário de bordo para planejamento
											semanal de preparo das refeições
											para impressão (imprima o número
											adequado para os participantes do
											grupo). Agora, confira as ações a
											serem realizadas no dia da oficina
											no grupo de gestantes. Acompanhe!
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											<b>
												ATIVIDADE 1 – Dinâmica com os
												participantes:
											</b>
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											É fundamental que nesta atividade a
											gestante esteja acompanhada pela
											pessoa que divide a casa com ela,
											sejam os pais, parceiro(a) e/ou
											filhos.
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											Peça para todos os participantes se
											colocarem em uma grande roda, e
											diga:
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											Agora, vamos fazer uma brincadeira.
											Eu vou falar algumas atividades
											cotidianas realizadas para o preparo
											das refeições diárias e quero que
											aquela pessoa da casa responsável
											pela tarefa ou que divide a
											atividade na casa, dê um passo a
											frente. Aquele que não participa
											dessa tarefa, fica onde está.{" "}
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											Depois da instrução, leia cada
											atividade e espere que as pessoas se
											movimentem.
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											<b>
												Atividades que podem ser
												citadas:
											</b>
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											• Dê um passo à frente quem tem
											costume de lavar a louça{" "}
										</p>
										<p className="textoUnidade">
											• Dê mais um passo à frente quem tem
											costume de secar a louça
										</p>
										<p className="textoUnidade">
											• Dê mais um passo à frente quem
											participa do pré-preparo das
											refeições
										</p>
									</Col>
									<Col md="6" lg="6">
										<p className="textoUnidade">
											• Dê mais um passo à frente quem
											monta a mesa antes de comer
										</p>
										<p className="textoUnidade">
											• Um passo à frente quem retira os
											pratos da mesa da refeição
										</p>
										<p className="textoUnidade">
											• Um passo a frente quem vai às
											compras dos ingredientes para a
											comida
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											Depois de finalizada a brincadeira,
											peça para todos voltarem aos seus
											assentos e pergunte qual foi a
											impressão do jogo. Alguém se
											percebeu sobrecarregado nas
											atividades? Como cada um se sentiu?
											Quem está precisando participar um
											pouco mais da divisão de tarefas e
											como isso seria possível?
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											<b>
												ATIVIDADE 2 – DIÁRIO DE BORDO
												PARA PLANEJAMENTO SEMANAL E
												DIVISÃO DE TAREFAS
											</b>
										</p>
									</Col>
								</Row>
							</Container>

							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											Após a dinâmica, com todos os
											participantes sensibilizados e
											ouvidos, você pode propor a montagem
											de um plano de ação. No dia do
											grupo, você pode orientar as
											famílias dizendo:
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											Queridos participantes, queridas
											famílias, gostaram do primeiro
											exercício?! Para auxiliá-los no
											planejamento da divisão de tarefas
											culinárias, montamos essa tabelinha,
											que funciona como um diário de bordo
											para que nas suas casas, com todos
											os integrantes da família, montem um
											plano de ação para a semana, onde
											cada um colocará qual atividade irá
											realizar na divisão das tarefas.
											Dessa forma, todos se alimentarão de
											maneira deliciosa, saudável,
											prazerosa e sem sobrecarregar
											ninguém em casa. Não é legal?!
										</p>
									</Col>
								</Row>
							</Container>
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade">
											Nessa tabela, na primeira coluna
											temos os dias da semana, a seguir
											vocês podem colocar as atividades
											relacionadas ao preparo dos
											alimentos e montagem da mesa, como
											está no exemplo. E finalmente há a
											coluna onde vocês definirão quem é
											ou são as pessoas responsáveis por
											cada atividade. No próximo dia do
											grupo nos contem como foi!
										</p>
									</Col>
								</Row>
							</Container>
						</div>
					}
				/>
				<div className="fundoUni3 espacamento">
							<Container>
								<Row>
									<Col md="12" lg="12">
										<p className="textoUnidade"></p>
									</Col>
								</Row>
							</Container>
							
			

					<TituloSecaoBarraNova
						tipoBarra={
							this.props.tipoUnidade +
							" espacamento degradeTitulo3"
						}
						titulo={"Encerramento da unidade"}
					/>
			
					<Container className="">
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									Nesta unidade estudamos os aspectos
									relevantes e didáticos trabalhados no
									coletivo. A intenção de aproveitar o espaço
									e a oportunidade de compartilhamentos de
									experiências e conhecimentos por meio da
									realização de atividades de educação
									nutricional em grupos para gestantes já
									existentes nas UBS traz uma oportunidade
									tanto para os profissionais da Atenção
									Primária à Saúde quanto para as gestantes e
									seus acompanhantes, complementando e
									reforçando temas discutidos nas consultas
									individuais e trazendo as vivências do grupo
									à tona para tomada de decisões assertivas.
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade">
									Também discutimos aspectos relevantes para a
									conscientização da necessidade de um ganho
									de peso adequado e saudável para prevenção
									de sobrepeso e obesidade, além de outras
									complicações relacionadas ao parto e ao
									bebê. Temos certeza que, ao aplicar as
									estratégias sugeridas, possibilitaremos o
									acolhimento, informação e adesão a hábitos
									de vida saudáveis pela gestante e sua
									família.
								</p>
							</Col>
						</Row>
					</Container>
					<Container>
						<Row>
							<Col md="12" lg="12">
								<p className="textoUnidade"></p>
								<br/>
								<br/>
							</Col>
						</Row>
					</Container>
				</div>
			</div>
		);
	}
}

export default Unidade;
