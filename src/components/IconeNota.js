import React from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Row,
  Col
} from "reactstrap";
import iconeSaibaMais from "../img/saibaMais.png";
import iconeReflexao from "../img/reflexao.png";
import iconeDestaque from "../img/destaque.png";
import iconeNota from "../img/nota.png";
import iconeLink from "../img/link.png";

import iconeVideo from "../img/video.png";
import ScrollAnimation from "react-animate-on-scroll";
import "./css/Utilitarios.css";
import {
  TituloSecaoBarra2,
  TituloSubSecaoBarra,
  CaixaTemplate,
  CaixaGeralRetorno,
  TituloSecaoBarraNova
} from "./Utilitarios.js";

class IconeHide extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isHidden: true
    };
  }

  toggleHidden() {
    this.setState({
      isHidden: !this.state.isHidden
    });
  }
  render(props) {
    return (
      <span className="cursorClicavel" onClick={this.toggleHidden.bind(this)}>
        <b className="blackLink">
          {" " + this.props.textoClique + " "}
          <img src={this.props.icone} className={this.props.classImg} />
        </b>

        {!this.state.isHidden && (
          <Child
            tipoCaixa={this.props.tipoCaixa}
            icone={this.props.icone}
            textoUnidade={this.props.textoUnidade}
            link={this.props.link}
            inserirLink={this.props.inserirLink}
            textoLink={this.props.textoLink}
            inserirLinkInicio={this.props.inserirLinkInicio}
            proporcaoCol1={this.props.proporcaoCol1}
            proporcaoCol2={this.props.proporcaoCol2}
            className="espacamento"
          />
        )}
      </span>
    );
  }
}

const Child = props => {
  const inserirLink = props.inserirLink;
  const proporcaoCol1 = props.proporcaoCol1;
  const proporcaoCol2 = props.proporcaoCol2;
  var text = <p className="textoCaixa">{props.textoUnidade}</p>;
  var paddingEsquerda = "";
  if (inserirLink) {
    const inserirLinkInicio = props.inserirLinkInicio;
    text = (
      <p className="textoCaixa">
        <a href={props.link} target="_blank">
          {props.textoLink}
        </a>
        {" " + props.textoUnidade}
      </p>
    );
    if (!inserirLinkInicio) {
      text = (
        <p className="textoCaixa">
          {props.textoUnidade}
          <a href={props.link}>{props.textoLink} </a>
        </p>
      );
    }
  }

  return (
    <div className="espacamento">
      <Container className={props.tipoCaixa + " borda10"} id="caixa">
        <Row noGutters={true} className="marginLeftNegative15">
          <Col
            md={proporcaoCol1}
            lg={proporcaoCol1}
            xs={proporcaoCol1}
            sm={proporcaoCol1}
          >
            <ScrollAnimation
              animateIn={"flipInY"}
              duration={2}
              delay={2}
              animateOnce={true}
            >
              <img src={props.icone} alt="" className="imgCaixaPP" />
            </ScrollAnimation>
          </Col>
          <Col
            md={proporcaoCol2}
            lg={proporcaoCol2}
            xs={proporcaoCol2}
            sm={proporcaoCol2}
            className={paddingEsquerda}
          >
            {text}
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default IconeHide;
