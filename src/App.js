/* eslint-disable import/first */
import React from "react";
import "./App.css";
import Testeira from "./components/Testeira.js";
import CapaUnidade from "./components/capa/CapaPP.js";
import Unidade from "./components/Unidade2.js";
import DescritorUnidade1 from "./components/DescritorUnidade1.js";
import DescritorUnidade2 from "./components/DescritorUnidade2.js";


import menuHamburguer from "./img/menuObesidade2.png";
import { Link } from "react-router-dom";

import AOS from "aos";
import "aos/dist/aos.css";

const testeira = {
  nomeCursoPt1: "Ações de enfrentamento do",
  nomeCursoPt2: "sobrepeso e obesidade na Atenção Básica",
  nomeUnidadePt1: "Ações no território ampliado para",
  nomeUnidadePt2: "o enfrentamento do sobrepeso e obesidade",
  tipoBarra: "barraHamburguerLogo"
};

const sumario = {
  qntColunas: 2,
  tipoSumario: "sumarioUnidade"
};

const sumarioTitulosPt1 = [
  "Alimentação saudável e o território",
  "O território e a intersetorialidade"
];

const sumarioTitulosPt2 = [
  "Estratégias intersetoriais para o enfrentamento do sobrepeso e da obesidade",
  "Promoção de ambientes saudáveis"
];

const unidade = {
  tipoUnidade: "barraUnidade",
  corBarra: "yellow"
};

/*
const testeira = {
  nomeCursoPt1 : 'Ações de enfrentamento do',
  nomeCursoPt2: 'sobrepeso e obesidade na Atenção Básica',
  nomeUnidadePt1: 'Ações individuais para o',
  nomeUnidadePt2: 'enfrentamento do sobrepeso e obesidade',
  tipoBarra: 'barraHamburguerLogo'
}






const testeira = {
  nomeCursoPt1 : 'Ações de enfrentamento do',
  nomeCursoPt2: 'sobrepeso e obesidade na Atenção Básica',
  nomeUnidadePt1: 'PROMOÇÃO DA ALIMENTAÇÃO SAUDÁVEL',
  nomeUnidadePt2: 'E PREVENÇÃO DO SOBREPESO E OBESIDADE ',
  tipoBarra: 'barraHamburguerLogo'
}

const sumario = {
  qntColunas : 2,
  tipoSumario : 'sumarioUnidade'
}



const sumarioTitulosPt1 = ['Cultura e hábitos alimentares ', ' avaliação e diagnóstico alimentar e nutricional de coletividades '];

const sumarioTitulosPt2 = ['Estratégias de promoção de alimentação saudável e prevenção de sobrepeso e obesidade com enfoque nas coletividades'];
*/

class App extends React.Component {
  constructor(props) {
    super(props);    
  }
  
  componentDidMount() {
  }
  render() {
    return (
      <div className="App">
        {/*<Testeira nomeCursoPt1={testeira.nomeCursoPt1} nomeCursoPt2 ={testeira.nomeCursoPt2} 
        nomeUnidadePt1={testeira.nomeUnidadePt1} nomeUnidadePt2={testeira.nomeUnidadePt2} 
        tipoBarra={testeira.tipoBarra} menuHamburguer={menuHamburguer}/> */}
        <DescritorUnidade1/>

        {/*<Link to="/un2">Teste un2</Link>*/}
      </div>
    );
  }
}

export default App;
